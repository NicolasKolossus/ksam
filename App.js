import React from 'react';
import { AppRegistry } from "react-native";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from "redux";
 
import AppReducer from "./src/reducer";

import { AppNavigator, middleware } from "./src/navigation/AppNavigation"; 

const store = createStore(AppReducer, applyMiddleware(middleware));

 
const App = () => {
  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  );
};

AppRegistry.registerComponent("StarterApp", () => App);
 
export default App;