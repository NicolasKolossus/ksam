import React from "react";

import {
  StyleSheet,
  View,
  ImageBackground,
} from "react-native";

const FormBackgroundContainer = ({ navigation }) => {

  return (
    <View style={{flex: 1}}>
      {/* // <ImageBackground */}
      {/* //   source={require("../assets/images/background.png")}
      //   style={styles.image}
      // ></ImageBackground> */}
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  }
});

export default FormBackgroundContainer;
