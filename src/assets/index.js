const background = {
    image: require('./images/background.png'),
    logo: require('./images/background-logo.png')
}

const header = {
    logo: require('./images/header.png')
}

export {
    background,
    header
};