import { Platform, StyleSheet, Dimensions } from "react-native";
import { version } from '../package.json';

export const appVersion = version;

export const AppStyles = {
  color: {
    main: "#5ea23a",
    text: "#000000",
    title: "#464646",
    subtitle: "#545454",
    categoryTitle: "#161616",
    tint: "#ff5a66",
    description: "#bbbbbb",
    filterTitle: "#8a8a8a",
    starRating: "#2bdf85",
    location: "#a9a9a9",
    white: "white",
    facebook: "#4267b2",
    grey: "#5C5C5C",
    black: "#000000",
    greenBlue: "#00aea8",
    placeholder: "#a0a0a0",
    background: "#f2f2f2",
    blue: "#3293fe",
  },
  fontSize: {
    title: 30,
    content: 20,
    normal: 16,
  },
  buttonWidth: {
    main: "70%",
  },
  textInputWidth: {
    main: "80%",
  },
  borderRadius: {
    main: 25,
    small: 5,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
};

export const AppIcon = {
  container: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 8,
    marginRight: 10,
  },
  style: {
    tintColor: AppStyles.color.tint,
    width: 25,
    height: 25,
  },
  images: {
    home: require("./assets/icons/home.png"),
    defaultUser: require("./assets/icons/default_user.jpg"),
    logout: require("./assets/icons/shutdown.png"),
  },
  iconContainer: {
    position: "absolute",
    top: Platform.OS === 'android'? 30 : 50,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white,
  },
  imgContainer: {
    position: "absolute",
    top: Platform.OS === 'android'? 30 : 50,
    right: 20,
    width: 30,
  },
  logoutContainer: {
    position: "absolute",
    top: Platform.OS === 'android'? 30 : 50,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main
  },
};

export const InputContainerStyle = StyleSheet.create({
  InputContainer: {
    marginBottom: 10,
  },
  body: {
    width: "100%",
    paddingLeft: 15,
    height: 54,
    borderColor: "#f5f5f5",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
  },
  textInputContainerBG: {
    height: 100,
    width: '100%',
    padding: 10,
    borderColor: "gray",
    marginTop: 5,
    borderWidth: 1,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: "#ffffff",
    marginBottom: 10,
  }
});

export const borderInputContainer = StyleSheet.create({
  InputContainer: {
    width: AppStyles.textInputWidth.main,
    marginTop: 10,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: AppStyles.color.grey,
    borderRadius: AppStyles.borderRadius.main,
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: AppStyles.color.text,
  }
});

export const HeaderButtonStyle = StyleSheet.create({
  multi: {
    flexDirection: "row",
  },
  container: {
    padding: 10,
  },
  image: {
    justifyContent: "center",
    width: 35,
    height: 35,
    margin: 6,
  },
  rightButton: {
    color: AppStyles.color.tint,
    marginRight: 10,
    fontWeight: "normal",
  },
});

export const ListStyle = StyleSheet.create({
  title: {
    fontSize: 16,
    color: AppStyles.color.subtitle,
    fontWeight: "bold",
  },
  subtitleView: {
    minHeight: 55,
    flexDirection: "row",
    paddingTop: 5,
    marginLeft: 10,
  },
  leftSubtitle: {
    flex: 2,
  },
  avatarStyle: {
    height: 80,
    width: 80,
  },
});
