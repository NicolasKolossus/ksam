import React from "react";
import { Animated, Easing, Image, StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import {
  createReactNavigationReduxMiddleware,
  createReduxContainer,
} from "react-navigation-redux-helpers";

//authontication:
import SignInScreen from "../pages/authentication/SignIn";
import SignUpScreen from "../pages/authentication/SignUp";
import WelcomeScreen from "../pages/authentication/Welcome";

//placeRegister
import Photos from "../pages/PlaceRegister/Photos";
import Principal from "../pages/PlaceRegister/Principal";
import Coordinates from "../pages/PlaceRegister/Coordinates";
import Caracteristicas from "../pages/PlaceRegister/Caracteristicas";

//eventsRegister
import Screen1 from "../pages/Excavation/Screen1";
import Screen3 from "../pages/Excavation/Screen3";
import Screen4 from "../pages/Excavation/Screen4";
import Screen5 from "../pages/Excavation/Screen5";

//monitorRegister
import MonitorRegisterScreen from "../pages/Monitor/MonitorRegister";
import CondicionesScreen from "../pages/Monitor/Condiciones";
import ActividadMonitoreadaScreen from "../pages/Monitor/ActividadMonitoreada";
import VertexRegisterScreen from "../pages/Monitor/VertexRegister";
import ArchaeologicalFinding from "../pages/Monitor/ArchaeologicalFinding";
import PhotosOnMonitor from '../pages/Monitor/Photos';

//home
import HomeScreen from "../pages/Home/Home";
import UploadSiteScreen from "../pages/Home/UploadSite";
import UploadExcavationScreen from "../pages/Home/UploadExcavation";
import UploadMonitorScreen from "../pages/Home/UploadMonitor";
import UploadListScreen from "../pages/Home/UploadList";

import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import DrawerContainer from "../components/DrawerContainer";

import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";

const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0,
  },
});

const middleware = createReactNavigationReduxMiddleware((state) => state.nav);

//login stack

const LoginStack = createStackNavigator(
  {
    Signin: { screen: SignInScreen },
    Signup: { screen: SignUpScreen },
    Welcome: { screen: WelcomeScreen },
  },
  {
    initialRouteName: "Welcome",
    headerMode: "none",
  }
);

const HomeStack = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    UploadSite: { screen: UploadSiteScreen },
    UploadExcavation: { screen: UploadExcavationScreen },
    UploadMonitor: { screen: UploadMonitorScreen },
    UploadList: { screen: UploadListScreen }
  },
  {
    initialRouteName: "Home",
    headerMode: "none",
  }
);

//PlaceRegisterStack
const PlaceRegisterStack = createStackNavigator(
  {
    ArchForm: { screen: Principal },
  },
  {
    initialRouteName: "ArchForm",
    headerMode: "none",
  }
);

const PlaceRegisterTabNavigator = createBottomTabNavigator(
  {
    Principal: {
      screen: PlaceRegisterStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <AntDesign style={[{ color: tintColor }]} size={25} name={"home"} />
          </View>
        ),
      },
    },
    Coordinates: {
      screen: Coordinates,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <MaterialIcons
              style={[{ color: tintColor }]}
              size={25}
              name={"place"}
            />
          </View>
        ),
      },
    },
    Caracteristicas: {
      screen: Caracteristicas,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <MaterialIcons
              style={[{ color: tintColor }]}
              size={25}
              name={"speaker-notes"}
            />
          </View>
        ),
      },
    },
    Photos: {
      screen: Photos,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <MaterialIcons
              style={[{ color: tintColor }]}
              size={25}
              name={"insert-photo"}
            />
          </View>
        ),
      },
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === "Home") {
          return (
            <View>
              <Icon
                style={[{ color: tintColor }]}
                size={25}
                name={"ios-home"}
              />
            </View>
          );
        } else {
          return (
            <View>
              <Icon
                style={[{ color: tintColor }]}
                size={25}
                name={"ios-person"}
              />
            </View>
          );
        }
      },
    }),
    tabBarOptions: {
      showLabel: false,
      activeTintColor: "#D68303",
      inactiveTintColor: "rgba(214, 131, 3, 0.4)",
      style: {
        height: 60,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.0,
        elevation: 24,
      },
    },
  },
  {
    initialRouteName: "Home",
  }
);

// PlaceRegisterDrawerStack
const PlaceRegisterDrawerStack = createDrawerNavigator(
  {
    Tab: PlaceRegisterTabNavigator,
  },
  {
    drawerPosition: "left",
    initialRouteName: "Tab",
    drawerWidth: 200,
    contentComponent: DrawerContainer,
  }
);

//EventsRegisterStack
const EventsRegisterStack = createStackNavigator(
  {
    ExcavationMainForm: { screen: Screen1 },
  },
  {
    initialRouteName: "ExcavationMainForm",
    headerMode: "none",
  }
);

const PhotosExcaStack = createStackNavigator(
  {
    EventsRegisterStack: { screen: EventsRegisterStack },
    SelectNivel: { screen: Screen4, navigationOptions: {
      header: null,
    } },
    ViewImages: { screen: Screen5, navigationOptions: {
      headerTitle: 'Agregar imagenes',
    } },
  },
  {
    initialRouteName: "SelectNivel"
  }
);

const EventsRegisterTabNavigator = createBottomTabNavigator(
  {
    Screen1: {
      screen: EventsRegisterStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <AntDesign style={[{ color: tintColor }]} size={25} name={"home"} />
          </View>
        ),
      },
    },
    Caracteristicas: {
      screen: Screen3,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <MaterialIcons
              style={[{ color: tintColor }]}
              size={25}
              name={"place"}
            />
          </View>
        ),
      },
    },
    Photos: {
      screen: PhotosExcaStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <MaterialIcons
              style={[{ color: tintColor }]}
              size={25}
              name={"insert-photo"}
            />
          </View>
        ),
      },
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === "Home") {
          return (
            <View>
              <Icon
                style={[{ color: tintColor }]}
                size={25}
                name={"ios-home"}
              />
            </View>
          );
        } else {
          return (
            <View>
              <Icon
                style={[{ color: tintColor }]}
                size={25}
                name={"ios-person"}
              />
            </View>
          );
        }
      },
    }),
    tabBarOptions: {
      showLabel: false,
      activeTintColor: "#D68303",
      inactiveTintColor: "rgba(214, 131, 3, 0.4)",
      style: {
        height: 60,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.0,
        elevation: 24,
      },
    },
  },
  {
    initialRouteName: "Home",
  }
);

//EventsRegisterStack
const MonitorRegisterStack = createStackNavigator(
  {
    MonitorMainForm: { screen: MonitorRegisterScreen },
  },
  {
    initialRouteName: "MonitorMainForm",
    headerMode: "none",
  }
);

const MonitorRegisterTabNavigator = createBottomTabNavigator(
  {
    Screen1: {
      screen: MonitorRegisterStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <AntDesign style={[{ color: tintColor }]} size={25} name={"home"} />
          </View>
        ),
      },
    },
    Condiciones: {
      screen: CondicionesScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <MaterialIcons
              style={[{ color: tintColor }]}
              size={25}
              name={"speaker-notes"}
            />
          </View>
        ),
      },
    },
    ActividadMonitoreada: {
      screen: ActividadMonitoreadaScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <FontAwesome
              style={[{ color: tintColor }]}
              size={25}
              name={"list"}
            />
          </View>
        ),
      },
    },
    Cercania: {
      screen: ArchaeologicalFinding,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <MaterialIcons
              style={[{ color: tintColor }]}
              size={25}
              name={"sort"}
            />
          </View>
        ),
      },
    },
    Vertice: {
      screen: VertexRegisterScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <MaterialIcons
              style={[{ color: tintColor }]}
              size={25}
              name={"place"}
            />
          </View>
        ),
      },
    },
    Photos: {
      screen: PhotosOnMonitor,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <View>
            <MaterialIcons
              style={[{ color: tintColor }]}
              size={25}
              name={"insert-photo"}
            />
          </View>
        ),
      },
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === "Home") {
          return (
            <View>
              <Icon
                style={[{ color: tintColor }]}
                size={25}
                name={"ios-home"}
              />
            </View>
          );
        } else {
          return (
            <View>
              <Icon
                style={[{ color: tintColor }]}
                size={25}
                name={"ios-person"}
              />
            </View>
          );
        }
      },
    }),
    tabBarOptions: {
      showLabel: false,
      activeTintColor: "#D68303",
      inactiveTintColor: "rgba(214, 131, 3, 0.4)",
      style: {
        height: 60,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.0,
        elevation: 24,
      },
    },
  },
  {
    initialRouteName: "Home",
  }
);

// MonitorRegisterDrawerStack
const MonitorRegisterDrawerStack = createDrawerNavigator(
  {
    Tab: MonitorRegisterTabNavigator,
  },
  {
    drawerPosition: "left",
    initialRouteName: "Tab",
    drawerWidth: 200,
    contentComponent: DrawerContainer,
  }
);

// PlaceRegisterDrawerStack
const EventsRegisterDrawerStack = createDrawerNavigator(
  {
    Tab: EventsRegisterTabNavigator,
  },
  {
    drawerPosition: "left",
    initialRouteName: "Tab",
    drawerWidth: 200,
    contentComponent: DrawerContainer,
  }
);

// Manifest of possible screens
const RootNavigator = createStackNavigator(
  {
    LoginStack: { screen: LoginStack },
    DrawerStack: { screen: HomeStack },
    PlaceRegisterDrawerStack: { screen: PlaceRegisterDrawerStack },
    EventsRegisterDrawerStack: { screen: EventsRegisterDrawerStack },
    MonitorRegisterDrawerStak: { screen: MonitorRegisterDrawerStack },
  },
  {
    // Default config for all screens
    headerMode: "none",
    initialRouteName: "DrawerStack",
    transitionConfig: noTransitionConfig,
  }
);

const AppWithNavigationState = createReduxContainer(RootNavigator, "root");

const mapStateToProps = (state) => ({
  state: state.nav,
});

const AppNavigator = connect(mapStateToProps)(AppWithNavigationState);

export { RootNavigator, AppNavigator, middleware };
export default function App() {
  const navigationRef = useNavigationContainerRef();

  useFlipper(navigationRef);

  return (
    <NavigationContainer ref={navigationRef}>{/* ... */}</NavigationContainer>
  );
}