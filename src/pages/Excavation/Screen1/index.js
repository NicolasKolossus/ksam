import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import Icon from "react-native-vector-icons/Ionicons";
import FontistoIcon from "react-native-vector-icons/Fontisto";
import { vw } from "react-native-css-vh-vw";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AntIcon from "react-native-vector-icons/AntDesign";

import { AppStyles, InputContainerStyle, AppIcon } from "../../../AppStyles";
import DropDownPicker from "../../../components/react-native-dropdown-picker";
import { background, header } from '../../../assets';
import DateTimePickerModal from "react-native-modal-datetime-picker";

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";

const Screen1 = ({ navigation }) => {

  const [eventsMainData, setEventsMainDataState] = useState({
    site: "",
    structure: "",
    substructure: "",
    date:"Sin seleccionar fecha",
    unit: "",
    methodology:"",
    observations:"",
    measurements: {
      width: '',
      height: '',
      depth: '',
    }
  });

  const [ dateModal, setDateModalState ] = useState(false);

  const hideDatePicker = () => {
    setDateModalState(false);
  };
 
  const handleConfirm = (date) => {
    let current = getDateFromDateObject(date);

    setEventsMainDataState({ ...eventsMainData, date: current });
    hideDatePicker();
  };

  const getDateFromDateObject = (date) => {
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let year = date.getYear() + 1900;
    month = month < 10? '0' + month : month;
    day = day < 10? '0' + day : day;
    return month + '/' + day + '/' + year;
  }
  
  const viewDatePicker = () => {
    setDateModalState(true);
  }

  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@mainevents:state");
      const parsingTemp = JSON.parse(temp);
      if (temp) {
        setEventsMainDataState({
          ...eventsMainData,
          site: parsingTemp.site,
          structure: parsingTemp.structure,
          date: parsingTemp.date,
          unit: parsingTemp.unit,
          methodology: parsingTemp.methodology,
          observations: parsingTemp.observations,
          measurements: parsingTemp.measurements,
          spinner: false,
        });
      }
    })();
  }, []);

  const setMeasurementsData = (text, flag) => {
    if(flag=="width"){
      const originalData = eventsMainData.measurements.height;
      const originalData2 = eventsMainData.measurements.depth;
      setEventsMainDataState({ ...eventsMainData, measurements: {'width':text, 'height':originalData, 'depth':originalData2}});
    }else if(flag=="height"){
      const originalData = eventsMainData.measurements.width;
      const originalData2 = eventsMainData.measurements.depth;
      setEventsMainDataState({ ...eventsMainData, measurements: {'width':originalData, 'height':text, 'depth':originalData2}});
    }else{
      const originalData = eventsMainData.measurements.width;
      const originalData2 = eventsMainData.measurements.height;
      setEventsMainDataState({ ...eventsMainData, measurements: {'width':originalData, 'height':originalData2, 'depth':text}});
    }
  }

  useEffect(() => {
    const temp = JSON.stringify(eventsMainData);
    AsyncStorage.setItem("@mainevents:state", temp);
  }, [eventsMainData]);

  const mainView = () => {
    return (
      <View style={{paddingLeft: 30,paddingRight: 30}}>
        <View>
          <Text style={styles.mainContainerTitle}>Registro de Excavaciones</Text>
          <View style={InputContainerStyle.InputContainer}>
          <View style={{display: 'flex', flexDirection:'row'}}>
            <TextInput
              style={InputContainerStyle.body}
              placeholder="Sitio"
              onChangeText={(text) =>
                setEventsMainDataState({ ...eventsMainData, site: text })
              }
              value={eventsMainData.site}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            { 
              eventsMainData.site == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
          </View>
          </View>
          <View style={InputContainerStyle.InputContainer}>
          <View style={{display: 'flex', flexDirection:'row'}}>
            <TextInput
                style={InputContainerStyle.body}
                placeholder="Sector/Estructura"
                onChangeText={(text) =>
                  setEventsMainDataState({ ...eventsMainData, structure: text })
                }
                value={eventsMainData.structure}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
                          { 
              eventsMainData.structure == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
          </View>
          </View>
          <View style={InputContainerStyle.InputContainer}>
          <View style={{display: 'flex', flexDirection:'row'}}>
            <TextInput
              style={InputContainerStyle.body}
              placeholder="Unidad"
              onChangeText={(text) =>{
                let num = text.replace(".", '');
                if(!isNaN(num)){
                  setEventsMainDataState({ ...eventsMainData, unit: text })
                }
              }
                
              }
              value={eventsMainData.unit}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
             { 
              eventsMainData.unit == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
          </View>
          </View>
          <View style={InputContainerStyle.InputContainer}>
          <View style={{display: 'flex', flexDirection:'row'}}>
            <TouchableOpacity
              style={[InputContainerStyle.body, { borderWidth:0.7, borderColor: '#CECECE', justifyContent: "center"}]}
              onPress={viewDatePicker}
            >
              <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', paddingRight: 20}}>
                <Text style={{textAlign: "left"}}>{eventsMainData.date}</Text>
                <FontistoIcon name="date" size={20} color="#000000" />
              </View>
              <DateTimePickerModal
                isVisible={dateModal}
                mode="date"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
            </TouchableOpacity>
          </View>
          </View>
          <View style={InputContainerStyle.InputContainer}>
          <View style={{display: 'flex', flexDirection:'row'}}>
            <TextInput
              style={InputContainerStyle.body}
              placeholder="Metodologia"
              onChangeText={(text) =>
                setEventsMainDataState({ ...eventsMainData, methodology: text })
              }
              value={eventsMainData.methodology}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            { 
              eventsMainData.methodology == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
          </View>
          </View>
          <View style={InputContainerStyle.InputContainer}>
          <View style={{display: 'flex', flexDirection:'row'}}>
            <TextInput
              style={InputContainerStyle.body}
              placeholder="Observaciones"
              onChangeText={(text) =>
                setEventsMainDataState({ ...eventsMainData, observations: text })
              }
              value={eventsMainData.observations}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
          </View>
          </View>
          <Text style={{textAlign: 'center', marginTop:5, marginBottom:5}}>Medidas</Text>
          <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', marginBottom:10}}>
              <TextInput
                  style={styles.smInput}
                  placeholder=""
                  onChangeText={(text) =>{
                    let num = text.replace(".", '');
                    if(!isNaN(num)){
                      setMeasurementsData(text, 'width')
                    }
                  }
                    
                  }
                  value={eventsMainData.measurements.width}
                  placeholderTextColor={AppStyles.color.grey}
                  underlineColorAndroid="transparent"
                />
                { 
                  eventsMainData.measurements.width == ""?
                  <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
                  :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
                }
              <View style={{ justifyContent: 'center' }}>
                <FontistoIcon name="close-a" size={20}/>
              </View>
              <View style={{display: 'flex', flexDirection: 'row'}}>
                <TextInput
                  style={styles.smInput}
                  placeholder=""
                  onChangeText={(text) =>{
                    let num = text.replace(".", '');
                    if(!isNaN(num)){
                      setMeasurementsData(text, 'height')
                    }
                  }
                    
                  }
                  value={eventsMainData.measurements.height}
                  placeholderTextColor={AppStyles.color.grey}
                  underlineColorAndroid="transparent"
                />
                {
                  eventsMainData.measurements.height == ""?
                  <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
                  :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
                }
                <View style={{ justifyContent: 'center', marginLeft: 10}}>
                  <Text>cm</Text>
                </View>
              </View>
          </View>
          <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', marginBottom:10, marginLeft:-20}}>
                <View style={{display: 'flex', flexDirection:'row'}}>
                  <TextInput
                    style={styles.smInput}
                      placeholder="Profundidad"
                      onChangeText={(text) =>{
                        let num = text.replace(".", '');
                        if(!isNaN(num)){
                          setMeasurementsData(text, 'depth')
                        }
                      }
                        
                      }
                      value={eventsMainData.measurements.depth}
                      placeholderTextColor={AppStyles.color.grey}
                      underlineColorAndroid="transparent"
                  />
                </View>
              </View>
        </View>
      </View>
    )
  }

  return (
    <ImageBackground
      source={background.image}
      style={AppStyles.image}
    >
      <Spinner
        visible={eventsMainData.spinner}
        textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.formContainer}>
        <TouchableOpacity
            style={AppIcon.iconContainer}
            onPress={() => navigation.navigate("Home")}
          >
            <Icon name="chevron-back-sharp" size={30} color="#D68303" />
          </TouchableOpacity>
          <TouchableOpacity
            style={AppIcon.imgContainer}
            onPress={() => navigation.navigate("UploadExcavation")}
          >
            <Image source={header.logo} />
          </TouchableOpacity>
          <View style={styles.mainView}>
            {
              Platform.OS === 'android'?
              <ScrollView
              >
                {mainView()}
              </ScrollView>:
              <KeyboardAwareScrollView
              >
                {mainView()}
              </KeyboardAwareScrollView>
            }
          </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    justifyContent:'flex-end'
  },
  mainView: {
    maxHeight: "85%",
    backgroundColor: "#f8f8f8",

    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  mainContainerTitle: {
    paddingTop: 30,
    paddingBottom: 30,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  mainFormGroupTitle: {
    paddingTop: 20,
    paddingBottom: 10,
    paddingLeft: 15,
    textAlign: "left",
    color: "#5C5C5C",
    fontSize: 16,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: AppStyles.color.blue,
    marginTop: 10,
    textAlign: "left",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  smInput: {
    paddingLeft: 15,
    height: 50,
    borderColor: "#5C5C5C",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
    width: vw(25)
  }
});

export default Screen1;
