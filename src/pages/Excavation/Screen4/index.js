import React, { useState, useEffect } from "react";
import { FlatGrid } from "react-native-super-grid";
import { vw, vh } from "react-native-css-vh-vw";
import ImagePicker from "react-native-image-crop-picker";
import AsyncStorage from "@react-native-community/async-storage";
import Button from "react-native-button";
import Icon from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { AppStyles, AppIcon } from "../../../AppStyles";
import { background, header } from '../../../assets';
import DropDownPicker from "../../../components/react-native-dropdown-picker";

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  Modal
} from "react-native";

const Screen4 = ({ navigation }) => {
  const [levelLayerData, setLevelLayerData] = useState([]);
  const [comboboxData, setComboboxData] = useState([]);
  const [selectedComboBox, setSelectedCombobox] = useState('');
  const [imageUrl, setImageUrl] = useState([]);
  const [imageUrlArr, setImageUrlArr] = useState({});


  var focusListener;
  
  useEffect(() => {
    (async () => {
      focusListener = navigation.addListener('didFocus', () => {
        onFocusFunction()
      })
      let data = await AsyncStorage.getItem("@NivelCaps:state");
      data = JSON.parse(data);
      setLevelLayerData(data);
      const temp = await AsyncStorage.getItem("@excavationPhotos:state");
      if (temp) {
        const parsingTemp = JSON.parse(temp);
        setImageUrlArr(parsingTemp);
        setImageUrlArrlocal(parsingTemp);
      } else {
        setImageUrlArr({});
        setImageUrlArrlocal({});
        AsyncStorage.removeItem("@excavationPhotos:state");
      }
    })();
  }, []);

  useEffect(() => {
    if(levelLayerData != null){
      var tempArr = [];
      levelLayerData.map((ele, levelIndex) => {
        if(ele['levelName'] == ''){
          var levelName = 'Nivel ' + (levelIndex + 1);
        }else{
          var levelName = ele['levelName'];
        }
        tempArr.push({ label: levelName, value: levelName });
        // if(imageUrlArr){
        //   let temp3 = [];
        //   //temp3.push({});
        //   var t = imageUrlArr;
        //   t[levelName] = temp3;
        //   setImageUrlArr(t);
        // }
        Object.keys(ele['capas']).map((layer, layerIndex) => {
          var layerName = levelName + '_'+ ele['capas'][layer]['layerData']['Nombre'];
          tempArr.push({ label: layerName, value: layerName });
          // if(imageUrlArr){
          //   let temp3 = [];
          //   //temp3.push({});
          //   var t = imageUrlArr;
          //   t[layerName] = temp3;
          //   setImageUrlArr(t);
          //   console.log(t);
          // }
          Object.keys(ele['capas'][layer]).map((trait, traitIndex) => { 
            if(trait != 'layerData')
              tempArr.push({ label: layerName+'_trait'+(traitIndex), value: layerName+'_trait'+(traitIndex) });
              // if(imageUrlArr){
              //   let temp3 = [];
              //   //temp3.push({});
              //   var t = imageUrlArr;
              //   t[layerName+'_trait'+(traitIndex)] = temp3;
              //   setImageUrlArr(t);
              // }
          })
        })
      })
      setComboboxData(tempArr);
    }
  }, [levelLayerData])
  const onFocusFunction = async() => {
    let data = await AsyncStorage.getItem("@NivelCaps:state");
    data = JSON.parse(data);
    //setViewFlagData(false);
    setLevelLayerData(data);
    const temp = await AsyncStorage.getItem("@excavationPhotos:state");
    if (temp) {
      const parsingTemp = JSON.parse(temp);
      setImageUrlArr(parsingTemp);
      setImageUrlArrlocal(parsingTemp);
    } else {
      setImageUrlArr({});
      setImageUrlArrlocal({});
      AsyncStorage.removeItem("@excavationPhotos:state");
    }
  }

  const getFilePath = (id) => {
    var newarr = JSON.stringify(imageUrlArr[id]["images"]);
    if(imageUrlArr[id][0] != undefined){
      return imageUrlArr[id][0]["image"];
    }
  };



  function mainView1() {
    return (
    <View style={styles.mainContainer}>
      {comboboxData.length > 0 ? <>
      <Text style={styles.buttonGroupTitle}>Seleccionar Nivel/Capa</Text>
        <FlatGrid
        itemDimension={80}
        data={comboboxData}
        style={styles.gridView}
        spacing={10}
        renderItem={({ item }) => (
          <View style={styles.itemContainer}>
            <TouchableOpacity
              onPress={() => {setSelectedCombobox(item.value); navigation.navigate('ViewImages', {selectedComboBox : item.value}); 
            }}
            >
              {imageUrlArr[item.value] != null ? (
                <Image
                source={{ uri: getFilePath(item.value) }}
                style={{ width: 85, height: 85, marginBottom: 20 }}
              />
            ) : (<MaterialIcons
              style={[{ color: "#000000" }]}
              size={100}
              name={"insert-photo"} 
            />)}
              
            <Text style={styles.textIcon}>{item.value}</Text>
            </TouchableOpacity>
          </View>
        )}
      />
      </>
      :<Text style={styles.buttonGroupTitle}>Sin nivel y capa</Text>}
    </View>
    );
  }

  return (
    <ImageBackground
    source={background.image}
    style={AppStyles.image}
    >
    <SafeAreaView style={styles.formContainer}>
    <TouchableOpacity
        style={AppIcon.iconContainer}
        onPress={() => navigation.navigate("Home")}
      >
        <Icon name="chevron-back-sharp" size={30} color="#D68303" />
      </TouchableOpacity>
      <TouchableOpacity
        style={AppIcon.imgContainer}
        onPress={() => navigation.navigate("UploadExcavation")}
      >
        <Image source={header.logo} />
      </TouchableOpacity>
      <View style={styles.mainContainer}>
        {mainView1()}
      </View>
      </SafeAreaView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  formContainer: {
    flex: 1,
    justifyContent: "flex-end"
  },
  mainContainer: {
    backgroundColor: "#f8f8f8",
    paddingLeft: 20,
    paddingRight: 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    height: 520
  },

  mainContainer2: {
    backgroundColor: "#f8f8f8",
    alignItems: "center",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    height: 550
  },
  imgPickerFormContainer: {
    backgroundColor: "#f8f8f8",
    alignItems: "center",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    flex: 0.9,
  },
  buttonGroupTitle: {
    fontSize: 20,
    color: "#5C5C5C",
    fontWeight: "bold",
    marginBottom: 30,
    marginTop: 20,
  },
  buttonContainer: {
    width: vw(80),
    marginTop: 10,
    borderWidth: 1,
    borderColor: "#CECECE",
    backgroundColor: "#ffffff",
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
  },
  smButtonContainer: {
    width: vw(50),
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    width: vw(50),
    marginTop: 50,
    marginBottom: 50,
  },
  selectButtonContainer: {
    width: vw(50),
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    width: vw(50),
    marginTop: 30,
    borderWidth: 1,
    borderColor: "#CECECE",
    backgroundColor: "#ffffff",
  },
  deleteButtonContainer: {
    width: vw(50),
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    width: vw(50),
    marginTop: 10,
    marginBottom: 30,
  },
  buttonText: {
    fontSize: 16,
    color: AppStyles.color.grey,
  },
  bgImage: {
    width: "100%",
    height: vh(25),
    borderRadius: 10,
  },
  gridView: {
    height: vw(40),
    marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    justifyContent: "center",
    height: 120,
  },
  title: {
    fontSize: 22,
  },
  textIcon: {
    alignItems: 'center', 
    justifyContent: 'center',
    fontSize: 13
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  }
});

export default Screen4;
