import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/Ionicons";
import AntDesignIcon from "react-native-vector-icons/AntDesign";
import { vw, vh } from "react-native-css-vh-vw";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Modal from "react-native-modal";
import DropDownPicker from "../../../components/react-native-dropdown-picker";
import { InputContainerStyle } from "../../../AppStyles";
import { dataMaterial } from "../../../service/util";
import {useRoute} from '@react-navigation/native';
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
  Button
} from "accordion-collapse-react-native";

import { AppStyles, AppIcon } from "../../../AppStyles";
import { background, header } from "../../../assets";

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
} from "react-native";

const Screen3 = ({ navigation }) => {
  const [state, setState] = useState([]);

  const [detailState, setDetailState] = useState({
    levelName: "",
  });

 

  const addLevel = () => {
    const temp = state;
    var levelKey = "capas";
    var arr = {};
    (arr["levelName"] = ""),
      (arr[levelKey] = {
        capa1: {
          layerData: {
            Características_Depositacionales: "",
            Nombre: "Capa 1",
            Propiedades_Matriz: "",
            Espesor: "",
            Materials : {
              0 : {
                Material_Cultural: "",
                Frecuencia: ""
              }
            }
          },
        },
      });
    temp.push(arr);
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
  };

  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@NivelCaps:state");
      const parsingTemp = JSON.parse(temp);
      if (parsingTemp.length) {
        setState(parsingTemp);
        setDetailState({
          ...detailState,
          levelName: "",
        });
      }
    })();
  }, []);

  const setLevelName = (levelIndex, value) => {
    const temp = state;
    temp[levelIndex]["levelName"] = value;
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
  };

  const addLayer = (levelIndex, levelKey) => {
    const temp = state;
    var key = "capa" + (Object.keys(state[levelIndex][levelKey]).length + 1);
    temp[levelIndex][levelKey][key] = {
      layerData: {
        Nombre: key,
        Características_Depositacionales: "",
        Propiedades_Matriz: "",
        Espesor: "",
        Materials : {
          0 : {
            Material_Cultural: "",
            Frecuencia: ""
          }
        }
      }
    };
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
  };

  const addTrait = (levelIndex, levelKey, layerKey) => {
    const temp = state;
    var key =
      "trait" + (Object.keys(state[levelIndex][levelKey][layerKey]).length);
    temp[levelIndex][levelKey][layerKey][key] = {
      Características_Depositacionales: "",
      Propiedades_Matriz: "",
      Espesor: "",
      Materials : {
        0 : {
          Material_Cultural: "",
          Frecuencia: ""
        }
      }
    };
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
  };

  useEffect(() => {
    const temp = JSON.stringify(state);
    AsyncStorage.setItem("@NivelCaps:state", temp);
  }, [detailState]);

  const levelUpload = (levelKey) => {
    console.log(levelKey);
  };

  const saveFormData = (
    levelIndex,
    levelKey,
    layerKey,
    traitKey,
    eleKey,
    value
  ) => {
    const temp = state;
    temp[levelIndex][levelKey][layerKey][traitKey][eleKey] = value;
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
  };

  const saveLayerFormData = (
    levelIndex,
    levelKey,
    layerKey,
    eleKey,
    value
  ) => {
    const temp = state;
    temp[levelIndex][levelKey][layerKey]['layerData'][eleKey] = value;
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
  }

  const deleteAll = () => {
    setState([]);
    AsyncStorage.removeItem("@excavationPhotos:state");
    setDetailState({
      ...detailState,
      levelName: "",
    });
  };

  const returnValue = (levelIndex, levelKey, layerKey, traitKey, eleKey) => {
    return state[levelIndex][levelKey][layerKey][traitKey][eleKey];
  };

  const returnLayerValue = (levelIndex, levelKey, layerKey, eleKey) => {
    return state[levelIndex][levelKey][layerKey]['layerData'][eleKey];
  }

  const onClickAdd = (levelIndex, levelKey, layerKey) => {
    const temp = state;
    var key = (Object.keys(state[levelIndex][levelKey][layerKey]['layerData']['Materials']).length);
    temp[levelIndex][levelKey][layerKey]['layerData']['Materials'][key] = {
      Material_Cultural: "",
      Frecuencia: ""
    };
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
    console.log(temp[levelIndex][levelKey][layerKey]['layerData']['Materials']);
  };

  const saveFormDataMaterialLayer = (
    levelIndex,
    levelKey,
    layerKey,
    traitKey,
    eleKey,
    value
  ) => {
    const temp = state;
    temp[levelIndex][levelKey][layerKey]['layerData']['Materials'][traitKey][eleKey] = value;
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
  };

  const returnLayerValueMaterial = (levelIndex, levelKey, layerKey, index, eleKey) => {
    return state[levelIndex][levelKey][layerKey]['layerData']['Materials'][index][eleKey];
  }

  const onClickAddTrait = (levelIndex, levelKey, layerKey, traitKey) => {
    const temp = state;
    var key = (Object.keys(state[levelIndex][levelKey][layerKey][traitKey]['Materials']).length);
    temp[levelIndex][levelKey][layerKey][traitKey]['Materials'][key] = {
      Material_Cultural: "",
      Frecuencia: ""
    };
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
    console.log(temp[levelIndex][levelKey][layerKey]['layerData']['Materials']);
  };

  const saveFormDataMaterialTrait = (
    levelIndex,
    levelKey,
    layerKey,
    traitKey,
    index,
    eleKey,
    value
  ) => {
    const temp = state;
    temp[levelIndex][levelKey][layerKey][traitKey]['Materials'][index][eleKey] = value;
    setState(temp);
    setDetailState({
      ...detailState,
      levelName: "",
    });
  };

  const returnLayerValueMaterialTrait = (levelIndex, levelKey, layerKey, traitKey, index ,eleKey) => {
    return state[levelIndex][levelKey][layerKey][traitKey]['Materials'][index][eleKey];
  }

  const mainView1 = () => {
    var flag = state.length;
    return (
      <View>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.mainContainerTitle}>Nivel/Capa</Text>
          <TouchableOpacity onPress={() => deleteAll()}>
            <AntDesignIcon name="delete" size={30} color="#5C5C5C" />
          </TouchableOpacity>
        </View>
        {flag ? (
          <>
            {state.map((levelData, levelIndex) => {
              return Object.keys(levelData["capas"]).map(
                (layerKey, layerIndex) => {
                  return (
                    <View key={"uniqueId" + layerIndex + "_" + levelIndex}>
                      <Collapse style={styles.collapseContainer}>
                        <CollapseHeader>
                          <View
                            style={styles.collapseHeader}
                            key={"uniqueId" + layerIndex + "_" + levelIndex}
                          >
                            <Text>
                              {levelIndex +
                                1 +
                                " ( " +
                                levelData["levelName"] +
                                ")" +
                                " / " +
                                returnLayerValue(
                                  levelIndex,
                                  "capas",
                                  layerKey,
                                  "Nombre"
                                )}
                            </Text>
                            {layerKey == "capa1" ? (
                              <></>
                            ) : (
                              <AntDesignIcon
                                key={levelIndex + "_" + layerIndex}
                                name="plus"
                                size={30}
                                color="rgba(111, 66, 193, 0)"
                              />
                            )}

                            <TouchableOpacity
                              key={levelIndex + "_" + layerIndex}
                              onPress={() => addLayer(levelIndex, "capas")}
                              style={{
                                backgroundColor: "rgba(27, 181, 92, 0.2)",
                                borderRadius: 20,
                              }}
                            >
                              <AntDesignIcon
                                key={levelIndex + "_" + layerIndex}
                                name="plus"
                                size={30}
                                color="rgba(27, 181, 92, 1)"
                              />
                            </TouchableOpacity>
                          </View>
                        </CollapseHeader>
                        <CollapseBody style={styles.collapseBody}>
                          <View
                            style={{
                              width: "100%",
                              display: "flex",
                              flexDirection: "row",
                              justifyContent: "space-between",
                              paddingRight: 10,
                            }}
                          >
                            <TextInput
                              style={[styles.normalInput, {width:'80%'}]}
                              onChangeText={(text) =>
                                setLevelName(levelIndex, text)
                              }
                              value={levelData["levelName"]}
                              placeholder="Introduzca el nombre de su nivel"
                            />
                            <TouchableOpacity
                              key={levelIndex + "_" + layerIndex}
                              onPress={() =>
                                addTrait(levelIndex, "capas", layerKey)
                              }
                              style={{
                                alignSelf: "center",
                                width: 30,
                                height: 30,
                                backgroundColor: "rgba(27, 181, 92, 0.2)",
                                borderRadius: 20,
                              }}
                            >
                              <AntDesignIcon
                                key={levelIndex + "_" + layerIndex}
                                name="plus"
                                size={30}
                                color="rgba(27, 181, 92, 1)"
                              />
                            </TouchableOpacity>
                          </View>
                          <Collapse style={styles.collapseContainer}>
                            <CollapseHeader>
                              <View style={styles.collapseHeader}>
                                <Text>
                                  {levelIndex +
                                    1 +
                                    " ( " +
                                    levelData["levelName"] +
                                    ")" +
                                    " / " +
                                    returnLayerValue(
                                      levelIndex,
                                      "capas",
                                      layerKey,
                                      "Nombre"
                                    )
                                  }
                                </Text>
                              </View>
                            </CollapseHeader>
                            <CollapseBody style={styles.collapseBody}>
                            <View style={styles.titleLabel}>
                                <Text>
                                  Nombre de la capa
                                </Text>
                              </View>
                              <TextInput
                                style={styles.textInputContainerBG}
                                multiline
                                numberOfLines={3}
                                onChangeText={(text) =>
                                  saveLayerFormData(
                                    levelIndex,
                                    "capas",
                                    layerKey,
                                    "Nombre",
                                    text
                                  )
                                }
                                value={returnLayerValue(
                                  levelIndex,
                                  "capas",
                                  layerKey,
                                  "Nombre"
                                )}
                              />
                              <View style={styles.titleLabel}>
                                <Text>
                                  Características Depositacionales
                                </Text>
                              </View>
                              <TextInput
                                style={InputContainerStyle.textInputContainerBG}
                                multiline
                                numberOfLines={3}
                                onChangeText={(text) =>
                                  saveLayerFormData(
                                    levelIndex,
                                    "capas",
                                    layerKey,
                                    "Características_Depositacionales",
                                    text
                                  )
                                }
                                value={returnLayerValue(
                                  levelIndex,
                                  "capas",
                                  layerKey,
                                  "Características_Depositacionales"
                                )}
                              />
                              <View style={styles.titleLabel}>
                                <Text>Propiedades de la Matriz</Text>
                              </View>
                              <TextInput
                                style={InputContainerStyle.textInputContainerBG}
                                multiline
                                numberOfLines={3}
                                onChangeText={(text) =>
                                  saveLayerFormData(
                                    levelIndex,
                                    "capas",
                                    layerKey,
                                    "Propiedades_Matriz",
                                    text
                                  )
                                }
                                value={returnLayerValue(
                                  levelIndex,
                                  "capas",
                                  layerKey,
                                  "Propiedades_Matriz"
                                )}
                              />
                              <View style={styles.unitInputContainer}>
                                <Text>Cm</Text>
                                <TextInput
                                  style={styles.unitInput}
                                  onChangeText={(text) =>
                                    saveLayerFormData(
                                      levelIndex,
                                      "capas",
                                      layerKey,
                                      "Espesor",
                                      text
                                    )
                                  }
                                  value={returnLayerValue(
                                    levelIndex,
                                    "capas",
                                    layerKey,
                                    "Espesor"
                                  )}
                                />
                              </View>
                              {Object.keys(levelData["capas"][layerKey]["layerData"]["Materials"]).map((ele, index) => {
                            return (
                              <View
                                  key={
                                    "materialunq" +
                                    layerIndex +
                                    "_" +
                                    levelIndex +
                                    "_" +
                                    index
                                  }
                                  style={[Platform.OS !== "android" ? { zIndex:1} : null]}
                                >


                                      <View style={styles.titleLabel}>
                                        <Text>Material Cultural</Text>
                                      </View>
                                      <DropDownPicker
                                        items={dataMaterial}
                                        placeholder="Material Cultural"
                                        containerStyle={{
                                          height: 54,
                                          width: "100%"
                                        }}
                                        style={{ backgroundColor: AppStyles.color.white }}
                                        itemStyle={{ justifyContent: "flex-start" }}
                                        dropDownStyle={{ backgroundColor: AppStyles.color.white }}
                                        defaultValue={returnLayerValueMaterial(
                                          levelIndex,
                                          "capas",
                                          layerKey,
                                          index,
                                          "Material_Cultural"
                                        )}
                                        onChangeItem={(item) => {
                                          saveFormDataMaterialLayer(
                                            levelIndex,
                                            "capas",
                                            layerKey,
                                            index,
                                            "Material_Cultural",
                                            item.value
                                          )
                                        }}
                                      />

                                      <View style={styles.titleLabel}>
                                        <Text>Frecuencia</Text>
                                      </View>
                                      <TextInput
                                        style={styles.textInputContainerBG}
                                        multiline
                                        numberOfLines={3}
                                        onChangeText={(text) =>{
                                          let num = text.replace(".", "");
                                          if (!isNaN(num)) {
                                            saveFormDataMaterialLayer(
                                              levelIndex,
                                              "capas",
                                              layerKey,
                                              index,
                                              "Frecuencia",
                                              text
                                            )
                                          }
                                        }
                                        }
                                        value={returnLayerValueMaterial(
                                          levelIndex,
                                          "capas",
                                          layerKey,
                                          index,
                                          "Frecuencia"
                                        )}
                                      />


                                </View>
                               )}
                               )}
                              <TouchableOpacity
                                onPress={() => onClickAdd(levelIndex, "capas", layerKey)}
                                style={{
                                  width: "60%",
                                  alignItems: "center",
                                  justifyContent: "center",
                                  height: 50,
                                  borderWidth: 2,
                                  borderRadius: 20,
                                  borderColor: "#979797",
                                  alignSelf: "center",
                                }}
                              >
                                <Text>Añadir Material</Text>
                              </TouchableOpacity>
                            </CollapseBody>
                          </Collapse>
                          {Object.keys(
                            levelData["capas"][layerKey]
                          ).map((traitKey, traitIndex) => {
                            return (
                              <>
                              {traitKey == "layerData"? <></>:
                                <View
                                  key={
                                    "uniqueId" +
                                    layerIndex +
                                    "_" +
                                    levelIndex +
                                    "_" +
                                    traitIndex
                                  }
                                >
                                  <Collapse style={styles.collapseContainer}>
                                    <CollapseHeader>
                                      <View style={styles.collapseHeader}>
                                        <Text>
                                          {levelIndex +
                                            1 +
                                            " ( " +
                                            levelData["levelName"] +
                                            ")" +
                                            " / " +
                                            returnLayerValue(
                                                levelIndex,
                                                "capas",
                                                layerKey,
                                                "Nombre"
                                              ) +
                                            " / " +
                                            "Rasgo" +
                                            (traitIndex)}
                                        </Text>
                                      </View>
                                    </CollapseHeader>
                                    <CollapseBody style={styles.collapseBody}>
                                      <View style={styles.titleLabel}>
                                        <Text>
                                          Características Depositacionales
                                        </Text>
                                      </View>
                                      <TextInput
                                        style={styles.textInputContainerBG}
                                        multiline
                                        numberOfLines={3}
                                        onChangeText={(text) =>
                                          saveFormData(
                                            levelIndex,
                                            "capas",
                                            layerKey,
                                            traitKey,
                                            "Características_Depositacionales",
                                            text
                                          )
                                        }
                                        value={returnValue(
                                          levelIndex,
                                          "capas",
                                          layerKey,
                                          traitKey,
                                          "Características_Depositacionales"
                                        )}
                                      />
                                      <View style={styles.titleLabel}>
                                        <Text>Propiedades de la Matriz</Text>
                                      </View>
                                      <TextInput
                                        style={styles.textInputContainerSM}
                                        multiline
                                        numberOfLines={3}
                                        onChangeText={(text) =>
                                          saveFormData(
                                            levelIndex,
                                            "capas",
                                            layerKey,
                                            traitKey,
                                            "Propiedades_Matriz",
                                            text
                                          )
                                        }
                                        value={returnValue(
                                          levelIndex,
                                          "capas",
                                          layerKey,
                                          traitKey,
                                          "Propiedades_Matriz"
                                        )}
                                      />
                                      <View style={styles.unitInputContainer}>
                                        <Text>Cm</Text>
                                        <TextInput
                                          style={styles.unitInput}
                                          onChangeText={(text) =>
                                            saveFormData(
                                              levelIndex,
                                              "capas",
                                              layerKey,
                                              traitKey,
                                              "Espesor",
                                              text
                                            )
                                          }
                                          value={returnValue(
                                            levelIndex,
                                            "capas",
                                            layerKey,
                                            traitKey,
                                            "Espesor"
                                          )}
                                        />
                                      </View>
                                      {Object.keys(levelData["capas"][layerKey][traitKey]["Materials"]).map((ele, index) => {
                            return (
                              <View
                                  key={
                                    "materialunq" +
                                    layerIndex +
                                    "_" +
                                    levelIndex +
                                    "_" +
                                    traitKey +
                                    "_" +
                                    index
                                  }
                                  style={[Platform.OS !== "android" ? { zIndex:1} : null]}
                                >

                                      <View style={styles.titleLabel}>
                                        <Text>Material Cultural</Text>
                                      </View>
                                      <DropDownPicker
                                        items={dataMaterial}
                                        placeholder="Material Cultural"
                                        containerStyle={{
                                          height: 54,
                                          width: "100%"
                                        }}
                                        style={{ backgroundColor: AppStyles.color.white }}
                                        itemStyle={{ justifyContent: "flex-start" }}
                                        dropDownStyle={{ backgroundColor: AppStyles.color.white }}
                                        defaultValue={returnLayerValueMaterialTrait(
                                          levelIndex,
                                          "capas",
                                          layerKey,
                                          traitKey,
                                          index,
                                          "Material_Cultural"
                                        )}
                                        onChangeItem={(item) => {
                                          saveFormDataMaterialTrait(
                                            levelIndex,
                                            "capas",
                                            layerKey,
                                            traitKey,
                                            index,
                                            "Material_Cultural",
                                            item.value
                                          )
                                        }}
                                      />

                                      <View style={styles.titleLabel}>
                                        <Text>Frecuencia</Text>
                                      </View>
                                      <TextInput
                                        style={styles.textInputContainerBG}
                                        multiline
                                        numberOfLines={3}
                                        onChangeText={(text) =>{
                                          let num = text.replace(".", "");
                                          if (!isNaN(num)) {
                                            saveFormDataMaterialTrait(
                                              levelIndex,
                                              "capas",
                                              layerKey,
                                              traitKey,
                                              index,
                                              "Frecuencia",
                                              text
                                            )
                                          }
                                        }
                                        }
                                        value={returnLayerValueMaterialTrait(
                                          levelIndex,
                                          "capas",
                                          layerKey,
                                          traitKey,
                                          index,
                                          "Frecuencia"
                                        )}
                                      />
                                </View>
                               )}
                               )}
                              <TouchableOpacity
                                onPress={() => onClickAddTrait(levelIndex, "capas", layerKey, traitKey)}
                                style={{
                                  width: "60%",
                                  alignItems: "center",
                                  justifyContent: "center",
                                  height: 50,
                                  borderWidth: 2,
                                  borderRadius: 20,
                                  borderColor: "#979797",
                                  alignSelf: "center",
                                }}
                              >
                                <Text>Añadir Material</Text>
                              </TouchableOpacity>
                                    </CollapseBody>
                                  </Collapse>
                                </View>
                              }
                              </>
                            );
                          })}
                        </CollapseBody>
                      </Collapse>
                    </View>
                  );
                }
              );
            })}
          </>
        ) : (
          <></>
        )}
        <View style={{ marginTop: 30 }}>
          <CollapseHeader>
            <View style={styles.collapseHeader}>
              <Text>Agregar Nivel</Text>
              <TouchableOpacity
                style={{
                  backgroundColor: "rgba(27, 181, 92, 0.2)",
                  borderRadius: 20,
                }}
                onPress={() => addLevel()}
              >
                <AntDesignIcon
                  name="plus"
                  size={30}
                  color="rgba(27, 181, 92, 1)"
                />
              </TouchableOpacity>
            </View>
          </CollapseHeader>
        </View>
      </View>
    );
  };

  const mainView = () => {
    return (
      <View style={{ flex: 1 }}>
        <KeyboardAwareScrollView
          contentContainerStyle={{ flex: 1, justifyContent: "flex-end" }}
        >
          <TouchableOpacity
            style={AppIcon.iconContainer}
            onPress={() => navigation.navigate("Home")}
          >
            <Icon name="chevron-back-sharp" size={30} color="#D68303" />
          </TouchableOpacity>
          <TouchableOpacity
            style={AppIcon.imgContainer}
            onPress={() => navigation.navigate("UploadExcavation")}
          >
            <Image source={header.logo} />
          </TouchableOpacity>
          <View style={styles.mainContainer}>
            {Platform.OS === "android" ? (
              <ScrollView>{mainView1()}</ScrollView>
            ) : (
              <KeyboardAwareScrollView>{mainView1()}</KeyboardAwareScrollView>
            )}
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  };

  return (
    <ImageBackground source={background.image} style={styles.image}>
      <View style={{ flex: 1 }}>
        <View style={styles.formContainer}>
          <View style={{ flex: 1 }}>
            <KeyboardAwareScrollView
              contentContainerStyle={{ flex: 1, justifyContent: "flex-end" }}
            >
              <TouchableOpacity
                style={AppIcon.iconContainer}
                onPress={() => navigation.navigate("Home")}
              >
                <Icon name="chevron-back-sharp" size={30} color="#D68303" />
              </TouchableOpacity>
              <TouchableOpacity
                style={AppIcon.imgContainer}
                onPress={() => navigation.navigate("UploadExcavation")}
              >
                <Image source={header.logo} />
              </TouchableOpacity>
              <View style={styles.mainContainer}>
                {Platform.OS === "android" ? (
                  <ScrollView>{mainView1()}</ScrollView>
                ) : (
                  <KeyboardAwareScrollView>
                    {mainView1()}
                  </KeyboardAwareScrollView>
                )}
              </View>
            </KeyboardAwareScrollView>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  mainContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    height: vh(76),
    backgroundColor: "#f8f8f8",
    paddingLeft: 15,
    paddingRight: 15,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingBottom: 30,
  },
  collapseContainer: {
    marginBottom: 10,
  },
  mainContainerTitle: {
    paddingTop: 30,
    paddingBottom: 30,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  mainFormGroupTitle: {
    paddingTop: 20,
    paddingBottom: 10,
    paddingLeft: 15,
    textAlign: "left",
    color: "#5C5C5C",
    fontSize: 16,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  imgContainer: {
    position: "absolute",
    top: 10,
    right: 20,
    width: 30,
  },
  iconContainer: {
    position: "absolute",
    top: 10,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: AppStyles.color.blue,
    marginTop: 10,
    textAlign: "left",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  InputContainer: {
    marginTop: 10,
    marginBottom: 10,
  },
  body: {
    paddingLeft: 15,
    height: 54,
    borderColor: "#f5f5f5",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
  },
  smInput: {
    paddingLeft: 15,
    height: 54,
    borderColor: "#5C5C5C",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
    width: vw(25),
  },
  collapseHeader: {
    display: "flex",
    flexDirection: "row",
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: "space-between",
    alignItems: "center",
    height: 50,
    backgroundColor: "#f0f0f0",
    borderRadius: 7,
  },
  collapseBody: {
    backgroundColor: "#e8e8e8",
    marginTop: 10,
    borderRadius: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  titleLabel: {
    justifyContent: "center",
    marginLeft: 10,
    marginTop: 20,
  },
  textInputContainerBG: {
    height: 50,
    padding: 10,
    borderColor: "gray",
    marginTop: 5,
    borderWidth: 1,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: "#ffffff",
    marginBottom: 10,
  },
  textInputContainerModal: {
    width: vw(70),
    padding: 20,
    paddingTop: 20,
    borderColor: "gray",
    marginTop: 10,
    borderWidth: 1,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: "#ffffff",
    marginBottom: 10,
  },
  textInputContainerSM: {
    height: 50,
    padding: 10,
    borderColor: "gray",
    marginTop: 5,
    borderWidth: 1,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: "#ffffff",
  },
  unitInputContainer: {
    alignItems: "center",
    marginTop: 10,
  },
  normalInput: {
    padding: 5,
    borderColor: "gray",
    marginTop: 10,
    marginBottom: 10,
    borderWidth: 1,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: "#ffffff",
  },
  unitInput: {
    padding: 5,
    width: 100,
    height: 50,
    paddingTop: 5,
    borderColor: "gray",
    marginTop: 10,
    borderWidth: 1,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: "#ffffff",
  },
});

export default Screen3;
