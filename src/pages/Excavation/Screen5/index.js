import React, { useState, useEffect } from "react";
import { FlatGrid } from "react-native-super-grid";
import { vw, vh } from "react-native-css-vh-vw";
import ImagePicker from "react-native-image-crop-picker";
import AsyncStorage from "@react-native-community/async-storage";
import Button from "react-native-button";
import Icon from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { AppStyles, AppIcon } from "../../../AppStyles";
import { background, header } from '../../../assets';
import DropDownPicker from "../../../components/react-native-dropdown-picker";

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  Modal
} from "react-native";

const Screen5 = ({ route, navigation }) => {
  //const { selectedComboBox } = route.params;  
  const selectedComboBox = navigation.getParam('selectedComboBox');
  const [previewImage, setPreviewImageData] = useState("");  
  const [modalVisible, setModalVisible] = useState(false);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [selectedImage, setSelectedImage] = useState(""); 
  const [imageUrlArr, setImageUrlArr] = useState({});
  const [viewFlag, setViewFlagData] = useState(false);  
  const [imageUrl, setImageUrl] = useState([]);

  var focusListener;
  
  useEffect(() => {
    (async () => {
      // focusListener = navigation.addListener('didFocus', () => {
      //   onFocusFunction()
      // })
      const temp = await AsyncStorage.getItem("@excavationPhotos:state");
      if (temp) {
        const parsingTemp = JSON.parse(temp);
        setImageUrlArr(parsingTemp);
      } else {
        setImageUrlArr({});
        AsyncStorage.removeItem("@excavationPhotos:state");
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {})();
  }, [previewImage]);

  const saveImageUrlArrToLocal = () => {
    const temp = JSON.stringify(imageUrlArr);
    AsyncStorage.setItem("@excavationPhotos:state", temp);
    console.log(temp);
  }
  
const openCamera = (_callback) => {
    if(confirmSelectedBox()){
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        quality: 0.5
      }).then((image) => {
        let temp = [];
        if(imageUrlArr[selectedComboBox])
          temp = imageUrlArr[selectedComboBox]
        temp.push({ id: parseInt(image.modificationDate) + 1, image: image.path });
        setImageUrl(temp);
        var t = imageUrlArr;
        t[selectedComboBox] = temp;
        setImageUrlArr(t);
        setViewFlagData(true);
        saveImageUrlArrToLocal();
        setPreviewImageData(image.path);
        _callback();
      });
    }
  };

  const openLibrary = (_callback) => {
    if(confirmSelectedBox()){
      ImagePicker.openPicker({
        multiple: true,
        width: 300,
        height: 400,
        quality: 0.5
      }).then((images) => {
          let temp = [];
          if(imageUrlArr[selectedComboBox])
            temp = imageUrlArr[selectedComboBox]
          images.map((item) => {
            temp.push({id: parseInt(item.modificationDate) + 1, image: item.path });
          });
          setImageUrl(temp);
          var t = imageUrlArr;
          t[selectedComboBox] = temp;
          setImageUrlArr(t);
          setViewFlagData(true);
          saveImageUrlArrToLocal();
          setPreviewImageData(images[0].path);
          _callback();
      });
      
    }
  };
  
const confirmDel = (id) => {
    setModalVisible1(true);
    setSelectedImage(id);
  };

function closeModal() {
  setModalVisible(!modalVisible);
}

  const confirmSelectedBox = () => {
    if(selectedComboBox)
      return true;
    else{
      alert("Por favor, seleccione un nivel o capa.")
      return false;
    }
  }

  const delImage2 = () => {
    var newarr = imageUrlArr[selectedComboBox].filter(x => x.id != selectedImage);
    // if(newarr.length == 0){
    //   newarr.push(null);
    // }
    var t = imageUrlArr;
    if(newarr.length == 0){
      delete t[selectedComboBox];
    }else{
      t[selectedComboBox] = newarr;
    }
    setImageUrlArr(t);
    AsyncStorage.removeItem("@excavationPhotos:state");
    const temp1 = JSON.stringify(imageUrlArr);
    AsyncStorage.setItem("@excavationPhotos:state", temp1);
    setSelectedImage("");
    setModalVisible1(!modalVisible1);
  };  

function MainView() {
    return (
      <SafeAreaView style={styles.formContainer}>
          <View style={styles.mainContainer2}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(!modalVisible);
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                  <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => openCamera(function() {
                    closeModal();
                  })}
                >
                  Tomar foto
                </Button>
                <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => openLibrary(function() {
                    closeModal();
                  })}
                >
                  Elegir de galeria
                </Button>
                <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => setModalVisible(!modalVisible)}
                >
                  Cancelar
                </Button>
              </View>
            </View>
          </Modal>

          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible1}
            onRequestClose={() => {
              setModalVisible1(!modalVisible1);
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
              <Text style={styles.buttonGroupTitle}>¿Esta seguro que desea eliminar esta imagen?</Text>
                  <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => delImage2()}
                >
                  OK
                </Button>
                <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => setModalVisible1(!modalVisible1)}
                >
                  Cancelar
                </Button>
              </View>
            </View>
          </Modal>

            <Text style={styles.buttonGroupTitle}>Imagenes</Text>
            {imageUrlArr[selectedComboBox] != null ? (
                <Image style={styles.bgImage} source={previewImage != "" ?  {uri : previewImage} : {uri : imageUrlArr[selectedComboBox][0]["image"]}  } />
            ) : (
              <View
                style={{
                  height: vh(30),
                  justifyContent: "flex-end",
                  alignItems: "center",
                }}
              >
                <MaterialIcons
                  style={[{ color: "#000000" }]}
                  size={100}
                  name={"insert-photo"}
                />
                <Text style={[styles.buttonGroupTitle, { fontSize: 15 }]}>
                  Sin imágen
                </Text>
              </View>
            )}
            {imageUrlArr[selectedComboBox] != null ? (
            <FlatGrid
              itemDimension={80}
              data={imageUrlArr[selectedComboBox]}
              style={styles.gridView}
              spacing={10}
              //extraData={imageUrl}
              renderItem={({ item }) => (
                <View style={styles.itemContainer}>
                  <TouchableOpacity
                    onPress={() => {setPreviewImageData(item.image);}}
                    delayLongPress={1000} onLongPress={()=>{
                      confirmDel(item.id)
                    }}
                  >
                    <Image
                      source={{ uri: item.image }}
                      style={{ width: "100%", height: "100%" }}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
            ) : (<></>)}
            <Button
              containerStyle={styles.selectButtonContainer}
              style={[styles.buttonText, { color: "#5C5C5C" }]}
              onPress={() => setModalVisible(true)}
            >
              Agregar imagen
            </Button>
          </View>
      </SafeAreaView>
    );
  }

  return (
    <View style={styles.mainContainer}>
          {MainView()}
    </View>
  );

}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  formContainer: {
    flex: 1,
    justifyContent: "flex-end"
  },
  mainContainer: {
    backgroundColor: "#f8f8f8",
    paddingLeft: 10,
    paddingRight: 10,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    height: 530
  },
  mainContainer2: {
    backgroundColor: "#f8f8f8",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    height: 550
  },
  imgPickerFormContainer: {
    backgroundColor: "#f8f8f8",
    alignItems: "center",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    flex: 0.9,
  },
  buttonGroupTitle: {
    fontSize: 20,
    color: "#5C5C5C",
    fontWeight: "bold",
    marginBottom: 30,
    marginTop: 20,
  },
  buttonContainer: {
    width: vw(80),
    marginTop: 10,
    borderWidth: 1,
    borderColor: "#CECECE",
    backgroundColor: "#ffffff",
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
  },
  smButtonContainer: {
    width: vw(50),
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    width: vw(50),
    marginTop: 50,
    marginBottom: 50,
  },
  selectButtonContainer: {
    width: vw(50),
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    width: vw(50),
    marginTop: 30,
    borderWidth: 1,
    borderColor: "#CECECE",
    backgroundColor: "#ffffff",
  },
  deleteButtonContainer: {
    width: vw(50),
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    width: vw(50),
    marginTop: 10,
    marginBottom: 30,
  },
  buttonText: {
    fontSize: 16,
    color: AppStyles.color.grey,
  },
  bgImage: {
    width: "100%",
    height: vh(25),
    borderRadius: 10,
  },
  gridView: {
    height: vw(40),
    marginTop: 10,
    flex: 1,
    width: "100%"
  },
  itemContainer: {
    justifyContent: "center",
    height: 120,
  },
  title: {
    fontSize: 22,
  },
  textIcon: {
    alignItems: 'center', 
    justifyContent: 'center',
    fontSize: 13
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  }
});

export default Screen5;
