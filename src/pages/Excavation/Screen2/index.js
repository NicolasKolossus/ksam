import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/Ionicons";
import { vw, vh } from "react-native-css-vh-vw";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { AppStyles, AppIcon } from "../../../AppStyles";
import { background, header } from '../../../assets';

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";

const Screen2 = ({ navigation }) => {
  const [eventsDetailState, setEventsDetailState] = useState({
    methodology:"",
    observations: ""
  });
  
  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@eventsdetail:state");
      const parsingTemp = JSON.parse(temp);
      if (temp) {
        setEventsDetailState({
          ...eventsDetailState,
          methodology: parsingTemp.methodology,
          observations: parsingTemp.observations
        });
      }
    })();
  }, []);

  useEffect(() => {
    const temp = JSON.stringify(eventsDetailState);
    AsyncStorage.setItem("@eventsdetail:state", temp);
  }, [eventsDetailState]);

  const mainView = () => {
    return(
      <>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
        <TouchableOpacity
          style={AppIcon.imgContainer}
          onPress={() => navigation.navigate("UploadExcavation")}
        >
          <Image source={header.logo} />
        </TouchableOpacity>
        <View style={styles.mainContainer}>
          <Text style={styles.mainContainerTitle}>Detalles</Text>
          <View style={{ justifyContent: 'center', marginLeft: 10}}>
              <Text>Metodologia</Text>
          </View>
          <TextInput
              style={{ height: vh(15), padding: 20, paddingTop:20, borderColor: 'gray', marginTop:10, borderWidth: 1, borderRadius: AppStyles.borderRadius.main, backgroundColor: '#ffffff' }}
              multiline
              numberOfLines={10}
              onChangeText={text => setEventsDetailState({...eventsDetailState, methodology: text })}
              value={eventsDetailState.methodology}
          />
          <View style={{ justifyContent: 'center',  marginLeft: 10, marginTop: 10}}>
              <Text>Observaciones</Text>
          </View>
          <TextInput
              style={{ height: vh(25), padding: 20, paddingTop:20, borderColor: 'gray', marginTop:10, borderWidth: 1, borderRadius: AppStyles.borderRadius.main, backgroundColor: '#ffffff' }}
              multiline
              numberOfLines={10}
              onChangeText={text => setEventsDetailState({...eventsDetailState, observations: text })}
              value={eventsDetailState.observations}
          />
        </View>
      </>
    )
  }

  return (
    <ImageBackground
      source={background.image}
      style={styles.image}
    >
      <View style={styles.formContainer}>
          {
            Platform.OS === 'android'?
            <ScrollView
              contentContainerStyle={{
                flexGrow: 1,
                justifyContent: "space-between",
                justifyContent: "flex-end",
              }}
            >
              {mainView()}
            </ScrollView>:
            <KeyboardAwareScrollView
              contentContainerStyle={{
                flexGrow: 1,
                justifyContent: "space-between",
                justifyContent: "flex-end",
              }}
            >
              {mainView()}
            </KeyboardAwareScrollView>
          }
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
  },
  mainContainer: {
    backgroundColor: "#f8f8f8",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingBottom:30
  },
  mainContainerTitle: {
    paddingTop: 30,
    paddingBottom: 30,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  mainFormGroupTitle: {
    paddingTop: 20,
    paddingBottom: 10,
    paddingLeft: 15,
    textAlign: "left",
    color: "#5C5C5C",
    fontSize: 16,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  imgContainer: {
    position: "absolute",
    top: 10,
    right: 20,
    width: 30,
  },
  iconContainer: {
    position: "absolute",
    top: 10,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: AppStyles.color.blue,
    marginTop: 10,
    textAlign: "left",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  InputContainer: {
    marginTop: 10,
    marginBottom: 10,
  },
  body: {
    paddingLeft: 15,
    height: 54,
    borderColor: "#f5f5f5",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
  },
  smInput: {
    paddingLeft: 15,
    height: 54,
    borderColor: "#5C5C5C",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
    width: vw(25)
  }
});

export default Screen2;
