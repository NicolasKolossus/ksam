import React, { useState, useEffect } from "react";
import Button from "react-native-button";
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'react-native-fetch-blob';
import Spinner from 'react-native-loading-spinner-overlay';
import { vw, vh } from 'react-native-css-vh-vw';
import Icon from 'react-native-vector-icons/Ionicons';

import { AppStyles, AppIcon } from '../../../AppStyles';
import { realDb, storage, firebase } from '../../../api'; 
import { background } from '../../../assets';

import {
  ImageBackground,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from "react-native";

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob
const Fetch = RNFetchBlob.polyfill.Fetch
window.fetch = new Fetch({
  auto : true,
  binaryContentTypes : [
      'image/',
      'video/',
      'audio/',
      'foo/',
  ]
}).build()
 
const UploadListScreen = ({ navigation }) => {

  const [state, setState] = useState({
    email: "",
    password: "",
    formKey: ""
  });

  const [ uploadList, setUploadList ] = useState([]);

  const [ uploadExcavationList, setUploadExcavationList ] = useState([]);

  const [ uploadMonitorList, setUploadMonitorList ] = useState([]);

  const [spinner, setSpinnerData] = useState(false);
  const [checksubida, setChecksubida] = useState(true);


  useEffect(() => {
    (async () => {
        const list = await AsyncStorage.getItem("@uploading:list"); 
        const uploadExcavationList = await AsyncStorage.getItem("@uploadingExcavation:list"); 
        const uploadMonitorList = await AsyncStorage.getItem("@uploadingMonitor:list");
        setUploadList(JSON.parse(list));
        setUploadExcavationList(JSON.parse(uploadExcavationList));
        setUploadMonitorList(JSON.parse(uploadMonitorList));
    })();
  },[])

  console.log(uploadExcavationList);

  const excavationList = () => {
    return uploadExcavationList.map((item, index) => {
      return (
        <View key={item.key} style={styles.listContainer}>
          <Text style={styles.listText}>{item.key}</Text>
          <Text style={styles.listText}>Unidad: {item.unit}</Text>
          <Text style={styles.listText}>Sitio: {item.site}</Text>
          <View style={styles.listButtonContainer}>
            <Button 
                containerStyle={styles.listUploadButton}
                style={styles.buttonText} 
                onPress={() => uploadData1(item.key, index)}>
                Subir
            </Button>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => editData1(item.key, index)}>
                Editar
            </Button>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => editData1(item.key, index)}>
                Re-subir
            </Button>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => deleteData1(item.key, index)}>
                Eliminar
            </Button>
          </View>
        </View>
      );
    });
  }

  const monitorList = () => {
    return uploadMonitorList.map((item, index) => {
      return (
        <View key={item} style={styles.listContainer}>
          <Text style={styles.listText}>{item}</Text>
          <View style={styles.listButtonContainer}>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => uploadData2(item, index)}>
                Subir
            </Button>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => editData2(item, index)}>
                Editar
            </Button>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => editData2(item, index)}>
                Re-Subir
            </Button>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => deleteData2(item, index)}>
                Eliminar
            </Button>
          </View>
        </View>
      );
    });
  }

  const list = () => {
    return uploadList.map((item, index) => {
      return (
        <View key={item.key} style={styles.listContainer}>
          <Text style={styles.listText}>{item.key}</Text>
          <Text style={styles.listText}>Tipo: {item.siteType}</Text>
          <View style={styles.listButtonContainer}>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => uploadData(item.key, index)}>
                Subir
            </Button>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => editData(item.key, index)}>
                Editar
            </Button>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => editData(item.key, index)}>
                Re-Subir
            </Button>
            <Button 
                containerStyle={styles.listUploadButton} 
                style={styles.buttonText} 
                onPress={() => deleteData(item.key, index)}>
                Eliminar
            </Button>
          </View>
        </View>
      );
    });
  };

  function checkSitio(data){

    var err = "";
    var principal = data.principal;
    var coordinates = data.coordinates;
    var caract = data.caracteristicas;

    //Principal
    if(principal.siteType == ""){
      err += "\n" + "Tipo de sitio";
    }

    if(principal.siteType == "Otro" && (principal.otroValue == "" || principal.otroValue == " ")){
      err += "\n" + "Tipo de sitio";
    }

    if(principal.geomorphological == "" || principal.geomorphological == " "){
      err += "\n" + "Ubicación Geomorfológica";
    }

    if(principal.description == "" || principal.description == " "){
      err += "\n" + "Descripción del sitio";
    }

    //Coordenadas

    if(coordinates == null){
      err += "\n" + "Coordenadas";
    }else{
      var loop = 1;
      coordinates.forEach(obj => {
        if(obj["A_E"] == "" || obj["A_N"] == "" || obj["A_msnm"] == ""){
          err += "\n" + "Set de coordenadas " + loop + ": ";
          if(obj["A_E"] == ""){
            err += "\n" + "   Punto Este";
          }
          if(obj["A_N"] == ""){
            err += "\n" + "   Punto Norte";
          }
          if(obj["A_msnm"] == ""){
            err += "\n" + "   Punto m.s.n.m";
          }
        }
        loop++;
      });
    }
    
    if(caract !=null){
      if(caract.dimensions == "" || caract.dimensions == " "){
        err += "\n" + "Dimensiones";
      }
      if(caract.relativeChronology == ""){
        err += "\n" + "Cronología Relativa";
      }
      for (let key in caract.culturalMaterial) {
        if(caract.culturalMaterial[key]["Material"] == ""){
          err += "\n" + "Material Cultural " + (parseInt(key) + 1);
        }
        if(caract.culturalMaterial[key]["Material"] == "Otro" && (caract.culturalMaterial[key]["OtroValue"] == "" || caract.culturalMaterial[key]["OtroValue"] == " ")){
          err += "\n" + "Material Cultural " + (parseInt(key) + 1);
        }
      }
    }else{
      err += "\n" + "Dimensiones";
      err += "\n" + "Cronología Relativa";
      err += "\n" + "Material Cultural 1";
    }

    return err;
  }

  const uploadData = async (form_key, index) => {
    let uploadData = await AsyncStorage.getItem(form_key);
    uploadData = JSON.parse(uploadData);

    const temp1 = {
      principal: uploadData['principal'],
      coordinates: uploadData['coordinates'],
      caracteristicas: uploadData['caracteristicas'],
    };

    var check = checkSitio(temp1);

    if(check != ""){
      alert("Se deben completar los siguientes campos: " + "\n" + check);
      navigation.navigate("UploadList");
    }else{
      realDb.ref('archaeologicalSites').child(form_key).push({
        principal: uploadData['principal'],
        coordinates: uploadData['coordinates'],
        caracteristicas: uploadData['caracteristicas'],
        created_at: new Date().toUTCString()
      });
      
      let mime = 'image/jpg';
      let temp = uploadData['photos'];
      temp.map((item, index) => {
        const image = item.image;
        if(image){
          let uploadBlob = null;
          let imageRef = storage.ref(form_key).child(`${index}.jpg`);
          fs.readFile(image, 'base64')
            .then((data) => {
              return Blob.build(data, { type: `image/png;BASE64` })
          })
          .then((blob) => {
              uploadBlob = blob
              return imageRef.put(blob, { contentType: 'image/png' })
          })
          .then(() => {
            uploadBlob.close()
            return imageRef.getDownloadURL()
          })
          .catch((error) => {
            console.log(error);
          })
        }
      });
      navigation.navigate("Home");
      //deleteData(form_key, index)
    }
  }

  const deleteData = async(form_key, index) => {
    const list = await AsyncStorage.getItem("@uploading:list");
    let arr = JSON.parse(list);
    arr.splice(index,1);
    setUploadList(arr);
    AsyncStorage.setItem("@uploading:list", JSON.stringify(arr));
    AsyncStorage.removeItem(form_key);
  }

  const editData = async(form_key, index) => {
    let uploadData = await AsyncStorage.getItem(form_key);
    uploadData = JSON.parse(uploadData);
    console.log(uploadData);
    AsyncStorage.setItem("@principal:state", JSON.stringify(uploadData['principal']));
    AsyncStorage.setItem("@coordinates:state", JSON.stringify(uploadData['coordinates']));
    AsyncStorage.setItem("@caracteristicas:state", JSON.stringify(uploadData['caracteristicas']));
    AsyncStorage.setItem("@photos:state", JSON.stringify(uploadData['photos']));
    const list = await AsyncStorage.getItem("@uploading:list");
    let arr = JSON.parse(list);
    arr.splice(index,1);
    AsyncStorage.setItem("@uploading:list", JSON.stringify(arr));
    AsyncStorage.removeItem(form_key);
    navigation.navigate("ArchForm");
  }

  function checkExcavacion(data){

    var err = "";
    var mainevents2 = data.mainevents1;
    var caract2 = data.caracteristicas1;

    //Pantalla 1
    if(mainevents2.site == "" || mainevents2.site == " "){
      err += "\n" + "Sitio";
    }

    if(mainevents2.structure == "" || mainevents2.structure == " "){
      err += "\n" + "Sector/Esctructura";
    }

    if(mainevents2.unit == "" || mainevents2.unit == " "){
      err += "\n" + "Unidad";
    }

    if(mainevents2.methodology == "" || mainevents2.methodology == " "){
      err += "\n" + "Metodologia";
    }

    if(mainevents2.measurements.height == "" || mainevents2.measurements.height == " "){
      err += "\n" + "Medidas alto";
    }

    if(mainevents2.measurements.width == "" || mainevents2.measurements.width == " "){
      err += "\n" + "Medidas largo";
    }

    if(mainevents2.measurements.depth == "" || mainevents2.measurements.depth == " "){
      err += "\n" + "Medidas profundidad";
    }

    //Nivel / Capa

    var looplvl = 1;
    var loopcapa = 1;
    caract2.forEach(nivel => {
      var errlvl = "";
      if(nivel["levelName"] == "" || nivel["levelName"] == " "){
        errlvl += "\n" + "Nivel " + looplvl + ": ";
        errlvl += "\n" + "   Nombre del nivel";
      }
      for (let capas in nivel["capas"]) {
        var capname = "";
        var errcapa = "";
        var loopmaterial = 1;
        if(nivel["capas"][capas]["layerData"]["Nombre"] == "" || nivel["capas"][capas]["layerData"]["Nombre"] == " "){
          if(errlvl == ""){
            errlvl += "\n" + "Nivel " + looplvl + ": ";
          }
          errlvl += "\n" + "   Nombre de la capa " + loopcapa;
          capname = "Capa " + loopcapa;
        }else{
          capname = nivel["capas"][capas]["layerData"]["Nombre"];
        }
        
        if(nivel["capas"][capas]["layerData"]["Características_Depositacionales"] == "" 
        || nivel["capas"][capas]["layerData"]["Características_Depositacionales"] == " "){
          if(errlvl == ""){
            errlvl += "\n" + "Nivel " + looplvl + ": ";
          }
          errcapa = "\n" + "   " + capname + ":";
          errcapa += "\n" + "             Características depositacionales";
        }

        if(nivel["capas"][capas]["layerData"]["Propiedades_Matriz"] == "" 
        || nivel["capas"][capas]["layerData"]["Propiedades_Matriz"] == " "){
          if(errlvl == ""){
            errlvl += "\n" + "Nivel " + looplvl + ": ";
          }
          if(errcapa == ""){
            errcapa = "\n" + "   " + capname + ":";
          }
          errcapa += "\n" + "             Propiedades de la matriz";
        }

        if(nivel["capas"][capas]["layerData"]["Espesor"] == "" 
        || nivel["capas"][capas]["layerData"]["Espesor"] == " "){
          if(errlvl == ""){
            errlvl += "\n" + "Nivel " + looplvl + ": ";
          }
          if(errcapa == ""){
            errcapa = "\n" + "   " + capname + ":";
          }
          errcapa += "\n" + "             Espesor";
        }

        for (let material in nivel["capas"][capas]["layerData"]["Materials"]) {
          if(nivel["capas"][capas]["layerData"]["Materials"][material]["Material_Cultural"] == ""){
            if(errlvl == ""){
              errlvl += "\n" + "Nivel " + looplvl + ": ";
            }
            if(errcapa == ""){
              errcapa = "\n" + "   " + capname + ":";
            }
            errcapa += "\n" + "             Material cultural " + loopmaterial;
          }

          if(nivel["capas"][capas]["layerData"]["Materials"][material]["Frecuencia"] == ""
          || nivel["capas"][capas]["layerData"]["Materials"][material]["Frecuencia"] == " "){
            if(errlvl == ""){
              errlvl += "\n" + "Nivel " + looplvl + ": ";
            }
            if(errcapa == ""){
              errcapa = "\n" + "   " + capname + ":";
            }
            errcapa += "\n" + "             Frecuencia " + loopmaterial;
          }
          loopmaterial++;
        }
        
        if(errcapa != ""){
          errlvl+= errcapa;
        }

        loopcapa++;
      }
      looplvl++;
      if(errlvl != ""){
        err+= errlvl;
      }
    });

    return err;
  }

   function postImagenesApi(data){
      return fetch('https://maa-desa.kolossus.tech/api/subirfotosexca',{
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          ficha: data.ficha,
          nombre: data.nombre,
          index: data.index,
          imagen: data.imagen
        })
      }).then((response) => response.json())
  };

  function getResponseImage(data){
    return Promise.all([postImagenesApi(data)]);
  }

  const uploadHandler = async (form_key) => {
    let uploadData = await AsyncStorage.getItem(form_key);
    uploadData = JSON.parse(uploadData);
    let temp = uploadData['photos'];

    Object.keys(temp).map((key, index) => {
      temp[key].map((item, index) => {
      const image = item.image;
      if(image) {
        fs.readStream(image, "base64", 102399)
          .then((data) => {
            setSpinnerData(true);
            data.open()
            data.onData((chunk) => {
              data += chunk
            })
            data.onError((err) => {
              setChecksubida(false);
              alert(err);
              navigation.navigate("UploadList");
              return false;
            })
            data.onEnd(() => {
              var json = {
                'ficha': form_key,
                'nombre': key,
                'index': index,
                'imagen': data
              };
              getResponseImage(json)
              .then(([imagenes]) => {
                if(imagenes.status == "ER"){
                  setChecksubida(false);
                  alert(imagenes.response);
                  navigation.navigate("UploadList");
                  return false;
                }
              });
            })
          })
          .catch((error) => {
            setChecksubida(false);
            alert(error);
            navigation.navigate("UploadList");
            return false;
          });
        }
      });
    });
    return true;
  }

  const uploadData1 = async (form_key, index) => {
    let uploadData = await AsyncStorage.getItem(form_key);
    uploadData = JSON.parse(uploadData);
    let carc = uploadData['caracteristicas'];
    
    const temp1 = {
      mainevents1: uploadData['mainevents'],
      caracteristicas1: uploadData['caracteristicas']
    };

    var check = checkExcavacion(temp1);

    if(carc.length == 0){
      alert("La ficha de excavacion debe tener al menos 1 nivel y capa.");
      navigation.navigate("UploadList");
    }else if(check != ""){
      alert("Se deben completar los siguientes campos: " + "\n" + check);
      navigation.navigate("UploadList");
    }else{
     let temp = uploadData['photos'];

     console.log(temp);

      if(temp != null){
        if(temp.length == 0){
          alert("Debe agregar al menos 1 imagen en un nivel o capa.");
          navigation.navigate("UploadList");
        }else{
          await uploadHandler(form_key);
          setTimeout(function() { 
            setSpinnerData(false);
            if(checksubida){
              realDb.ref('excavation').child(form_key).push({
                mainevents: uploadData['mainevents'],
                eventsdetail: uploadData['eventsdetail'],
                caracteristicas: uploadData['caracteristicas'],
                created_at: new Date().toUTCString()
              });
              alert("Se subio la ficha con ID: " + form_key);
              navigation.navigate("Home");

              //deleteData1(form_key, index);
          }}, 3000);
        }
      }
    }
  }

  const deleteData1 = async(form_key, index) => {
    const list = await AsyncStorage.getItem("@uploadingExcavation:list");
    let arr = JSON.parse(list);
    arr.splice(index,1);
    setUploadExcavationList(arr);
    AsyncStorage.setItem("@uploadingExcavation:list", JSON.stringify(arr));
    AsyncStorage.removeItem(form_key);
  }

  const editData1 = async(form_key, index) => {
    let uploadData = await AsyncStorage.getItem(form_key);
    uploadData = JSON.parse(uploadData);
    console.log(uploadData);
    AsyncStorage.setItem("@mainevents:state", JSON.stringify(uploadData['mainevents']));
    AsyncStorage.setItem("@eventsdetail:state", JSON.stringify(uploadData['eventsdetail']));
    AsyncStorage.setItem("@NivelCaps:state", JSON.stringify(uploadData['caracteristicas']));
    AsyncStorage.setItem("@excavationPhotos:state", JSON.stringify(uploadData['photos']));
    
    const list = await AsyncStorage.getItem("@uploadingExcavation:list");
    let arr = JSON.parse(list);
    arr.splice(index,1);
    AsyncStorage.setItem("@uploadingExcavation:list", JSON.stringify(arr));
    AsyncStorage.removeItem(form_key);
    navigation.navigate("ExcavationMainForm");
  }

  const uploadData2 = async (form_key, index) => {
    let uploadData = await AsyncStorage.getItem(form_key);
    uploadData = JSON.parse(uploadData);
    realDb.ref('monitor').child(form_key).push({
      monitor: uploadData['monitor'],
      vertex: uploadData['vertex'],
      finding: uploadData['archaeological'],
      created_at: new Date().toUTCString()
    });
    
    let mime = 'image/jpg';
    let temp = uploadData['photos'];
    if(temp){
      Object.keys(temp).map((key, index) => {
      temp[key].map((item, index) => {
        const image = item.image;
        if(image){
          let uploadBlob = null;
          let imageRef = storage.ref(form_key).child(`${key}/${index}.jpg`);
          fs.readFile(image, 'base64')
            .then((data) => {
              return Blob.build(data, { type: `image/png;BASE64` })
          })
          .then((blob) => {
              uploadBlob = blob
              return imageRef.put(blob, { contentType: 'image/png' })
          })
          .then(() => {
            uploadBlob.close()
            return imageRef.getDownloadURL()
          })
          .catch((error) => {
            console.log(error);
          })
        }
      })
    })
  }
    navigation.navigate("Home");
    //deleteData2(form_key, index)
  }

  const deleteData2 = async(form_key, index) => {
    const list = await AsyncStorage.getItem("@uploadingMonitor:list");
    let arr = JSON.parse(list);
    arr.splice(index,1);
    setUploadMonitorList(arr);
    AsyncStorage.setItem("@uploadingMonitor:list", JSON.stringify(arr));
    AsyncStorage.removeItem(form_key);
  }

  const editData2 = async(form_key, index) => {
    let uploadData = await AsyncStorage.getItem(form_key);
    console.log(form_key);
    uploadData = JSON.parse(uploadData);
    AsyncStorage.setItem("@monitor:state", JSON.stringify(uploadData['monitor']));
    AsyncStorage.setItem("@vertex:state", JSON.stringify(uploadData['vertex']));
    AsyncStorage.setItem("@archaeological:state", JSON.stringify(uploadData['archaeological']));
    AsyncStorage.setItem("@monitorPhotos:state", JSON.stringify(uploadData['photos']));
    // console.log(uploadData)
    const list = await AsyncStorage.getItem("@uploadingMonitor:list");
    let arr = JSON.parse(list);
    arr.splice(index,1);
    AsyncStorage.setItem("@uploadingMonitor:list", JSON.stringify(arr));
    AsyncStorage.removeItem(form_key);
    navigation.navigate("MonitorMainForm");
  }

  return (
    <ImageBackground source={background.image} style={styles.image}>
      <View style={styles.container}>
        <Spinner visible={spinner} textStyle={{ color: AppStyles.color.white }} textContent="Subiendo imágenes..."/>
        <TouchableOpacity style={AppIcon.iconContainer} 
            onPress={() => navigation.navigate("Home")}
        >
            <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
    </View>
    <View style={styles.mainContainer}>
        <ScrollView>
            <View style={{marginBottom: 30}}>
              {
                uploadList.length?
                <>
                  <Text style={styles.buttonGroupTitle}>
                    lista de sitios arqueológicos
                  </Text>
                  {list()}
                </>:
                <></>
              }{
                uploadExcavationList.length?
                <>
                  <Text style={styles.buttonGroupTitle}>
                    lista de excavación
                  </Text>
                  {excavationList()}
                </>:<>
                </>
              }
              {
                uploadMonitorList.length?
                <>
                  <Text style={styles.buttonGroupTitle}>
                    lista de monitoreo
                  </Text>
                  {monitorList()}
                </>:<>
                </>
              }
            </View>
        </ScrollView>
    </View>
  </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  mainContainer: {
    height: vh(80),
    alignItems: "center",
    backgroundColor: '#ffffff',
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  buttonGroupTitle: {
    textAlign:'center',
    fontSize: 20,
    color: '#5C5C5C',
    fontWeight: 'bold',
    marginBottom: 30,
    marginTop: 20
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  iconContainer: {
    position: 'absolute',
    top: 30,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white
  },
  buttonText: {
    fontSize: 16,
    color: AppStyles.color.grey
  },
  listContainer:{
    width: vw(70),
    padding:10,
    borderRadius:20,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "#5C5C5C",
    justifyContent: 'space-between'
  },
  listText:{
    textAlign: 'center',
    borderRadius:5,
  },
  listUploadButton:{
    marginTop: 20,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5", 
    borderWidth: 1,
    borderColor:'#CECECE',
    backgroundColor: '#ffffff',
  },
  listButtonContainer:{
      display:'flex',
      flexDirection: 'row',
      justifyContent: 'space-around'
  },
  buttonText: {
    fontSize:13,
    color: AppStyles.color.grey
  },
});


 
export default UploadListScreen;