import React, { useState, useEffect } from "react";
import Button from "react-native-button";
import AsyncStorage from "@react-native-community/async-storage";
import RNFetchBlob from "react-native-fetch-blob";
import Spinner from "react-native-loading-spinner-overlay";
import { vw } from "react-native-css-vh-vw";
import Icon from "react-native-vector-icons/Ionicons";

import { AppStyles, AppIcon } from "../../../AppStyles";
import { realDb, storage } from "../../../api";
import { background } from '../../../assets';

import {
  ImageBackground,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from "react-native";

const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;
const Fetch = RNFetchBlob.polyfill.Fetch;
window.fetch = new Fetch({
  auto: true,
  binaryContentTypes: ["image/", "video/", "audio/", "foo/"],
}).build();

const UploadScreen = ({ navigation }) => {
  const [state, setState] = useState({
    email: "",
    password: "",
    formKey: "",
  });

  const [spinner, setSpinnerData] = useState(false);
  const [checksubida, setChecksubida] = useState(true);

  useEffect(() => {
    (async () => {
      const username = await AsyncStorage.getItem("@loggedInUserID:username");
      const form_id = username + "_02_" + Number(new Date());
      setState({ ...state, formKey: form_id });
    })();
  }, []);

  function checkFicha(data){

    var err = "";
    var mainevents2 = data.mainevents1;
    var caract2 = data.caracteristicas1;

    //Pantalla 1
    if(mainevents2.site == "" || mainevents2.site == " "){
      err += "\n" + "Sitio";
    }

    if(mainevents2.structure == "" || mainevents2.structure == " "){
      err += "\n" + "Sector/Esctructura";
    }

    if(mainevents2.unit == "" || mainevents2.unit == " "){
      err += "\n" + "Unidad";
    }

    if(mainevents2.methodology == "" || mainevents2.methodology == " "){
      err += "\n" + "Metodologia";
    }

    if(mainevents2.measurements.height == "" || mainevents2.measurements.height == " "){
      err += "\n" + "Medidas alto";
    }

    if(mainevents2.measurements.width == "" || mainevents2.measurements.width == " "){
      err += "\n" + "Medidas largo";
    }

    if(mainevents2.measurements.depth == "" || mainevents2.measurements.depth == " "){
      err += "\n" + "Medidas profundidad";
    }

    //Nivel / Capa

    var looplvl = 1;
    var loopcapa = 1;
    caract2.forEach(nivel => {
      var errlvl = "";
      if(nivel["levelName"] == "" || nivel["levelName"] == " "){
        errlvl += "\n" + "Nivel " + looplvl + ": ";
        errlvl += "\n" + "   Nombre del nivel";
      }
      for (let capas in nivel["capas"]) {
        var capname = "";
        var errcapa = "";
        var loopmaterial = 1;
        if(nivel["capas"][capas]["layerData"]["Nombre"] == "" || nivel["capas"][capas]["layerData"]["Nombre"] == " "){
          if(errlvl == ""){
            errlvl += "\n" + "Nivel " + looplvl + ": ";
          }
          
          capname = "Capa " + loopcapa;
        }else{
          capname = nivel["capas"][capas]["layerData"]["Nombre"];
        }
        
        if(nivel["capas"][capas]["layerData"]["Características_Depositacionales"] == "" 
        || nivel["capas"][capas]["layerData"]["Características_Depositacionales"] == " "){
          if(errlvl == ""){
            errlvl += "\n" + "Nivel " + looplvl + ": ";
          }
          errcapa = "\n" + "   " + capname + ":";
          errcapa += "\n" + "             Características depositacionales";
        }

        if(nivel["capas"][capas]["layerData"]["Propiedades_Matriz"] == "" 
        || nivel["capas"][capas]["layerData"]["Propiedades_Matriz"] == " "){
          if(errlvl == ""){
            errlvl += "\n" + "Nivel " + looplvl + ": ";
          }
          if(errcapa == ""){
            errcapa = "\n" + "   " + capname + ":";
          }
          errcapa += "\n" + "             Propiedades de la matriz";
        }

        if(nivel["capas"][capas]["layerData"]["Espesor"] == "" 
        || nivel["capas"][capas]["layerData"]["Espesor"] == " "){
          if(errlvl == ""){
            errlvl += "\n" + "Nivel " + looplvl + ": ";
          }
          if(errcapa == ""){
            errcapa = "\n" + "   " + capname + ":";
          }
          errcapa += "\n" + "             Espesor";
        }

        for (let material in nivel["capas"][capas]["layerData"]["Materials"]) {
          if(nivel["capas"][capas]["layerData"]["Materials"][material]["Material_Cultural"] == ""){
            if(errlvl == ""){
              errlvl += "\n" + "Nivel " + looplvl + ": ";
            }
            if(errcapa == ""){
              errcapa = "\n" + "   " + capname + ":";
            }
            errcapa += "\n" + "             Material cultural " + loopmaterial;
          }

          if(nivel["capas"][capas]["layerData"]["Materials"][material]["Frecuencia"] == ""
          || nivel["capas"][capas]["layerData"]["Materials"][material]["Frecuencia"] == " "){
            if(errlvl == ""){
              errlvl += "\n" + "Nivel " + looplvl + ": ";
            }
            if(errcapa == ""){
              errcapa = "\n" + "   " + capname + ":";
            }
            errcapa += "\n" + "             Frecuencia " + loopmaterial;
          }
          loopmaterial++;
        }
        
        if(errcapa != ""){
          errlvl+= errcapa;
        }

        loopcapa++;
      }
      looplvl++;
      if(errlvl != ""){
        err+= errlvl;
      }
    });

    return err;
  }

  function postImagenesApi(data){
      return fetch('https://maa-desa.kolossus.tech/api/subirfotosexca',{
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          ficha: data.ficha,
          nombre: data.nombre,
          index: data.index,
          imagen: data.imagen
        })
      }).then((response) => response.json())
  };

  function getResponseImage(data){
    return Promise.all([postImagenesApi(data)]);
  }

  const uploadHandler = async () => {
    const photos = await AsyncStorage.getItem("@excavationPhotos:state");
    let temp = JSON.parse(photos);
    Object.keys(temp).map((key, index) => {
      temp[key].map((item, index) => {
      const image = item.image;
      if(image) {
        fs.readStream(image, "base64", 102399)
          .then((data) => {
            setSpinnerData(true);
            data.open()
            data.onData((chunk) => {
              data += chunk
            })
            data.onError((err) => {
              setChecksubida(false);
              alert(err);
              navigation.navigate("ExcavationMainForm");
              return false;
            })
            data.onEnd(() => {
              var json = {
                'ficha': state.formKey,
                'nombre': key,
                'index': index,
                'imagen': data
              };
              getResponseImage(json)
              .then(([imagenes]) => {
                if(imagenes.status == "ER"){
                  setChecksubida(false);
                  alert(imagenes.response);
                  navigation.navigate("ExcavationMainForm");
                  return false;
                }
              });
            })
          })
          .catch((error) => {
              setChecksubida(false);
              alert(error);
              navigation.navigate("ExcavationMainForm");
              return false;
          });
        }
      });
    });
    return true;
  }

  const uploadServer = async () => {
    setChecksubida(true);
    const mainevents = await AsyncStorage.getItem("@mainevents:state");
    const eventsdetail = await AsyncStorage.getItem("@eventsdetail:state");
    const caracteristicas = await AsyncStorage.getItem("@NivelCaps:state");
    const photos = await AsyncStorage.getItem("@excavationPhotos:state");
    let carc = JSON.parse(caracteristicas);

    const temp1 = {
      mainevents1: JSON.parse(mainevents),
      caracteristicas1: JSON.parse(caracteristicas)
    };

    var check = checkFicha(temp1);

    if(carc.length == 0){
      alert("La ficha de excavacion debe tener al menos 1 nivel y capa.");
      navigation.navigate("ExcavationMainForm");
    }else if(check != ""){
      alert("Se deben completar los siguientes campos: " + "\n" + check);
      navigation.navigate("ExcavationMainForm");
    }else{
     let temp = JSON.parse(photos);

      if(temp != null){
        if(temp.length == 0){
          alert("Debe agregar al menos 1 imagen en un nivel o capa.");
          navigation.navigate("ExcavationMainForm");
        }else{
          await uploadHandler();
          setTimeout(function() { 
            setSpinnerData(false);
            if(checksubida){
            realDb
              .ref("excavation")
              .child(state.formKey)
              .push({
                mainevents: JSON.parse(mainevents),
                eventsdetail: JSON.parse(eventsdetail),
                caracteristicas: JSON.parse(caracteristicas),
                created_at: new Date().toUTCString(),
              });
              
              alert("Se subio la ficha con ID: " + state.formKey);

              navigation.navigate("Home");
              savePhone();
              
          /*  AsyncStorage.removeItem("@mainevents:state");
              AsyncStorage.removeItem("@eventsdetail:state");
              AsyncStorage.removeItem("@NivelCaps:state");
              AsyncStorage.removeItem("@excavationPhotos:state"); */
          }else{
            alert("Error al subir la ficha, intente nuevamente.");
            navigation.navigate("ExcavationMainForm");
          }
        }, 3000);
        }
      }
    }
  };

  const savePhone = async () => {

    const username = await AsyncStorage.getItem("@loggedInUserID:username");
    const form_id = username + "_02_" + Number(new Date());
    setState({ ...state, formKey: form_id });

    const mainevents = await AsyncStorage.getItem("@mainevents:state");
    const eventsdetail = await AsyncStorage.getItem("@eventsdetail:state");
    const caracteristicas = await AsyncStorage.getItem("@NivelCaps:state");
    const photos = await AsyncStorage.getItem("@excavationPhotos:state");

    let tempp = JSON.parse(photos);

    if(tempp == null){
      alert("Debe agregar al menos 1 imagen en un nivel o capa.");
      navigation.navigate("ExcavationMainForm");
    }else{
      const temp = {
        mainevents: JSON.parse(mainevents),
        eventsdetail: JSON.parse(eventsdetail),
        caracteristicas: JSON.parse(caracteristicas),
        photos: JSON.parse(photos),
      };
      

      var mainevents1 =  JSON.parse(mainevents);
  
      var unit1 = mainevents1.unit;
      var site1 = mainevents1.site;

      const uploading_list = await AsyncStorage.getItem("@uploadingExcavation:list");
  
      if (uploading_list) {
        const existList = JSON.parse(uploading_list);
        let key = existList.length;
  
        existList[key] = {
          key: state.formKey,
          unit: unit1,
          site: site1
        };
  
        AsyncStorage.setItem("@uploadingExcavation:list", JSON.stringify(existList));
  
      } else {
        const temp = [];
  
        temp[0] = {
          key: state.formKey,
          unit: unit1,
          site: site1
        };
  
        AsyncStorage.setItem("@uploadingExcavation:list", JSON.stringify(temp));
      }
  
      AsyncStorage.setItem(state.formKey, JSON.stringify(temp));
  
      navigation.navigate("Home");
      AsyncStorage.removeItem("@mainevents:state");
      AsyncStorage.removeItem("@eventsdetail:state");
      AsyncStorage.removeItem("@NivelCaps:state");
      AsyncStorage.removeItem("@excavationPhotos:state");
    }
  };

  return (
    <ImageBackground
      source={background.image}
      style={styles.image}
    >
      <View style={styles.container}>
        <Spinner visible={spinner} textStyle={{ color: AppStyles.color.white }} textContent="Subiendo imágenes..."/>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
      </View>
      <View style={styles.mainContainer}>
        <Text style={styles.buttonGroupTitle}>{state.formKey}</Text>
        <Button
          containerStyle={styles.buttonContainer}
          style={[styles.buttonText, { color: "#5C5C5C" }]}
          onPress={uploadServer}
        >
          Subir al servidor
        </Button>
        <Button
          containerStyle={styles.buttonContainer}
          style={[styles.buttonText, { color: "#5C5C5C" }]}
          onPress={savePhone}
        >
          Guardar en el teléfono
        </Button>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  mainContainer: {
    alignItems: "center",
    backgroundColor: "#f8f8f8",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  buttonGroupTitle: {
    fontSize: 20,
    color: "#5C5C5C",
    fontWeight: "bold",
    marginBottom: 30,
    marginTop: 20,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  iconContainer: {
    position: "absolute",
    top: 30,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white,
  },
  buttonContainer: {
    width: vw(80),
    marginBottom: 30,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    marginTop: 30,
    borderWidth: 1,
    borderColor: "#CECECE",
    backgroundColor: "#ffffff",
  },
  buttonText: {
    fontSize: 16,
    color: AppStyles.color.grey,
  },
});

export default UploadScreen;
