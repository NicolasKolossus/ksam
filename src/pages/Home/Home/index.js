import React, { useEffect, useState } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import MaterialIconsIcon from "react-native-vector-icons/SimpleLineIcons";
import Button from "react-native-button";
import { vw } from "react-native-css-vh-vw";
import Spinner from "react-native-loading-spinner-overlay";

import { AppIcon, AppStyles, appVersion } from "../../../AppStyles";
import { background } from '../../../assets';

import {
  TouchableOpacity,
  StyleSheet,
  View,
  Image,
  ImageBackground,
  Text,
} from "react-native";


const HomeScreen = ({ navigation }) => {

  const [spinner, setSpinnerData] = useState(false);

  const viewListButtonClick = async () => {
    const sitelist = await AsyncStorage.getItem("@uploading:list");
    const excavationlist = await AsyncStorage.getItem("@uploadingExcavation:list");
    const monitorlist = await AsyncStorage.getItem("@uploadingMonitor:list");
    console.log(monitorlist);
    
    if (!sitelist && !excavationlist && ! monitorlist) {
      alert("No tienes ninguna lista");
      return;
    }

    let sitelistTemp = JSON.parse(sitelist);
    let excavationlistTemp = JSON.parse(excavationlist);
    let monitorlistTemp = JSON.parse(monitorlist);
    if(!sitelistTemp) {
      sitelistTemp = {};
      AsyncStorage.setItem("@uploading:list", JSON.stringify([]));
    }
    if(!excavationlistTemp) {
      excavationlistTemp = {};
      AsyncStorage.setItem("@uploadingExcavation:list", JSON.stringify([]));
    }
    if(!monitorlistTemp) {
      monitorlistTemp = {};
      AsyncStorage.setItem("@uploadingMonitor:list", JSON.stringify([]));
    }
    if (!sitelistTemp.length && !excavationlistTemp.length && !monitorlistTemp.length) {
      alert("No tienes ninguna lista");
      return;
    }
    navigation.navigate("UploadList");
  };

  const registerSiteButtonClick = () => {
    navigation.navigate("ArchForm");
  };

  const registerExcavationButtonClick = () => {
    navigation.navigate("ExcavationMainForm");
  };

  const registerMonitorButtonClick = () => {
    navigation.navigate("MonitorMainForm");
  }

  return (
    <ImageBackground
      source={background.image}
      style={styles.image}
    >
      <View style={styles.container}>
        <Spinner
          visible={spinner}
          textContent="uploading..."
          textStyle={styles.spinnerTextStyle}
        />
        <TouchableOpacity
          style={AppIcon.logoutContainer}
          onPress={() => navigation.dispatch({ type: "Logout", user: {} })}
        >
          <MaterialIconsIcon name="logout" size={25} color="#ffffff" />
        </TouchableOpacity>
        <Image source={background.logo} />
        <View style={styles.buttonGroup}>
          <Text style={styles.buttonGroupTitle}>Bienvenido</Text>
          <Button
            containerStyle={styles.buttonContainer}
            style={styles.buttonText}
            onPress={() => registerSiteButtonClick()}
          >
            Registro de sitios arqueológicos
          </Button>
          <Button
            containerStyle={styles.buttonContainer}
            style={styles.buttonText}
            onPress={() => registerExcavationButtonClick()}
          >
            Registro de excavaciones
          </Button>
          <Button
            containerStyle={styles.buttonContainer}
            style={styles.buttonText}
            onPress={() => registerMonitorButtonClick()}
          >
            Registro de Monitoreo
          </Button>
          <Button
            containerStyle={styles.buttonContainer}
            style={styles.buttonText}
            onPress={() => viewListButtonClick()}
          >
            Ver lista de carga
          </Button>
          <Text>Versión : {appVersion}</Text>
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  buttonGroup: {
    alignItems: "center",
    width: vw(100),
    position: "absolute",
    paddingBottom: 20,
    paddingTop: 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    bottom: 0,
    backgroundColor: AppStyles.color.white,
  },
  buttonGroupTitle: {
    fontSize: 20,
    color: "#5C5C5C",
    fontWeight: "bold",
    marginBottom: 10,
  },
  buttonContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: "#E5E5E5",
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 15,
  },
  buttonText: {
    color: AppStyles.color.grey,
  }
});

export default HomeScreen;
