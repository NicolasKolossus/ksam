import React, { useState, useEffect } from "react";
import Button from "react-native-button";
import AsyncStorage from "@react-native-community/async-storage";
import RNFetchBlob from "react-native-fetch-blob";
import Spinner from "react-native-loading-spinner-overlay";
import { vw } from "react-native-css-vh-vw";
import Icon from "react-native-vector-icons/Ionicons";

import { AppStyles, AppIcon } from "../../../AppStyles";
import { realDb, storage } from "../../../api";
import { background } from '../../../assets';

import {
  ImageBackground,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from "react-native";

const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;
const Fetch = RNFetchBlob.polyfill.Fetch;
window.fetch = new Fetch({
  auto: true,
  binaryContentTypes: ["image/", "video/", "audio/", "foo/"],
}).build();

const UploadScreen = ({ navigation }) => {
  const [state, setState] = useState({
    email: "",
    password: "",
    formKey: "",
  });

  useEffect(() => {
    (async () => {
      const username = await AsyncStorage.getItem("@loggedInUserID:username");
      const form_id = username + "_01_" + Number(new Date());
      setState({ ...state, formKey: form_id });
    })();
  }, []);

  function checkFicha(data){

    err = "";
    var principal = data.principal;
    var coordinates = data.coordinates;
    var caract = data.caracteristicas1;

    //Principal
    if(principal.siteType == ""){
      err += "\n" + "Tipo de sitio";
    }

    if(principal.siteType == "Otro" && (principal.otroValue == "" || principal.otroValue == " ")){
      err += "\n" + "Tipo de sitio";
    }

    if(principal.geomorphological == "" || principal.geomorphological == " "){
      err += "\n" + "Ubicación Geomorfológica";
    }

    if(principal.description == "" || principal.description == " "){
      err += "\n" + "Descripción del sitio";
    }

    //Coordenadas

    if(coordinates == null){
      err += "\n" + "Coordenadas";
    }else{
      var loop = 1;
      coordinates.forEach(obj => {
        if(obj["A_E"] == "" || obj["A_N"] == "" || obj["A_msnm"] == ""){
          err += "\n" + "Set de coordenadas " + loop + ": ";
          if(obj["A_E"] == ""){
            err += "\n" + "   Punto Este";
          }
          if(obj["A_N"] == ""){
            err += "\n" + "   Punto Norte";
          }
          if(obj["A_msnm"] == ""){
            err += "\n" + "   Punto m.s.n.m";
          }
        }
        loop++;
      });
    }

    if(caract !=null){
      if(caract.dimensions == "" || caract.dimensions == " "){
        err += "\n" + "Dimensiones";
      }
      if(caract.relativeChronology == ""){
        err += "\n" + "Cronología Relativa";
      }
      for (let key in caract.culturalMaterial) {
        if(caract.culturalMaterial[key]["Material"] == ""){
          err += "\n" + "Material Cultural " + (parseInt(key) + 1);
        }
        if(caract.culturalMaterial[key]["Material"] == "Otro" && (caract.culturalMaterial[key]["OtroValue"] == "" || caract.culturalMaterial[key]["OtroValue"] == " ")){
          err += "\n" + "Material Cultural " + (parseInt(key) + 1);
        }
      }
    }else{
      err += "\n" + "Dimensiones";
      err += "\n" + "Cronología Relativa";
      err += "\n" + "Material Cultural 1";
    }

    return err;
  }

  const uploadServer = async () => {
    const principal = await AsyncStorage.getItem("@principal:state");
    const coordinates = await AsyncStorage.getItem("@coordinates:state");
    const caracteristicas = await AsyncStorage.getItem(
      "@caracteristicas:state"
    );
    
    const temp1 = {
      principal: JSON.parse(principal),
      coordinates: JSON.parse(coordinates),
      caracteristicas1: JSON.parse(caracteristicas)
    };

    var check = checkFicha(temp1);
    
    if(check != ""){
      alert("Se deben completar los siguientes campos: " + "\n" + check);
      navigation.navigate("ArchForm");
    }else{
      realDb
      .ref("archaeologicalSites")
      .child(state.formKey)
      .push({
        principal: JSON.parse(principal),
        coordinates: JSON.parse(coordinates),
        caracteristicas: JSON.parse(caracteristicas),
        created_at: new Date().toUTCString(),
      });
      const photos = await AsyncStorage.getItem("@photos:state");
      let mime = "image/jpg";
      let temp = JSON.parse(photos);
      if(temp){
        temp.map((item, index) => {
          const image = item.image;
          let uploadBlob = null;
          let imageRef = storage.ref(state.formKey).child(`${index}.jpg`);
    
          if (image) {
            fs.readFile(image, "base64")
              .then((data) => {
                return Blob.build(data, { type: `image/png;BASE64` });
              })
              .then((blob) => {
                uploadBlob = blob;
                return imageRef.put(blob, { contentType: 'image/png' });
              })
              .then(() => {
                uploadBlob.close();
                return imageRef.getDownloadURL();
              })
              .catch((error) => {
                console.log(error);
              });
          }
        });
      }
  
      alert("Se subio la ficha con ID: " + state.formKey);
      
      savePhone();
  
      // navigation.navigate("Home");
      // AsyncStorage.removeItem("@principal:state");
      // AsyncStorage.removeItem("@coordinates:state");
      // AsyncStorage.removeItem("@caracteristicas:state");
      // AsyncStorage.removeItem("@photos:state");
    }
  };

  const savePhone = async () => {

    const username = await AsyncStorage.getItem("@loggedInUserID:username");
    const form_id = username + "_01_" + Number(new Date());
    setState({ ...state, formKey: form_id });

    const principal = await AsyncStorage.getItem("@principal:state");
    const coordinates = await AsyncStorage.getItem("@coordinates:state");
    const caracteristicas = await AsyncStorage.getItem(
      "@caracteristicas:state"
    );

    const photos = await AsyncStorage.getItem("@photos:state");

    const temp = {
      principal: JSON.parse(principal),
      coordinates: JSON.parse(coordinates),
      caracteristicas: JSON.parse(caracteristicas),
      photos: JSON.parse(photos),
    };

    const uploading_list = await AsyncStorage.getItem("@uploading:list");
    if (uploading_list) {
      const existList = JSON.parse(uploading_list);
      let key = existList.length;

      existList[key] = {
        key: state.formKey,
        siteType: temp.principal.siteType
      };
      
      AsyncStorage.setItem("@uploading:list", JSON.stringify(existList));
    } else {
      const temp1 = [];
      temp1[0] = {
        key: state.formKey,
        siteType: temp.principal.siteType
      };
      AsyncStorage.setItem("@uploading:list", JSON.stringify(temp1));
    }
    AsyncStorage.setItem(state.formKey, JSON.stringify(temp));
    navigation.navigate("Home");

    AsyncStorage.removeItem("@principal:state");
    AsyncStorage.removeItem("@coordinates:state");
    AsyncStorage.removeItem("@caracteristicas:state");
    AsyncStorage.removeItem("@photos:state");
  };

  return (
    <ImageBackground
      source={background.image}
      style={styles.image}
    >
      <View style={styles.container}>
        <Spinner visible={state.spinner} textStyle={{ color: AppStyles.color.white }} />
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
      </View>
      <View style={styles.mainContainer}>
        <Text style={styles.buttonGroupTitle}>{state.formKey}</Text>
        <Button
          containerStyle={styles.buttonContainer}
          style={[styles.buttonText, { color: "#5C5C5C" }]}
          onPress={uploadServer}
        >
          Subir al servidor
        </Button>
        <Button
          containerStyle={styles.buttonContainer}
          style={[styles.buttonText, { color: "#5C5C5C" }]}
          onPress={savePhone}
        >
          Guardar en el teléfono
        </Button>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  mainContainer: {
    alignItems: "center",
    backgroundColor: "#f8f8f8",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  buttonGroupTitle: {
    fontSize: 20,
    color: "#5C5C5C",
    fontWeight: "bold",
    marginBottom: 30,
    marginTop: 20,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  iconContainer: {
    position: "absolute",
    top: 30,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white,
  },
  buttonContainer: {
    width: vw(80),
    marginBottom: 30,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    marginTop: 30,
    borderWidth: 1,
    borderColor: "#CECECE",
    backgroundColor: "#ffffff",
  },
  buttonText: {
    fontSize: 16,
    color: AppStyles.color.grey,
  },
});

export default UploadScreen;
