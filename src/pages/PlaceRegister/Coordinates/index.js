import React, { useState, useEffect } from "react";
import { AppIcon, AppStyles } from "../../../AppStyles";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/Ionicons";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AntDesignIcon from "react-native-vector-icons/AntDesign";
import { background, header } from "../../../assets";
import AntIcon from "react-native-vector-icons/AntDesign";

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  TextInput,
} from "react-native";

const Coordinates = ({ navigation }) => {

  const [centrosState, setCentrosData] = useState({
    controE: "",
    controN: ""
  });

  const [coordinatesState, setCoordinatesState] = useState({
    A_E: "",
    B_E: "",
    C_E: "",
    D_E: "",
    A_N: "",
    B_N: "",
    C_N: "",
    D_N: "",
    A_msnm: "",
    B_msnm: "",
    C_msnm: "",
    D_msnm: "",
  });

  const [items, setItems] = useState([
    {
      A_E: "",
      B_E: "",
      C_E: "",
      D_E: "",
      A_N: "",
      B_N: "",
      C_N: "",
      D_N: "",
      A_msnm: "",
      B_msnm: "",
      C_msnm: "",
      D_msnm: "",
    }
  ]);

  const [currentIndex, setCurrentIndex] = useState(0);

  const [render, setRenderData] = useState(true);

  const addItem = () => {
    const temp = items;
    temp.push({
      A_E: "",
      B_E: "",
      C_E: "",
      D_E: "",
      A_N: "",
      B_N: "",
      C_N: "",
      D_N: "",
      A_msnm: "",
      B_msnm: "",
      C_msnm: "",
      D_msnm: "",
    });
    setItems(temp);
    AsyncStorage.setItem("@coordinates:state", JSON.stringify(temp));
    setRenderData(!render);
  };

  const deleteAll = () => {
    setItems([
      {
        A_E: "",
        B_E: "",
        C_E: "",
        D_E: "",
        A_N: "",
        B_N: "",
        C_N: "",
        D_N: "",
        A_msnm: "",
        B_msnm: "",
        C_msnm: "",
        D_msnm: "",
      },
    ]);
  };

  useEffect(() => {
    setItemByIndex(0);
  }, []);

  const setItemByIndex = async (index) => {
    setCurrentIndex(index);
    const n = await AsyncStorage.getItem("@centros:state");
    const temp = await AsyncStorage.getItem("@coordinates:state");
    const parsingTemp = JSON.parse(temp);
    setItems(parsingTemp);
    setCoordinatesState({
      ...coordinatesState,
      A_E: parsingTemp[index].A_E,
      B_E: parsingTemp[index].B_E,
      C_E: parsingTemp[index].C_E,
      D_E: parsingTemp[index].D_E,
      A_N: parsingTemp[index].A_N,
      B_N: parsingTemp[index].B_N,
      C_N: parsingTemp[index].C_N,
      D_N: parsingTemp[index].D_N,
      A_msnm: parsingTemp[index].A_msnm,
      B_msnm: parsingTemp[index].B_msnm,
      C_msnm: parsingTemp[index].C_msnm,
      D_msnm: parsingTemp[index].D_msnm,
    });
    if(JSON.parse(n)){
      setCentrosData(JSON.parse(n))
    }
  };

  useEffect(() => {
    const n = items;
    n[currentIndex] = coordinatesState;
    const temp = JSON.stringify(n);
    AsyncStorage.setItem("@coordinates:state", temp);
  }, [coordinatesState]);

  useEffect(() => {
    AsyncStorage.setItem("@centros:state", JSON.stringify(centrosState));
  }, [centrosState])

  const mainView = () => {
    return (
      <>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
        <TouchableOpacity
          style={AppIcon.imgContainer}
          onPress={() => navigation.navigate("UploadSite")}
        >
          <Image source={header.logo} />
        </TouchableOpacity>
        <View style={styles.mainContainer}>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text style={styles.mainContainerTitle}>Coordenadas</Text>
            <TouchableOpacity
              style={{ position: "absolute", right: 10 }}
              onPress={() => deleteAll()}
            >
              <AntDesignIcon name="delete" size={30} color="#5C5C5C" />
            </TouchableOpacity>
          </View>
          <View style={styles.CoordinatesHeader}>
            <Text style={styles.CoordinatesHeaderText}>Polígono</Text>
            <Text style={styles.CoordinatesHeaderText}>E</Text>
            <Text style={styles.CoordinatesHeaderText}>N</Text>
            <Text style={styles.CoordinatesHeaderText}>msnm</Text>
          </View>
          <View style={styles.CoordinatesInputContainer}>
            <Text style={styles.CoordinatesText}>A</Text>
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, A_E: text });
                }
              }}
              value={coordinatesState.A_E}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            { 
              coordinatesState.A_E == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, A_N: text });
                }
              }}
              value={coordinatesState.A_N}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            { 
              coordinatesState.A_N == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, A_msnm: text });
                }
              }}
              value={coordinatesState.A_msnm}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            { 
              coordinatesState.A_msnm == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
          </View>
          <View style={styles.CoordinatesInputContainer}>
            <Text style={styles.CoordinatesText}>B</Text>
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, B_E: text });
                }
              }}
              value={coordinatesState.B_E}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, B_N: text });
                }
              }}
              value={coordinatesState.B_N}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, B_msnm: text });
                }
              }}
              value={coordinatesState.B_msnm}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
          </View>
          <View style={styles.CoordinatesInputContainer}>
            <Text style={styles.CoordinatesText}>C</Text>
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, C_E: text });
                }
              }}
              value={coordinatesState.C_E}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, C_N: text });
                }
              }}
              value={coordinatesState.C_N}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, C_msnm: text });
                }
              }}
              value={coordinatesState.C_msnm}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
          </View>
          <View style={styles.CoordinatesInputContainer}>
            <Text style={styles.CoordinatesText}>D</Text>
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, D_E: text });
                }
              }}
              value={coordinatesState.D_E}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, D_N: text });
                }
              }}
              value={coordinatesState.D_N}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCoordinatesState({ ...coordinatesState, D_msnm: text });
                }
              }}
              value={coordinatesState.D_msnm}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
          </View>
          <View
            style={{
              width: "100%",
              backgroundColor: "#e8e8e8",
              height: 50,
              borderRadius: 30,
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignContent: "center",
              padding: 10,
            }}
          >
            {items.map((key, index) => (
              <TouchableOpacity
                onPress={() => setItemByIndex(index)}
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "gray",
                  width: 30,
                  height: 30,
                  borderRadius: 15,
                }}
              >
                <Text style={{ fontSize: 18 }}>{index + 1}</Text>
              </TouchableOpacity>
            ))}

            <TouchableOpacity
              onPress={() => addItem()}
              style={{
                backgroundColor: "rgba(27, 181, 92, 0.2)",
                borderRadius: 20,
              }}
            >
              <AntDesignIcon
                name="plus"
                size={30}
                color="rgba(27, 181, 92, 1)"
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
              marginTop: 20,
              marginBottom: 20,
              alignItems: "center",
            }}
          >
            <Text>centro_E:</Text>
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCentrosData({ ...centrosState, controE: text });
                }
              }}
              value={centrosState.controE}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            { 
              centrosState.controE == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
            <Text>centro_N:</Text>
            <TextInput
              style={styles.CoordinatesInput}
              onChangeText={(text) => {
                let num = text.replace(".", "");
                if (!isNaN(num)) {
                  setCentrosData({ ...centrosState, controN: text });
                }
              }}
              value={centrosState.controN}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
            { 
              centrosState.controN == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
          </View>
        </View>
      </>
    );
  };

  return (
    <ImageBackground source={background.image} style={styles.image}>
      <View style={styles.formContainer}>
        {Platform.OS === "android" ? (
          <ScrollView
            contentContainerStyle={{
              flexGrow: 1,
              justifyContent: "space-between",
              justifyContent: "flex-end",
            }}
          >
            {mainView()}
          </ScrollView>
        ) : (
          <KeyboardAwareScrollView
            contentContainerStyle={{
              flexGrow: 1,
              justifyContent: "space-between",
              justifyContent: "flex-end",
            }}
          >
            {mainView()}
          </KeyboardAwareScrollView>
        )}
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
  },
  CoordinatesHeader: {
    display: "flex",
    flexDirection: "row",
    marginTop: 20,
    marginBottom: 20,
    paddingTop: 10,
    paddingBottom: 10,
  },
  mainContainer: {
    backgroundColor: "#f8f8f8",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  mainContainerTitle: {
    display: "flex",
    flexDirection: "row",
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  imgContainer: {
    position: "absolute",
    top: 10,
    right: 20,
    width: 30,
  },
  iconContainer: {
    position: "absolute",
    top: 10,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white,
  },
  CoordinatesHeaderText: {
    textAlign: "center",
    fontSize: 15,
    width: "25%",
  },
  CoordinatesInputContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  CoordinatesInput:
    Platform.OS === "android"
      ? {
          width: "22%",
          borderTopLeftRadius: 30,
          borderTopRightRadius: 30,
          borderBottomLeftRadius: 30,
          borderBottomRightRadius: 30,
          borderWidth: 1,
          borderStyle: "solid",
          borderColor: "#5C5C5C",
          borderRadius: 2,
          backgroundColor: AppStyles.color.white,
          paddingLeft: 10,
          paddingRight: 10,
          color: AppStyles.color.text,
        }
      : {
          width: "22%",
          borderTopLeftRadius: 30,
          borderTopRightRadius: 30,
          borderBottomLeftRadius: 30,
          borderBottomRightRadius: 30,
          borderWidth: 1,
          borderStyle: "solid",
          borderColor: "#5C5C5C",
          borderRadius: 2,
          backgroundColor: AppStyles.color.white,
          paddingTop: 15,
          paddingBottom: 15,
          paddingLeft: 15,
          paddingRight: 15,
          color: AppStyles.color.text,
        },
  CoordinatesText: {
    textAlignVertical: "center",
    textAlign: "center",
    width: "22%",
    borderRadius: 2,
    paddingLeft: 10,
    paddingRight: 10,
    color: AppStyles.color.text,
  },
});

export default Coordinates;
