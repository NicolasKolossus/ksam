import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import Icon from "react-native-vector-icons/Ionicons";
import AntIcon from "react-native-vector-icons/AntDesign";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { AppStyles, InputContainerStyle, AppIcon } from "../../../AppStyles";
import DropDownPicker from "../../../components/react-native-dropdown-picker";
import { siteType, dataRegion, dataComunas } from "../../../service/util";
import { background, header } from "../../../assets";

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { Platform } from "react-native";

const Principal = ({ navigation }) => {
  const [principalState, setPrincipalState] = useState({
    siteType: "",
    region: "",
    common: "",
    geomorphological: "",
    description: "",
    otroValue: ""
  });

  const [otroState, setOtroState] = useState(false);
  const [comunasr, setComunasbyRegion] = useState(dataComunas);

  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@principal:state");
      const parsingTemp = JSON.parse(temp);
      if (parsingTemp.otroValue) {
        parsingTemp.siteType = "Otro";
      }
      if (temp) {
        setPrincipalState({
          ...principalState,
          siteType: parsingTemp.siteType,
          region: parsingTemp.region,
          common: parsingTemp.common,
          geomorphological: parsingTemp.geomorphological,
          description: parsingTemp.description,
          otroValue: parsingTemp.otroValue
        });
      } else {
        setPrincipalState({
          ...principalState
        });
      }
    })();
  }, []);

  const setRegion = (itemreg) => {
    setComunasbyRegion({});

    console.log(itemreg);
    setPrincipalState({
      ...principalState,
      region: itemreg.value,
    });

    var toSearch = itemreg.regionId;

    let temp1 = {};
    temp1 = dataComunas.filter(obj => {
      return obj.comunaRegionId === toSearch
    });

    setComunasbyRegion(temp1);

    //console.log(comunasr);

  };

  const mainView = () => {
    return (
      <>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
        <TouchableOpacity
          style={AppIcon.imgContainer}
          onPress={() => navigation.navigate("UploadSite")}
        >
          <Image source={header.logo} />
        </TouchableOpacity>
        <View style={styles.mainContainer}>
          <Text style={styles.mainContainerTitle}>Sitios arqueológicos</Text>
          <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
            <DropDownPicker
              items={siteType}
              placeholder="Tipo de sitio"
              containerStyle={{
                width: "100%",
                height: 54,
              }}
              defaultValue={principalState.siteType}
              style={{ backgroundColor: "#ffffff" }}
              itemStyle={{ justifyContent: "flex-start" }}
              dropDownStyle={{ backgroundColor: "#ffffff" }}
              onChangeItem={(item) => {
                if (item.value == "Otro") {
                  setPrincipalState({
                    ...principalState,
                    siteType: item.value,
                  });
                } else {
                  setPrincipalState({
                    ...principalState,
                    siteType: item.value,
                    otroValue: "",
                  });
                }
              }}
            />
            {(principalState.siteType == "") ? (
              <AntIcon
                name="warning"
                style={{ marginLeft: 2, alignSelf: "center" }}
                size={15}
                color="red"
              />
            ) : (
              <AntIcon
                name="checkcircleo"
                style={{ marginLeft: 2, alignSelf: "center" }}
                size={15}
                color="green"
              />
            )}
          </View>
          {principalState.siteType == "Otro" ? (
            <View
              style={{ display: "flex", flexDirection: "row", marginTop: 10 }}
            >
              <TextInput
                style={InputContainerStyle.body}
                placeholder="Otro"
                onChangeText={(text) =>
                  setPrincipalState({ ...principalState, otroValue: text })
                }
                value={principalState.otroValue}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
              {principalState.otroValue == "" ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
          ) : (
            <></>
          )}

          <View>
            <Text style={styles.mainFormGroupTitle}>Ubicación GeoPolítica</Text>
            <View style={{ marginBottom: 10 }}>
              <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
                <DropDownPicker
                  items={dataRegion}
                  placeholder="Región"
                  containerStyle={{
                    width: "100%",
                    height: 54,
                  }}
                  defaultValue={principalState.region}
                  style={{ backgroundColor: "#ffffff" }}
                  itemStyle={{ justifyContent: "flex-start" }}
                  dropDownStyle={{ backgroundColor: "#ffffff" }}
                  onChangeItem={(item) => {
                    setRegion(item);
                  }}
                />
              </View>
              <View style={{ marginTop: 10 }}>
                <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
                  <DropDownPicker
                    items={comunasr}
                    placeholder="Comuna"
                    containerStyle={{
                      height: 54,
                      width: "100%",
                    }}
                    defaultValue={principalState.common}
                    style={{ backgroundColor: "#ffffff" }}
                    itemStyle={{ justifyContent: "flex-start" }}
                    dropDownStyle={{ backgroundColor: "#ffffff" }}
                    onChangeItem={(item) => {
                      setPrincipalState({
                        ...principalState,
                        common: item.value,
                      });
                    }}
                  />
                </View>
                <View
                  style={[
                    InputContainerStyle.InputContainer,
                    { marginTop: 50 },
                  ]}
                >
                  <View style={{ display: "flex", flexDirection: "row" }}>
                    <TextInput
                      style={InputContainerStyle.body}
                      placeholder="Ubicación Geomorfológica"
                      onChangeText={(text) =>
                        setPrincipalState({
                          ...principalState,
                          geomorphological: text,
                        })
                      }
                      value={principalState.geomorphological}
                      placeholderTextColor={AppStyles.color.grey}
                      underlineColorAndroid="transparent"
                    />
                    {principalState.geomorphological == "" ? (
                      <AntIcon
                        name="warning"
                        style={{ marginLeft: 2, alignSelf: "center" }}
                        size={15}
                        color="red"
                      />
                    ) : (
                      <AntIcon
                        name="checkcircleo"
                        style={{ marginLeft: 2, alignSelf: "center" }}
                        size={15}
                        color="green"
                      />
                    )}
                  </View>
                </View>
                <View style={InputContainerStyle.InputContainer}>
                  <View style={{ display: "flex", flexDirection: "row" }}>
                    <TextInput
                      style={InputContainerStyle.body}
                      placeholder="Descripción del Sitio"
                      onChangeText={(text) =>
                        setPrincipalState({
                          ...principalState,
                          description: text,
                        })
                      }
                      value={principalState.description}
                      placeholderTextColor={AppStyles.color.grey}
                      underlineColorAndroid="transparent"
                    />
                    {principalState.description == "" ? (
                      <AntIcon
                        name="warning"
                        style={{ marginLeft: 2, alignSelf: "center" }}
                        size={15}
                        color="red"
                      />
                    ) : (
                      <AntIcon
                        name="checkcircleo"
                        style={{ marginLeft: 2, alignSelf: "center" }}
                        size={15}
                        color="green"
                      />
                    )}
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </>
    );
  };

  useEffect(() => {
    const temp = JSON.stringify(principalState);
    AsyncStorage.setItem("@principal:state", temp);
  }, [principalState]);

  return (
    <ImageBackground source={background.image} style={AppStyles.image}>
      <View style={styles.formContainer}>
        {Platform.OS === "android" ? (
          <ScrollView
            contentContainerStyle={{
              flexGrow: 1,
              justifyContent: "space-between",
              justifyContent: "flex-end",
            }}
          >
            {mainView()}
          </ScrollView>
        ) : (
          <KeyboardAwareScrollView
            contentContainerStyle={{
              flexGrow: 1,
              justifyContent: "space-between",
              justifyContent: "flex-end",
            }}
          >
            {mainView()}
          </KeyboardAwareScrollView>
        )}
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
  },
  mainContainer: {
    backgroundColor: "#f8f8f8",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  mainContainerTitle: {
    paddingTop: 10,
    paddingBottom: 20,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  mainFormGroupTitle: {
    paddingTop: 20,
    paddingBottom: 10,
    paddingLeft: 15,
    textAlign: "left",
    color: "#5C5C5C",
    fontSize: 16,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: AppStyles.color.blue,
    marginTop: 10,
    textAlign: "left",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  iconContainer: {
    position: "absolute",
    top: 50,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white,
  },
  dropdownWrap: {
    display: "flex", flexDirection: "row"
  }
});

export default Principal;
