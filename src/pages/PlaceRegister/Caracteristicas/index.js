import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/Ionicons";
import { background, header } from "../../../assets";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AntIcon from "react-native-vector-icons/AntDesign";
import ModalSelector from 'react-native-modal-selector';

import {
  dataCronologia,
  dataMaterial,
  dataEstado,
  dataAgentes,
  dataTipoRegistros,
} from "../../../service/util";

import DropDownPicker from "../../../components/react-native-dropdown-picker";
import { AppStyles, InputContainerStyle, AppIcon } from "../../../AppStyles";

import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
  Image,
  SafeAreaView,
  TextInput,
} from "react-native";

const Caracteristicas = ({ navigation }) => {
  const [CaracteristicasState, setCaracteristicasState] = useState({
    dimensions: "",
    relativeChronology: "",
    culturalMaterial : {
      0 : {
        Material : "",
        OtroValue : ""
      }
    },
    stateConservation: "",
    agentsAffection: "",
    affectingFactors: "",
    observations: "",
    TypeRegister: "",
  });

  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@caracteristicas:state");
      const parsingTemp = JSON.parse(temp);
      setCaracteristicasState({
        ...CaracteristicasState,
        dimensions: parsingTemp.dimensions,
        relativeChronology: parsingTemp.relativeChronology,
        culturalMaterial: parsingTemp.culturalMaterial,
        stateConservation: parsingTemp.stateConservation,
        agentsAffection: parsingTemp.agentsAffection,
        affectingFactors: parsingTemp.affectingFactors,
        observations: parsingTemp.observations,
        TypeRegister: parsingTemp.TypeRegister,
        otroValue: parsingTemp.otroValue,
      });
    })();
  }, []);

  useEffect(() => {
    const temp = JSON.stringify(CaracteristicasState);
    AsyncStorage.setItem("@caracteristicas:state", temp);
  }, [CaracteristicasState]);

  const onClickAdd = () => {
    var obj = CaracteristicasState.culturalMaterial
    var key = (Object.keys(CaracteristicasState.culturalMaterial).length);
    var arr = {};

    arr[key] = {
        Material : "",
        OtroValue : ""
    };

    obj = {...obj, ...arr};

    setCaracteristicasState({
      ...CaracteristicasState,
      culturalMaterial: obj,
    });

  };

  const saveMaterial = (index, material, otro) => {
    var obj = CaracteristicasState.culturalMaterial
    var arr = {};

    arr[index] = {
        Material : material,
        OtroValue : otro
    };

    obj = {...obj, ...arr};

    setCaracteristicasState({
      ...CaracteristicasState,
      culturalMaterial: obj,
    });

  };

  const mainBody = () => {
    return (
      <View style={{paddingLeft: 30,paddingRight: 30,}}>
        <Text style={styles.mainContainerTitle}>Caracteristicas</Text>
          <View style={{display: 'flex', flexDirection:'row', marginBottom:10}}>
            <TextInput
              style={InputContainerStyle.body}
              placeholder="Dimensiones"
              onChangeText={(text) =>
                setCaracteristicasState({
                  ...CaracteristicasState,
                  dimensions: text,
                })
              }
              value={CaracteristicasState.dimensions}
              placeholderTextColor={AppStyles.color.grey}
            />
            { 
              CaracteristicasState.dimensions == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
        </View>
        <View>
          <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
            <DropDownPicker
              items={dataCronologia}
              placeholder="Cronologia Relativa"
              containerStyle={{
                height: 54,
                width: '100%'
              }}
              defaultValue={CaracteristicasState.relativeChronology}
              style={{ backgroundColor: AppStyles.color.white }}
              itemStyle={{ justifyContent: "flex-start" }}
              dropDownStyle={{ backgroundColor: AppStyles.color.white }}
              onChangeItem={(item) => {
                setCaracteristicasState({
                  ...CaracteristicasState,
                  relativeChronology: item.value,
                });
              }}
            />
            { 
              CaracteristicasState.relativeChronology == ""?
              <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
              :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
            }
          </View>
          <View style={{ marginTop: 10 }}>
          {Object.keys(CaracteristicasState.culturalMaterial).map((key) => {
          return (
            <View
                key={
                  "materialunq" + "_" + key
                }
                style={[Platform.OS !== "android" ? { zIndex:1} : null]}
              >
              <View style={styles.titleLabel}>
                <Text>Material Cultural</Text>
              </View>
              <ModalSelector
                data={dataMaterial}
                keyExtractor= {item => item.materialId}
                labelExtractor= {item => item.label}
                initValue="Material Cultural"
                supportedOrientations={['portrait']}
                onChange={(item) => {
                  saveMaterial(key, item.label, "");
                }}>
                <TextInput
                  style={InputContainerStyle.body}
                  editable={false}
                  placeholder="Material Cultural"
                  underlineColorAndroid="transparent"
                  placeholderTextColor={AppStyles.color.grey}
                  value={CaracteristicasState.culturalMaterial[key].Material} />
                
              </ModalSelector>
              {CaracteristicasState.culturalMaterial[key].Material == "Otro" ? (
                <View
                  style={{ display: "flex", flexDirection: "row", marginTop: 10 }}
                >
                  <TextInput
                    style={InputContainerStyle.body}
                    placeholder="Otro"
                    onChangeText={(text) =>{
                      saveMaterial(key, "Otro", text);
                    }}
                    defaultValue={CaracteristicasState.culturalMaterial[key].OtroValue}
                    placeholderTextColor={AppStyles.color.grey}
                    underlineColorAndroid="transparent"
                  />
                </View>
              ) : (
                <></>
              )}
              </View>
              )}
              )}
            <TouchableOpacity
              onPress={() => onClickAdd()}
              style={{
                width: "60%",
                alignItems: "center",
                justifyContent: "center",
                height: 50,
                borderWidth: 2,
                borderRadius: 20,
                borderColor: "#979797",
                alignSelf: "center",
              }}
            >
              <Text>Añadir Material</Text>
            </TouchableOpacity>
            <View style={{ marginTop: 10 }}>
            <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
              <DropDownPicker
                items={dataEstado}
                placeholder="Estado de Conservación"
                containerStyle={{
                  height: 54,
                  width: '100%'
                }}
                defaultValue={CaracteristicasState.stateConservation}
                style={{ backgroundColor: AppStyles.color.white }}
                itemStyle={{ justifyContent: "flex-start" }}
                dropDownStyle={{ backgroundColor: AppStyles.color.white }}
                onChangeItem={(item) => {
                  setCaracteristicasState({
                    ...CaracteristicasState,
                    stateConservation: item.value,
                  });
                }}
              />
            </View>
              <View style={{ marginTop: 10 }}>
              <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
                <DropDownPicker
                  items={dataAgentes}
                  placeholder="Agentes de Afectación"
                  containerStyle={{
                    height: 54,
                    width:'100%',
                  }}
                  defaultValue={CaracteristicasState.agentsAffection}
                  style={{ backgroundColor: AppStyles.color.white }}
                  itemStyle={{ justifyContent: "flex-start" }}
                  dropDownStyle={{ backgroundColor: AppStyles.color.white }}
                  onChangeItem={(item) => {
                    setCaracteristicasState({
                      ...CaracteristicasState,
                      agentsAffection: item.value,
                    });
                  }}
                />
              </View>
                <View style={InputContainerStyle.InputContainer}>
                <View style={{display: 'flex', flexDirection:'row', marginTop:10}}>
                  <TextInput
                    style={InputContainerStyle.body}
                    placeholder="Agentes o Factores de Afectacion"
                    onChangeText={(value) => {
                      setCaracteristicasState({
                        ...CaracteristicasState,
                        affectingFactors: value,
                      });
                    }}
                    value={CaracteristicasState.affectingFactors}
                    placeholderTextColor={AppStyles.color.grey}
                    underlineColorAndroid="transparent"
                  />
                </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={InputContainerStyle.InputContainer}>
        <View style={{display: 'flex', flexDirection:'row'}}>
          <TextInput
            style={InputContainerStyle.body}
            placeholder="Observaciones"
            onChangeText={(text) =>
              setCaracteristicasState({
                ...CaracteristicasState,
                observations: text,
              })
            }
            value={CaracteristicasState.observations}
            placeholderTextColor={AppStyles.color.grey}
            underlineColorAndroid="transparent"
          />
        </View>
        </View>
        <View style={{ marginBottom: 140 }}>
        <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
          <DropDownPicker
            items={dataTipoRegistros}
            placeholder="Tipo de Registro"
            containerStyle={{
              height: 54,
              width:"100%"
            }}
            defaultValue={CaracteristicasState.TypeRegister}
            style={{ backgroundColor: AppStyles.color.white }}
            itemStyle={{ justifyContent: "flex-start" }}
            dropDownStyle={{ backgroundColor: AppStyles.color.white }}
            onChangeItem={(item) => {
              setCaracteristicasState({
                ...CaracteristicasState,
                TypeRegister: item.value,
              });
            }}
          />
        </View>
        </View>
      </View>
    );
  };
  const mainView = () => {
    return (
      <>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
        <TouchableOpacity
          style={AppIcon.imgContainer}
          onPress={() => navigation.navigate("UploadSite")}
        >
          <Image source={header.logo} />
        </TouchableOpacity>
        <View style={styles.mainContainer}>
          {Platform.OS === "android" ? (
            <ScrollView 
              contentContainerStyle={{width:"100%"}} 
              style={{width:"100%"}}>
                {mainBody()}
            </ScrollView>
          ) : (
            <KeyboardAwareScrollView>{mainBody()}</KeyboardAwareScrollView>
          )}
        </View>
      </>
    );
  };
  return (
    <ImageBackground source={background.image} style={AppStyles.image}>
      <View style={styles.formContainer}>{mainView()}</View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  mainContainer: {
    maxHeight: "85%",
    width:"100%",
    backgroundColor: "#f8f8f8",

    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  mainContainerTitle: {
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  dropdownWrap: {
    display: "flex", flexDirection: "row"
  }
});

export default Caracteristicas;
