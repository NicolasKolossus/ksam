import React, { useEffect, useState } from "react";
import { View, StyleSheet, Image, ImageBackground, Text } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import { vw } from "react-native-css-vh-vw";
import Button from "react-native-button";

import { AppStyles, appVersion } from "../../../AppStyles";
import { background } from '../../../assets';


const WelcomeScreen = ({ navigation }) => {
  const [spinner, setState] = useState(true);

  const tryToLoginFirst = async () => {
    const id = await AsyncStorage.getItem("@loggedInUserID:id");
    const email = await AsyncStorage.getItem("@loggedInUserID:email");
    const password = await AsyncStorage.getItem("@loggedInUserID:password");
    setState(false);
    if (
      id != null &&
      id.length > 0 &&
      password != null &&
      password.length > 0 &&
      email != null &&
      email.length > 0
    ) {
      var dict = {
        id: id,
        email: email,
        profileURL: password,
      };
      navigation.dispatch({
        type: "Login",
        user: dict,
      });
    }

    return;
  };

  useEffect(() => {
    tryToLoginFirst();
  }, []);

  return (
    <ImageBackground
      source={background.image}
      style={styles.image}
    >
      <View style={styles.container}>
        <Spinner visible={spinner} textStyle={{ color: "#FFF" }} />
        <Image
          style={styles.logo}
          source={background.logo}
        />
        <View style={styles.buttonGroup}>
          <Button
            containerStyle={styles.signupContainer}
            style={styles.signupText}
            onPress={() => navigation.navigate("Signup")}
          >
            Registrar
          </Button>
          <Button
            containerStyle={styles.loginContainer}
            style={styles.loginText}
            onPress={() => navigation.navigate("Signin")}
          >
            Iniciar sesión
          </Button>
          <Text>Versión : {appVersion}</Text>
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  buttonGroup: {
    alignItems: "center",
    width: vw(100),
    position: "absolute",
    bottom: 20,
  },
  loginContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.white,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 20,
  },
  loginText: {
    fontSize: 16,
    color: AppStyles.color.grey,
  },
  signupContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.white,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
  },
  signupText: {
    fontSize: 16,
    color: AppStyles.color.grey,
  },
  spinner: {
    marginTop: 200,
  },
});

export default WelcomeScreen;
