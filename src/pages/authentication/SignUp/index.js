import React, { useEffect, useState } from "react";
import Button from "react-native-button";
import Spinner from "react-native-loading-spinner-overlay";
import { vw } from "react-native-css-vh-vw";
import Icon from "react-native-vector-icons/Ionicons";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  ImageBackground,
  View,
  TouchableOpacity
} from "react-native";

import { AppStyles, AppIcon, borderInputContainer } from "../../../AppStyles";
import { realDb, auth } from "../../../api";
import { background } from '../../../assets';

const SignUpScreen = ({ navigation }) => {
  const [state, setState] = useState({
    loading: false,
    fullname: "",
    phone: "",
    email: "",
    password: "",
    spinner: false,
  });

  const onRegister = () => {
    const { email, password } = state;
    setState({ ...setState, spinner: true });
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((response) => {
        const { fullname, phone, email } = state;
        const user_uid = response.user.uid;
        realDb.ref("users").child(user_uid).push({
          email: email,
          fullname: fullname,
          phone: phone,
          appIdentifier: "rn-android-universal-listings",
        });
        realDb
          .ref("users")
          .child(user_uid)
          .on("value", (data) => {
            Object.keys(data.val()).map((item, index) => {
              setState({ ...setState, spinner: false });
              navigation.navigate("Welcome");
            });
          });
      })
      .catch((error) => {
        const { message } = error;
        setState({ ...setState, spinner: false });
        alert(message);
      });
  };

  const mainView = () => {
    return (
      <View style={styles.containerForm}>
        <TouchableOpacity
        style={AppIcon.iconContainer}
        onPress={() => navigation.navigate("Welcome")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
          <Spinner visible={state.spinner} textStyle={{ color: "#FFF" }} />
          <View style={styles.inputGroup}>
            <Text style={styles.inputGroupTitle}>Registro</Text>
            <View style={borderInputContainer.InputContainer}>
              <TextInput
                  style={borderInputContainer.body}
                  placeholder="Usuario"
                  onChangeText={(text) => setState({ ...state, fullname: text })}
                  value={state.fullname}
                  placeholderTextColor={AppStyles.color.grey}
                  underlineColorAndroid="transparent"
              />
            </View>
            <View style={borderInputContainer.InputContainer}>
              <TextInput
                style={borderInputContainer.body}
                placeholder="Número Celular"
                onChangeText={(text) => setState({ ...state, phone: text })}
                value={state.phone}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
            <View style={borderInputContainer.InputContainer}>
              <TextInput
                style={borderInputContainer.body}
                placeholder="E-mail Address"
                onChangeText={(text) => setState({ ...state, email: text })}
                value={state.email}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
            <View style={borderInputContainer.InputContainer}>
              <TextInput
                style={borderInputContainer.body}
                placeholder="Password"
                secureTextEntry={true}
                onChangeText={(text) => setState({ ...state, password: text })}
                value={state.password}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
            <Button
              containerStyle={styles.buttonContainer}
              style={styles.buttonText}
              onPress={() => onRegister()}
            >
              Registro
            </Button>
        </View>
      </View>
    )
  }

  return (
    <ImageBackground
      source={background.image}
      style={AppStyles.image}
    >
      <KeyboardAwareScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: "space-between",
          justifyContent: "flex-end",
        }}
      >
        {mainView()}
      </KeyboardAwareScrollView>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  containerForm: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  container: {
    backgroundColor: "#f8f8f8",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  inputGroup: {
    alignItems: "center",
    width: vw(100),
    position: "absolute",
    paddingBottom: 20,
    paddingTop: 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    bottom: 0,
    backgroundColor: AppStyles.color.white,
  },
  inputGroupTitle: {
    fontSize: 20,
    color: "#5C5C5C",
    fontWeight: "bold",
    marginBottom: 10,
  },
  buttonContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: "#E5E5E5",
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30,
  },
  buttonText: {
    color: AppStyles.color.grey,
  }
});

export default SignUpScreen;
