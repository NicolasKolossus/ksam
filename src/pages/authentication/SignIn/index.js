import React, { useEffect, useState } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import Icon from "react-native-vector-icons/Ionicons";
import { vw } from "react-native-css-vh-vw";
import Button from "react-native-button";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { AppStyles, AppIcon, borderInputContainer } from "../../../AppStyles";
import { realDb, auth } from "../../../api";
import { background } from '../../../assets';

import {
  StyleSheet,
  Image,
  Text,
  TextInput,
  ImageBackground,
  View,
  TouchableOpacity
} from "react-native";

const SignInScreen = ({ navigation }) => {
  const [state, setState] = useState({
    spinner: false,
    email: "",
    password: "",
  });

  const onPressLogin = () => {
    const { email, password } = state;
    if (email.length <= 0 || password.length <= 0) {
      alert("Please fill out the required fields.");
      return;
    }
    setState({ ...setState, spinner: true });
    auth
      .signInWithEmailAndPassword(email, password)
      .then((response) => {
        const user_uid = response.user.uid;
        realDb
          .ref("users")
          .child(user_uid)
          .on("value", (data) => {
            if (data) {
              Object.keys(data.val()).map((item, index) => {
                const user = data.val()[item];
                AsyncStorage.setItem("@loggedInUserID:id", user_uid);
                AsyncStorage.setItem("@loggedInUserID:email", email);
                AsyncStorage.setItem("@loggedInUserID:password", password);
                AsyncStorage.setItem("@loggedInUserID:username", user.fullname);
                setState({ ...setState, spinner: false });
                navigation.dispatch({ type: "Login", user: data.val()[item] });
              });
            }
          });
      })
      .catch((error) => {
        const { message } = error;
        setState({ ...setState, spinner: false });
        alert(message);
      });
  };

  return (
    <ImageBackground
      source={background.image}
      style={AppStyles.image}
    >
      <KeyboardAwareScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: "space-between",
          justifyContent: "flex-end",
        }}
      >
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.iconContainer}
            onPress={() => navigation.navigate("Welcome")}
          >
            <Icon name="chevron-back-sharp" size={30} color="#D68303" />
          </TouchableOpacity>
          <Spinner visible={state.spinner} textStyle={{ color: "#FFF" }} />
          <Image source={background.logo} />
          <View style={styles.inputGroup}>
            <Text style={styles.inputGroupTitle}>Bienvenido</Text>
            <View style={borderInputContainer.InputContainer}>
              <TextInput
                style={borderInputContainer.body}
                placeholder="Usuario"
                onChangeText={(text) => setState({ ...state, email: text })}
                value={state.email}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
            <View style={borderInputContainer.InputContainer}>
              <TextInput
                style={borderInputContainer.body}
                secureTextEntry={true}
                placeholder="Contraseña"
                onChangeText={(text) => setState({ ...state, password: text })}
                value={state.password}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
            <Button
              containerStyle={styles.buttonContainer}
              style={styles.buttonText}
              onPress={() => onPressLogin()}
            >
              Iniciar sesión
            </Button>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  inputGroup: {
    alignItems: "center",
    width: vw(100),
    position: "absolute",
    paddingBottom: 20,
    paddingTop: 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    bottom: 0,
    backgroundColor: AppStyles.color.white,
  },
  inputGroupTitle: {
    fontSize: 20,
    color: "#5C5C5C",
    fontWeight: "bold",
    marginBottom: 10,
  },
  buttonContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: "#E5E5E5",
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30,
  },
  buttonText: {
    color: AppStyles.color.grey,
  },
  iconContainer: {
    position: "absolute",
    top: Platform.OS === 'android'? 30 : 50,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white,
  },
});

export default SignInScreen;
