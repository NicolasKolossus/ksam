import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import Icon from "react-native-vector-icons/Ionicons";
import FontistoIcon from "react-native-vector-icons/Fontisto";
import { vw } from "react-native-css-vh-vw";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AntIcon from "react-native-vector-icons/AntDesign";
import { dataRegion, dataComunas, dataEstado } from "../../../service/util";
import { AppStyles, InputContainerStyle, AppIcon } from "../../../AppStyles";
import DropDownPicker from "../../../components/react-native-dropdown-picker";
import { background, header } from "../../../assets";
import DateTimePickerModal from "react-native-modal-datetime-picker";


import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";

const CondicionesScreen = ({ navigation }) => {
  const [monitorCondicionesData, setmonitorCondicionesDataState] = useState({
    visibilidad:"Buena",
    visibilidadObs : "",
    obstrusividad : "Alta",
    obstrusividadObs:"",
    accesibilidad:"Buena",
    accesibilidadObs:""
  });

  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@condiciones:state");
      const parsingTemp = JSON.parse(temp);
      if (temp) {
        setmonitorCondicionesDataState({
          ...monitorCondicionesData,
          visibilidad: parsingTemp.visibilidad,
          visibilidadObs: parsingTemp.visibilidadObs,
          obstrusividad: parsingTemp.obstrusividad,
          obstrusividadObs: parsingTemp.obstrusividadObs,
          accesibilidad: parsingTemp.accesibilidad,
          accesibilidadObs: parsingTemp.accesibilidadObs
        });
      }
    })();
  }, []);

  useEffect(() => {
    const temp = JSON.stringify(monitorCondicionesData);
    AsyncStorage.setItem("@condiciones:state", temp);
  }, [monitorCondicionesData]);

  const mainView = () => {
    return (
      <View style={{ paddingLeft: 30, paddingRight: 30 }}>
        <View>
          <Text style={styles.mainContainerTitle}>
            {`Condiciones`}
          </Text>
            <Text style={{textAlign: 'center', fontWeight: "bold", fontSize: 20, marginBottom:10}}>Visibilidad</Text>
            <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
              <DropDownPicker
                items={[
                  { label: "Buena", value: "Buena" },
                  { label: "Regular", value: "Regular" },
                  { label: "Baja", value: "Baja" },
                ]}
                placeholder="Visibilidad"
                containerStyle={{
                  width: "100%",
                  height: 54,
                }}
                defaultValue={monitorCondicionesData.visibilidad}
                style={{ backgroundColor: "#ffffff" }}
                itemStyle={{ justifyContent: "flex-start" }}
                dropDownStyle={{ backgroundColor: "#ffffff" }}
                onChangeItem={(item) => {
                  setmonitorCondicionesDataState({
                      ...monitorCondicionesData,
                      visibilidad: item.value,
                  });
                }}
              />
              {(monitorCondicionesData.visibilidad == "") ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
            <TextInput
              style={InputContainerStyle.textInputContainerBG}
              placeholder="Observaciones"
              onChangeText={(text) =>
                setmonitorCondicionesDataState({
                  ...monitorCondicionesData,
                  visibilidadObs: text,
                })
              }
              multiline
              numberOfLines={3}
              value={monitorCondicionesData.visibilidadObs}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />

            <Text style={{textAlign: 'center', fontWeight: "bold", fontSize: 20, marginBottom:10}}>Obstrusividad</Text>
            <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
              <DropDownPicker
                items={[
                  { label: "Alta", value: "Alta" },
                  { label: "Media", value: "Media" },
                  { label: "Baja", value: "Baja" },
                ]}
                placeholder="Obstrusividad"
                containerStyle={{
                  width: "100%",
                  height: 54,
                }}
                defaultValue={monitorCondicionesData.obstrusividad}
                style={{ backgroundColor: "#ffffff" }}
                itemStyle={{ justifyContent: "flex-start" }}
                dropDownStyle={{ backgroundColor: "#ffffff" }}
                onChangeItem={(item) => {
                  setmonitorCondicionesDataState({
                      ...monitorCondicionesData,
                      obstrusividad: item.value,
                  });
                }}
              />
              {(monitorCondicionesData.obstrusividad == "") ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
            <TextInput
              style={InputContainerStyle.textInputContainerBG}
              placeholder="Observaciones"
              onChangeText={(text) =>
                setmonitorCondicionesDataState({
                  ...monitorCondicionesData,
                  obstrusividadObs: text,
                })
              }
              multiline
              numberOfLines={3}
              value={monitorCondicionesData.obstrusividadObs}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />

            <Text style={{textAlign: 'center', fontWeight: "bold", fontSize: 20, marginBottom:10}}>Accesibilidad</Text>
            <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
              <DropDownPicker
                items={[
                  { label: "Buena", value: "Buena" },
                  { label: "Regular", value: "Regular" },
                  { label: "Baja", value: "Baja" },
                ]}
                placeholder="Accesibilidad"
                containerStyle={{
                  width: "100%",
                  height: 54,
                }}
                defaultValue={monitorCondicionesData.accesibilidad}
                style={{ backgroundColor: "#ffffff" }}
                itemStyle={{ justifyContent: "flex-start" }}
                dropDownStyle={{ backgroundColor: "#ffffff" }}
                onChangeItem={(item) => {
                  setmonitorCondicionesDataState({
                      ...monitorCondicionesData,
                      accesibilidad: item.value,
                  });
                }}
              />
              {(monitorCondicionesData.accesibilidad == "") ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
            <TextInput
              style={InputContainerStyle.textInputContainerBG}
              placeholder="Observaciones"
              onChangeText={(text) =>
                setmonitorCondicionesDataState({
                  ...monitorCondicionesData,
                  accesibilidadObs: text,
                })
              }
              multiline
              numberOfLines={3}
              value={monitorCondicionesData.accesibilidadObs}
              placeholderTextColor={AppStyles.color.grey}
              underlineColorAndroid="transparent"
            />
        </View>
      </View>
    );
  };

  return (
    <ImageBackground source={background.image} style={AppStyles.image}>
      <Spinner
        visible={monitorCondicionesData.spinner}
        textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.formContainer}>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
        <TouchableOpacity
          style={AppIcon.imgContainer}
          onPress={() => navigation.navigate("UploadMonitor")}
        >
          <Image source={header.logo} />
        </TouchableOpacity>
        <View style={styles.mainView}>
          {Platform.OS === "android" ? (
            <ScrollView>{mainView()}</ScrollView>
          ) : (
            <KeyboardAwareScrollView>{mainView()}</KeyboardAwareScrollView>
          )}
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  mainView: {
    maxHeight: "85%",
    backgroundColor: "#f8f8f8",

    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  mainContainerTitle: {
    paddingTop: 30,
    paddingBottom: 30,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  mainFormGroupTitle: {
    paddingTop: 20,
    paddingBottom: 10,
    paddingLeft: 15,
    textAlign: "left",
    color: "#5C5C5C",
    fontSize: 16,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: AppStyles.color.blue,
    marginTop: 10,
    textAlign: "left",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  smInput: {
    paddingLeft: 15,
    height: 50,
    borderColor: "#5C5C5C",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
    width: vw(40),
  },
  dropdownWrap: {
    display: "flex", flexDirection: "row"
  }
});

export default CondicionesScreen;
