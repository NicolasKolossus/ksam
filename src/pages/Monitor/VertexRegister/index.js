import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import Icon from "react-native-vector-icons/Ionicons";
import FontistoIcon from "react-native-vector-icons/Fontisto";
import { vw } from "react-native-css-vh-vw";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AntIcon from "react-native-vector-icons/AntDesign";

import { AppStyles, InputContainerStyle, AppIcon } from "../../../AppStyles";
import DropDownPicker from "../../../components/react-native-dropdown-picker";
import { background, header } from "../../../assets";
import AntDesignIcon from "react-native-vector-icons/AntDesign";

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";

const VertexRegisterScreen = ({ navigation }) => {
  const [vertextData, setVertextDataState] = useState([]);

  const [updateFlag, setFlag] = useState(false);

  const onClickAdd = () => {
    const temp = vertextData;
    temp.push({
      pointName: "",
      pointEast: "",
      pointNorth: "",
    });
    setVertextDataState(temp);
    setFlag(!updateFlag);
  };

  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@vertex:state");
      const parsingTemp = JSON.parse(temp);
      setVertextDataState(parsingTemp.points);
    })();
  }, []);

  const deleteElements = () => {
    setVertextDataState([])
  };

  useEffect(() => {
    let temp = {
      points: vertextData
    }
    temp = JSON.stringify(temp);
    AsyncStorage.setItem("@vertex:state", temp);
  }, [vertextData])

  const saveToAsyncStorage = () => {
    let temp = {
      points: vertextData
    }
    temp = JSON.stringify(temp);
    AsyncStorage.setItem("@vertex:state", temp);
  }

  const vertextDataSave = (text, index, key) => {
    let temp = vertextData;
    temp[index][key] = text;
    setVertextDataState(temp);
    setFlag(!updateFlag);
    saveToAsyncStorage();
  };

  const mainView = () => {
    return (
      <View style={{ paddingLeft: 30, paddingRight: 30 }}>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.mainContainerTitle}>Vértice de inspección visual </Text>
          <TouchableOpacity onPress={() => deleteElements()}>
            <AntDesignIcon name="delete" size={30} color="#5C5C5C" />
          </TouchableOpacity>
        </View>
        <View style={{ display: "flex", flexDirection: "row", width: "100%" }}>
          <Text
            style={{ width: "40%", textAlign: "center", padding: 3 }}
          >{`Nombre del punto`}</Text>
          <Text
            style={{ width: "30%", textAlign: "center", padding: 3 }}
          >{`Punto Este`}</Text>
          <Text
            style={{ width: "30%", textAlign: "center", padding: 3 }}
          >{`Punto Norte`}</Text>
        </View>
        {vertextData.map((ele, index) => (
          <View
            key={index}
            style={{ display: "flex", flexDirection: "row", width: "100%" }}
          >
            <View
              style={[
                InputContainerStyle.InputContainer,
                { width: "40%", padding: 3 },
              ]}
            >
              <TextInput
                style={[
                  InputContainerStyle.body,
                  { borderColor: "#CCCCCC", borderWidth: 1 },
                ]}
                onChangeText={(text) =>
                  vertextDataSave(text, index, "pointName")
                }
                value={ele.pointName}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
            <View
              style={[
                InputContainerStyle.InputContainer,
                { width: "30%", padding: 3 },
              ]}
            >
              <TextInput
                style={[
                  InputContainerStyle.body,
                  { borderColor: "#CCCCCC", borderWidth: 1 },
                ]}
                onChangeText={(text) =>
                  vertextDataSave(text, index, "pointEast")
                }
                value={ele.pointEast}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
            <View
              style={[
                InputContainerStyle.InputContainer,
                { width: "30%", padding: 3 },
              ]}
            >
              <TextInput
                style={[
                  InputContainerStyle.body,
                  { borderColor: "#CCCCCC", borderWidth: 1 },
                ]}
                onChangeText={(text) =>
                  vertextDataSave(text, index, "pointNorth")
                }
                value={ele.pointNorth}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
          </View>
        ))}
        <TouchableOpacity
          onPress={() => onClickAdd()}
          style={{
            width: "60%",
            alignItems: "center",
            justifyContent: "center",
            height: 50,
            borderWidth: 2,
            borderRadius: 20,
            borderColor: "#979797",
            alignSelf: "center",
          }}
        >
          <Text>Añadir punto</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <ImageBackground source={background.image} style={AppStyles.image}>
      <Spinner
        visible={vertextData.spinner}
        textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.formContainer}>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
        <TouchableOpacity
          style={AppIcon.imgContainer}
          onPress={() => navigation.navigate("UploadMonitor")}
        >
          <Image source={header.logo} />
        </TouchableOpacity>
        <View style={styles.mainView}>
          {Platform.OS === "android" ? (
            <ScrollView style={{ height: "100%" }}>{mainView()}</ScrollView>
          ) : (
            <KeyboardAwareScrollView style={{ height: "100%" }}>
              {mainView()}
            </KeyboardAwareScrollView>
          )}
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  mainView: {
    maxHeight: "85%",
    backgroundColor: "#f8f8f8",

    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  mainContainerTitle: {
    paddingTop: 30,
    paddingBottom: 30,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  mainFormGroupTitle: {
    paddingTop: 20,
    paddingBottom: 10,
    paddingLeft: 15,
    textAlign: "left",
    color: "#5C5C5C",
    fontSize: 16,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: AppStyles.color.blue,
    marginTop: 10,
    textAlign: "left",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  smInput: {
    paddingLeft: 15,
    height: 50,
    borderColor: "#5C5C5C",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
    width: vw(22),
  },
});

export default VertexRegisterScreen;
