import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import Icon from "react-native-vector-icons/Ionicons";
import FontistoIcon from "react-native-vector-icons/Fontisto";
import { vw } from "react-native-css-vh-vw";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AntIcon from "react-native-vector-icons/AntDesign";
import { dataRegion, dataComunas } from "../../../service/util";
import { AppStyles, InputContainerStyle, AppIcon } from "../../../AppStyles";
import DropDownPicker from "../../../components/react-native-dropdown-picker";
import { background, header } from "../../../assets";
import DateTimePickerModal from "react-native-modal-datetime-picker";


import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";

const MonitorRegisterScreen = ({ navigation }) => {
  const [monitorMainData, setmonitorMainDataState] = useState({
    proyecto:"",
    hInicio : "Sin seleccionar",
    hFin : "Sin seleccionar",
    sector:"",
    region:"",
    comuna:"",
    fecha: "Sin seleccionar fecha",
    observaciones: ""
  });

  const [dateModal, setDateModalState] = useState(false);
  const [dateTime1, setDateTime1State] = useState(false);
  const [dateTime2, setDateTime2State] = useState(false);

  const [comunasr, setComunasbyRegion] = useState(dataComunas);

  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@monitor:state");
      const parsingTemp = JSON.parse(temp);
      if (temp) {
        setmonitorMainDataState({
          ...monitorMainData,
          proyecto: parsingTemp.proyecto,
          hInicio: parsingTemp.hInicio,
          hFin: parsingTemp.hFin,
          sector: parsingTemp.sector,
          region: parsingTemp.region,
          comuna: parsingTemp.comuna,
          fecha: parsingTemp.fecha,
          observaciones: parsingTemp.observaciones
        });
      }
    })();
  }, []);

  const setRegion = (itemreg) => {
    setComunasbyRegion({});

    console.log(itemreg);
    setmonitorMainDataState({
      ...monitorMainData,
      region: itemreg.value,
    });

    var toSearch = itemreg.regionId;

    let temp1 = {};
    temp1 = dataComunas.filter(obj => {
      return obj.comunaRegionId === toSearch
    });

    setComunasbyRegion(temp1);

  };

  const hideDatePicker = () => {
    setDateModalState(false);
  };

  const hideTime1Picker = () => {
    setDateTime1State(false);
  };

  const hideTime2Picker = () => {
    setDateTime2State(false);
  };

  const handleConfirm = (date) => {
    let current = getDateFromDateObject(date);

    setmonitorMainDataState({ ...monitorMainData, fecha: current });
    hideDatePicker();
  };

  const handleConfirmTime1 = (date) => {
    let current = getTimeFromDateObject(date);

    setmonitorMainDataState({ ...monitorMainData, hInicio: current });
    hideTime1Picker();
  };

  const handleConfirmTime2 = (date) => {
    let current = getTimeFromDateObject(date);

    setmonitorMainDataState({ ...monitorMainData, hFin: current });
    hideTime2Picker();
  };

  const getDateFromDateObject = (date) => {
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let year = date.getYear() + 1900;
    month = month < 10 ? "0" + month : month;
    day = day < 10 ? "0" + day : day;
    return month + "/" + day + "/" + year;
  };

  const getTimeFromDateObject = (date) => {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    return hours + ":" + minutes;
  };

  const viewDatePicker = () => {
    setDateModalState(true);
  };

  const viewTime1Picker = () => {
    setDateTime1State(true);
  };

  const viewTime2Picker = () => {
    setDateTime2State(true);
  };

  useEffect(() => {
    const temp = JSON.stringify(monitorMainData);
    AsyncStorage.setItem("@monitor:state", temp);
  }, [monitorMainData]);

  const mainView = () => {
    return (
      <View style={{ paddingLeft: 30, paddingRight: 30 }}>
        <View>
          <Text style={styles.mainContainerTitle}>
            {`Registro de Monitoreo`}
          </Text>
          <View style={InputContainerStyle.InputContainer}>
            <View style={{ display: "flex", flexDirection: "row" }}>
              <TextInput
                style={InputContainerStyle.body}
                placeholder="Proyecto"
                onChangeText={(text) =>
                  setmonitorMainDataState({ ...monitorMainData, proyecto: text })
                }
                value={monitorMainData.proyecto}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
              {monitorMainData.proyecto == "" ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 3, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
          </View>
          <View style={{display: 'flex', flexDirection: 'row'}}>
            <Text style={{textAlign: 'left', marginTop:5, marginBottom:5, marginLeft: 50}}>Hora inicio</Text>
            <Text style={{textAlign: 'right', marginTop:5, marginBottom:5, marginLeft: 120}}>Hora Fin</Text>
          </View>
          <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', marginBottom:10}}>
            <TouchableOpacity
                style={[
                  styles.smInput,
                  {
                    borderWidth: 0.7,
                    borderColor: "#CECECE",
                    justifyContent: "center",
                  },
                ]}
                onPress={viewTime1Picker}
              >
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    paddingRight: 20,
                  }}
                >
                  <Text style={{ textAlign: "left" }}>
                    {monitorMainData.hInicio}
                  </Text>
                  <FontistoIcon name="clock" size={20} color="#000000" />
                </View>
                <DateTimePickerModal
                  isVisible={dateTime1}
                  mode="time"
                  onConfirm={handleConfirmTime1}
                  onCancel={hideTime1Picker}
                />
              </TouchableOpacity>
              <View style={{display: 'flex', flexDirection: 'row'}}>
                <TouchableOpacity
                  style={[
                    styles.smInput,
                    {
                      borderWidth: 0.7,
                      borderColor: "#CECECE",
                      justifyContent: "center",
                    },
                  ]}
                  onPress={viewTime2Picker}
                >
                  <View
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                      paddingRight: 20,
                    }}
                  >
                    <Text style={{ textAlign: "left" }}>
                      {monitorMainData.hFin}
                    </Text>
                    <FontistoIcon name="clock" size={20} color="#000000" />
                  </View>
                  <DateTimePickerModal
                    isVisible={dateTime2}
                    mode="time"
                    onConfirm={handleConfirmTime2}
                    onCancel={hideTime2Picker}
                  />
                </TouchableOpacity>
              </View>
          </View>
          <View style={InputContainerStyle.InputContainer}>
            <View style={{ display: "flex", flexDirection: "row" }}>
              <TextInput
                style={InputContainerStyle.body}
                placeholder="Sector proyecto / Obra monitoreada"
                onChangeText={(text) =>
                  setmonitorMainDataState({
                    ...monitorMainData,
                    sector: text,
                  })
                }
                value={monitorMainData.sector}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
              {monitorMainData.sector == "" ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 3, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
          </View>
          <View>
            <View style={{ marginBottom: 10 }}>
              <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
                <DropDownPicker
                  items={dataRegion}
                  placeholder="Región"
                  containerStyle={{
                    width: "100%",
                    height: 54,
                  }}
                  defaultValue={monitorMainData.region}
                  style={{ backgroundColor: "#ffffff" }}
                  itemStyle={{ justifyContent: "flex-start" }}
                  dropDownStyle={{ backgroundColor: "#ffffff" }}
                  onChangeItem={(item) => {
                    setRegion(item);
                  }}
                />
              </View>
              <View style={{ marginTop: 10 }}>
                <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
                  <DropDownPicker
                    items={comunasr}
                    placeholder="Comuna"
                    containerStyle={{
                      height: 54,
                      width: "100%",
                    }}
                    defaultValue={monitorMainData.comuna}
                    style={{ backgroundColor: "#ffffff" }}
                    itemStyle={{ justifyContent: "flex-start" }}
                    dropDownStyle={{ backgroundColor: "#ffffff" }}
                    onChangeItem={(item) => {
                      setmonitorMainDataState({
                        ...monitorMainData,
                        comuna: item.value,
                      });
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={[InputContainerStyle.InputContainer, Platform.OS !== "android" ? { zIndex:-5} : null]}>
            <View style={{ display: "flex", flexDirection: "row" }}>
              <TouchableOpacity
                style={[
                  InputContainerStyle.body,
                  {
                    borderWidth: 0.7,
                    borderColor: "#CECECE",
                    justifyContent: "center",
                  },
                ]}
                onPress={viewDatePicker}
              >
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    paddingRight: 20,
                  }}
                >
                  <Text style={{ textAlign: "left" }}>
                    {monitorMainData.fecha}
                  </Text>
                  <FontistoIcon name="date" size={20} color="#000000" />
                </View>
                <DateTimePickerModal
                  isVisible={dateModal}
                  mode="date"
                  onConfirm={handleConfirm}
                  onCancel={hideDatePicker}
                />
              </TouchableOpacity>
              {monitorMainData.date == "Sin seleccionar fecha" ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 3, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
          </View>
          <View style={[InputContainerStyle.InputContainer, Platform.OS !== "android" ? { zIndex:-5} : null]}>
            <View style={{ display: "flex", flexDirection: "row"}}>
              <TextInput
                style={InputContainerStyle.textInputContainerBG}
                placeholder="Observaciones"
                onChangeText={(text) =>
                  setmonitorMainDataState({
                    ...monitorMainData,
                    observaciones: text,
                  })
                }
                multiline
                numberOfLines={3}
                value={monitorMainData.observaciones}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
          </View>
        </View>
      </View>
    );
  };

  return (
    <ImageBackground source={background.image} style={AppStyles.image}>
      <Spinner
        visible={monitorMainData.spinner}
        textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.formContainer}>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
        <TouchableOpacity
          style={AppIcon.imgContainer}
          onPress={() => navigation.navigate("UploadMonitor")}
        >
          <Image source={header.logo} />
        </TouchableOpacity>
        <View style={styles.mainView}>
          {Platform.OS === "android" ? (
            <ScrollView>{mainView()}</ScrollView>
          ) : (
            <KeyboardAwareScrollView>{mainView()}</KeyboardAwareScrollView>
          )}
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  mainView: {
    maxHeight: "85%",
    backgroundColor: "#f8f8f8",

    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  mainContainerTitle: {
    paddingTop: 30,
    paddingBottom: 30,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  mainFormGroupTitle: {
    paddingTop: 20,
    paddingBottom: 10,
    paddingLeft: 15,
    textAlign: "left",
    color: "#5C5C5C",
    fontSize: 16,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: AppStyles.color.blue,
    marginTop: 10,
    textAlign: "left",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  smInput: {
    paddingLeft: 15,
    height: 50,
    borderColor: "#5C5C5C",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
    width: vw(40),
  },
  dropdownWrap: {
    display: "flex", flexDirection: "row"
  }
});

export default MonitorRegisterScreen;
