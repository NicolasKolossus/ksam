import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/Ionicons";
import AntDesignIcon from "react-native-vector-icons/AntDesign";
import { vw } from "react-native-css-vh-vw";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AntIcon from "react-native-vector-icons/AntDesign";
import DropDownPicker from "../../../components/react-native-dropdown-picker";

import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";

import { AppStyles, InputContainerStyle, AppIcon } from "../../../AppStyles";
import { background, header } from "../../../assets";
import { siteType } from "../../../service/util";

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { isNull } from "lodash";

const ArchaeologicalFinding = ({ navigation }) => {
  const [state, setState] = useState([]);
  const [displayState, setDisplayState] = useState(false);
  const [ArchaeologicalFindingData, setArchaeologicalFindingState] = useState({
    sigla:"",
    sitioNuevo:"",
    tipoSitio:"",
    cercaniaObra:"",
    otroValue: "",
    fueraPeligro: "",
    estadoSitio: "",
    medidaPrecaucion: "",
  });

  const onFocusFunction = async () => {
    const temp = await AsyncStorage.getItem("@actividad:state");
    const parsingTemp = JSON.parse(temp);
    if (
      isNull(parsingTemp.cercania) ||
      parsingTemp.cercania == "No" ||
      parsingTemp.cercania == ""
    ) {
      setDisplayState(false);
    } else {
      setDisplayState(true);
    }
  };

  useEffect(() => {
    (async () => {
      onFocusFunction();
      navigation.addListener("didFocus", () => {
        onFocusFunction();
      });
      let temp = await AsyncStorage.getItem("@archaeological:state");
      const parsingTemp = JSON.parse(temp);
      if (parsingTemp.otroValue) {
        parsingTemp.siteType = "Otro";
      }
      if (temp) {
        setArchaeologicalFindingState({
          ...ArchaeologicalFindingData,
          sigla: parsingTemp.sigla,
          sitioNuevo: parsingTemp.sitioNuevo,
          tipoSitio: parsingTemp.tipoSitio,
          cercaniaObra: parsingTemp.cercaniaObra,
          fueraPeligro: parsingTemp.fueraPeligro,
          otroValue: parsingTemp.otroValue,
          estadoSitio: parsingTemp.estadoSitio,
          medidaPrecaucion: parsingTemp.medidaPrecaucion,
        });
      }
    })();
  }, []);

  useEffect(() => {
    const temp = JSON.stringify(ArchaeologicalFindingData);
    AsyncStorage.setItem("@archaeological:state", temp);
  }, [ArchaeologicalFindingData]);

  const mainView1 = () => {
    return (
      <>
        {displayState ? (
        <View style={{ paddingLeft: 30, paddingRight: 30 }}>
          <View>
            <Text style={styles.mainContainerTitle}>
              {`Cercanía a sitios`}
            </Text>
            <View style={InputContainerStyle.InputContainer}>
              <View style={{ display: "flex", flexDirection: "row" }}>
                <TextInput
                  style={InputContainerStyle.body}
                  placeholder="Sigla"
                  onChangeText={(text) =>
                    setArchaeologicalFindingState({ ...ArchaeologicalFindingData, sigla: text })
                  }
                  value={ArchaeologicalFindingData.sigla}
                  placeholderTextColor={AppStyles.color.grey}
                  underlineColorAndroid="transparent"
                />
                {ArchaeologicalFindingData.sigla == "" ? (
                  <AntIcon
                    name="warning"
                    style={{ marginLeft: 3, alignSelf: "center" }}
                    size={15}
                    color="red"
                  />
                ) : (
                  <AntIcon
                    name="checkcircleo"
                    style={{ marginLeft: 2, alignSelf: "center" }}
                    size={15}
                    color="green"
                  />
                )}
            </View>
          </View>
          <View>
            <View style={{ marginBottom: 10 }}>
              <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
              <DropDownPicker
                items={[
                  { label: "Si", value: "Si" },
                  { label: "No", value: "No" }
                ]}
                placeholder="Sitio nuevo"
                containerStyle={{
                  width: "100%",
                  height: 54,
                }}
                defaultValue={ArchaeologicalFindingData.sitioNuevo}
                style={{ backgroundColor: "#ffffff" }}
                itemStyle={{ justifyContent: "flex-start" }}
                dropDownStyle={{ backgroundColor: "#ffffff" }}
                onChangeItem={(item) => {
                  setArchaeologicalFindingState({
                      ...ArchaeologicalFindingData,
                      sitioNuevo: item.value,
                  });
                }}
              />
              {ArchaeologicalFindingData.sitioNuevo == "" ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 3, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
              </View>
              <View style={{ marginTop: 10 }}>
                <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
                <DropDownPicker
                  items={siteType}
                  placeholder="Tipo de sitio"
                  containerStyle={{
                    width: "100%",
                    height: 54,
                  }}
                  defaultValue={ArchaeologicalFindingData.tipoSitio}
                  style={{ backgroundColor: "#ffffff" }}
                  itemStyle={{ justifyContent: "flex-start" }}
                  dropDownStyle={{ backgroundColor: "#ffffff" }}
                  onChangeItem={(item) => {
                    if (item.value == "Otro") {
                      setArchaeologicalFindingState({
                        ...ArchaeologicalFindingData,
                        tipoSitio: item.value,
                      });
                    } else {
                      setArchaeologicalFindingState({
                        ...ArchaeologicalFindingData,
                        tipoSitio: item.value,
                        otroValue: "",
                      });
                    }
                  }}
                />
                {(ArchaeologicalFindingData.tipoSitio == "") ? (
                  <AntIcon
                    name="warning"
                    style={{ marginLeft: 2, alignSelf: "center" }}
                    size={15}
                    color="red"
                  />
                ) : (
                  <AntIcon
                    name="checkcircleo"
                    style={{ marginLeft: 2, alignSelf: "center" }}
                    size={15}
                    color="green"
                  />
                )}
                </View>
              </View>
            </View>
          </View>
          {ArchaeologicalFindingData.tipoSitio == "Otro" ? (
            <View
              style={[InputContainerStyle.InputContainer, Platform.OS !== "android" ? { zIndex:-5} : null]}
            >
              <TextInput
                style={InputContainerStyle.body}
                placeholder="Otro"
                onChangeText={(text) =>
                  setArchaeologicalFindingState({ ...ArchaeologicalFindingData, otroValue: text })
                }
                value={ArchaeologicalFindingData.otroValue}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
              {ArchaeologicalFindingData.otroValue == "" ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
          ) : (
            <></>
          )}
          <View style={[InputContainerStyle.InputContainer, Platform.OS !== "android" ? { zIndex:-5} : null]}>
            <View style={{ display: "flex", flexDirection: "row"}}>
              <TextInput
                style={InputContainerStyle.textInputContainerBG}
                placeholder="Cercanía a obra"
                onChangeText={(text) =>
                  setArchaeologicalFindingState({
                    ...ArchaeologicalFindingData,
                    cercaniaObra: text,
                  })
                }
                multiline
                numberOfLines={3}
                value={ArchaeologicalFindingData.cercaniaObra}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
              {(ArchaeologicalFindingData.cercaniaObra == "") ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
          </View>
          <View>
            <View style={{ marginBottom: 10 }}>
              <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
              <DropDownPicker
                items={[
                  { label: "Si", value: "Si" },
                  { label: "No", value: "No" }
                ]}
                placeholder="Sitio fuera de peligro"
                containerStyle={{
                  width: "100%",
                  height: 54,
                }}
                defaultValue={ArchaeologicalFindingData.fueraPeligro}
                style={{ backgroundColor: "#ffffff" }}
                itemStyle={{ justifyContent: "flex-start" }}
                dropDownStyle={{ backgroundColor: "#ffffff" }}
                onChangeItem={(item) => {
                  setArchaeologicalFindingState({
                      ...ArchaeologicalFindingData,
                      fueraPeligro: item.value,
                  });
                }}
              />
              </View>
              <View style={{ marginTop: 10 }}>
                <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
                  <DropDownPicker
                    items={[
                      { label: "Dañado", value: "Dañado" },
                      { label: "Riesgo de daño", value: "Riesgo de daño" }
                    ]}
                    placeholder="Estado del sitio"
                    containerStyle={{
                      width: "100%",
                      height: 54,
                    }}
                    defaultValue={ArchaeologicalFindingData.estadoSitio}
                    style={{ backgroundColor: "#ffffff" }}
                    itemStyle={{ justifyContent: "flex-start" }}
                    dropDownStyle={{ backgroundColor: "#ffffff" }}
                    onChangeItem={(item) => {
                      setArchaeologicalFindingState({
                          ...ArchaeologicalFindingData,
                          estadoSitio: item.value,
                      });
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={[InputContainerStyle.InputContainer, Platform.OS !== "android" ? { zIndex:-5} : null]}>
            <View style={{ display: "flex", flexDirection: "row"}}>
              <TextInput
                style={InputContainerStyle.textInputContainerBG}
                placeholder="Medida de precaución"
                onChangeText={(text) =>
                  setArchaeologicalFindingState({
                    ...ArchaeologicalFindingData,
                    medidaPrecaucion: text,
                  })
                }
                multiline
                numberOfLines={3}
                value={ArchaeologicalFindingData.medidaPrecaucion}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
              {(ArchaeologicalFindingData.medidaPrecaucion == "") ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
            </View>
          </View>
        </View>
        </View>
        ) : (
          <Text style={styles.mainContainerTitle}>
            No se encuentra cercania a sitio arqueológico.
          </Text>
        )}
      </>
    );
  };

  return (
    <ImageBackground source={background.image} style={styles.image}>
      <View style={{ flex: 1 }}>
        <View style={styles.formContainer}>
          <View style={{ flex: 1 }}>
            <KeyboardAwareScrollView
              contentContainerStyle={{ flex: 1, justifyContent: "flex-end" }}
            >
              <TouchableOpacity
                style={AppIcon.iconContainer}
                onPress={() => navigation.navigate("Home")}
              >
                <Icon name="chevron-back-sharp" size={30} color="#D68303" />
              </TouchableOpacity>
              <TouchableOpacity
                style={AppIcon.imgContainer}
                onPress={() => navigation.navigate("UploadMonitor")}
              >
                <Image source={header.logo} />
              </TouchableOpacity>
              <View style={styles.mainContainer}>
                {Platform.OS === "android" ? (
                  <ScrollView>{mainView1()}</ScrollView>
                ) : (
                  <KeyboardAwareScrollView>
                    {mainView1()}
                  </KeyboardAwareScrollView>
                )}
              </View>
            </KeyboardAwareScrollView>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  mainContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "#f8f8f8",
    paddingLeft: 15,
    paddingRight: 15,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingBottom: 30,
  },
  mainContainerTitle: {
    paddingTop: 30,
    paddingBottom: 30,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  iconContainer: {
    position: "absolute",
    top: 10,
    left: 20,
    width: 30,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: AppStyles.color.white,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: AppStyles.color.blue,
    marginTop: 10,
    textAlign: "left",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  InputContainer: {
    marginTop: 10,
    marginBottom: 10,
  },
  body: {
    paddingLeft: 15,
    height: 54,
    borderColor: "#f5f5f5",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
  },
  collapseHeader: {
    display: "flex",
    flexDirection: "row",
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: "space-between",
    alignItems: "center",
    height: 50,
    backgroundColor: "#f0f0f0",
    borderRadius: 7,
  },
  collapseBody: {
    backgroundColor: "#e8e8e8",
    marginTop: 10,
    borderRadius: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  titleLabel: {
    justifyContent: "center",
    marginLeft: 10,
    marginTop: 20,
  },
  textInputContainerBG: {
    padding: 10,
    borderColor: "gray",
    marginTop: 5,
    borderWidth: 1,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: "#ffffff",
    marginBottom: 10,
  },
  textInputContainerSM: {
    height: 50,
    padding: 10,
    borderColor: "gray",
    marginTop: 5,
    borderWidth: 1,
    borderRadius: AppStyles.borderRadius.main,
    backgroundColor: "#ffffff",
  },
  dropdownWrap: {
    display: "flex", flexDirection: "row"
  }
});

export default ArchaeologicalFinding;
