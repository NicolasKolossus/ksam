import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import Icon from "react-native-vector-icons/Ionicons";
import FontistoIcon from "react-native-vector-icons/Fontisto";
import { vw } from "react-native-css-vh-vw";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AntIcon from "react-native-vector-icons/AntDesign";
import { dataRegion, dataComunas } from "../../../service/util";
import { AppStyles, InputContainerStyle, AppIcon } from "../../../AppStyles";
import DropDownPicker from "../../../components/react-native-dropdown-picker";
import { background, header } from "../../../assets";
import DateTimePickerModal from "react-native-modal-datetime-picker";


import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";

const ActividadMonitoreadaScreen = ({ navigation }) => {
  const [actividadMonitoreadaData, setactividadMonitoreadaState] = useState({
    intervencion:"",
    cercania:"",
    tipoIntervencion:"",
    observaciones:"",
    medidas: {
      ancho: '',
      largo: '',
      profundidad: '',
    }
  });

  const [displayState, setDisplayState] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@actividad:state");
      const parsingTemp = JSON.parse(temp);
      if (temp) {
        setactividadMonitoreadaState({
          ...actividadMonitoreadaData,
          intervencion: parsingTemp.intervencion,
          cercania: parsingTemp.cercania,
          tipoIntervencion: parsingTemp.tipoIntervencion,
          observaciones: parsingTemp.observaciones,
          medidas: parsingTemp.medidas
        });
      }
    })();
  }, []);

  useEffect(() => {
    const temp = JSON.stringify(actividadMonitoreadaData);
    AsyncStorage.setItem("@actividad:state", temp);
  }, [actividadMonitoreadaData]);

  const setMeasurementsData = (text, flag) => {
    if(flag=="ancho"){
      const originalData = actividadMonitoreadaData.medidas.largo;
      const originalData2 = actividadMonitoreadaData.medidas.profundidad;
      setactividadMonitoreadaState({ ...actividadMonitoreadaData, medidas: {'ancho':text, 'largo':originalData, 'profundidad':originalData2}});
    }else if(flag=="largo"){
      const originalData = actividadMonitoreadaData.medidas.ancho;
      const originalData2 = actividadMonitoreadaData.medidas.profundidad;
      setactividadMonitoreadaState({ ...actividadMonitoreadaData, medidas: {'ancho':originalData, 'largo':text, 'profundidad':originalData2}});
    }else{
      const originalData = actividadMonitoreadaData.medidas.ancho;
      const originalData2 = actividadMonitoreadaData.medidas.largo;
      setactividadMonitoreadaState({ ...actividadMonitoreadaData, medidas: {'ancho':originalData, 'largo':originalData2, 'profundidad':text}});
    }
  }

  const mainView = () => {
    return (
      <View style={{ paddingLeft: 30, paddingRight: 30 }}>
        <View>
          <Text style={styles.mainContainerTitle}>
            {`Actividad monitoreada`}
          </Text>
          <View>
            <View style={{ marginBottom: 10 }}>
              <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
                <DropDownPicker
                  items={[
                    { label: "Continuación de trabajos", value: "Continuación de trabajos" },
                    { label: "Intervención nueva", value: "Intervención nueva" }
                  ]}
                  placeholder="Intervención"
                  containerStyle={{
                    width: "100%",
                    height: 54,
                  }}
                  defaultValue={actividadMonitoreadaData.intervencion}
                  style={{ backgroundColor: "#ffffff" }}
                  itemStyle={{ justifyContent: "flex-start" }}
                  dropDownStyle={{ backgroundColor: "#ffffff" }}
                  onChangeItem={(item) => {
                    setactividadMonitoreadaState({
                        ...actividadMonitoreadaData,
                        intervencion: item.value,
                    });
                  }}
                />
              </View>
              <View style={{ marginTop: 10, marginBottom: open ? 60 : 0 }}>
                <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:2} : null]}>
              <DropDownPicker
                  items={[
                    { label: "Si", value: "Si" },
                    { label: "No", value: "No" }
                  ]}
                  placeholder="Cercanía a sitios arqueológicos"
                  containerStyle={{
                    width: "100%",
                    height: 54,
                  }}
                  defaultValue={actividadMonitoreadaData.cercania}
                  style={{ backgroundColor: "#ffffff" }}
                  itemStyle={{ justifyContent: "flex-start" }}
                  dropDownStyle={{ backgroundColor: "#ffffff" }}
                  onChangeItem={(item) => {
                    setactividadMonitoreadaState({
                      ...actividadMonitoreadaData,
                      cercania: item.value,
                  });
                  if(item.value == 'Si'){
                    setDisplayState(true);
                    if(Platform.OS !== "android"){
                      setOpen(true);
                    }
                  }else{
                    setDisplayState(false);
                    if(Platform.OS !== "android"){
                      setOpen(false);
                    }
                  }
                  }}
                />
              {actividadMonitoreadaData.intervencion == "" ? (
                <AntIcon
                  name="warning"
                  style={{ marginLeft: 3, alignSelf: "center" }}
                  size={15}
                  color="red"
                />
              ) : (
                <AntIcon
                  name="checkcircleo"
                  style={{ marginLeft: 2, alignSelf: "center" }}
                  size={15}
                  color="green"
                />
              )}
                </View>
              </View>
          { displayState ? (
          <View style={{ marginTop: 10 }}>
            <View style={[styles.dropdownWrap, Platform.OS !== "android" ? { zIndex:1} : null]}>
            <DropDownPicker
                items={[
                  { label: "Área ya intervenida", value: "Área ya intervenida" },
                  { label: "excavación en suelo virgen", value: "excavación en suelo virgen" }
                ]}
                placeholder="Tipo de intervención"
                containerStyle={{
                  width: "100%",
                  height: 54,
                }}
                defaultValue={actividadMonitoreadaData.tipoIntervencion}
                style={{ backgroundColor: "#ffffff" }}
                itemStyle={{ justifyContent: "flex-start" }}
                dropDownStyle={{ backgroundColor: "#ffffff" }}
                onChangeItem={(item) => {
                  setactividadMonitoreadaState({
                      ...actividadMonitoreadaData,
                      tipoIntervencion: item.value,
                  });
                }}
              />
            </View>
          </View>
          ) : (<></>)}
          </View>
        </View>
          <View style={[InputContainerStyle.InputContainer, Platform.OS !== "android" ? { zIndex:-5} : null]}>
            <View style={{ display: "flex", flexDirection: "row" }}>
              <TextInput
                style={InputContainerStyle.textInputContainerBG}
                placeholder="Observaciones"
                onChangeText={(text) =>
                  setactividadMonitoreadaState({
                    ...actividadMonitoreadaData,
                    observaciones: text,
                  })
                }
                multiline
                numberOfLines={3}
                value={actividadMonitoreadaData.observaciones}
                placeholderTextColor={AppStyles.color.grey}
                underlineColorAndroid="transparent"
              />
            </View>
          </View>
          <Text style={{textAlign: 'center', marginTop:5, marginBottom:10, fontSize:20}}>Dimensiones (metros)</Text>
          <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', marginBottom:10}}>
                { 
                  actividadMonitoreadaData.medidas.ancho == ""?
                  <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
                  :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
                }
              <TextInput
                  style={styles.smInput}
                  placeholder="Ancho"
                  onChangeText={(text) =>{
                    let num = text.replace(".", '');
                    if(!isNaN(num)){
                      setMeasurementsData(text, 'ancho')
                    }
                  }
                  }
                  value={actividadMonitoreadaData.medidas.ancho}
                  placeholderTextColor={AppStyles.color.grey}
                  underlineColorAndroid="transparent"
                />
              <View style={{ justifyContent: 'center' }}>
                <FontistoIcon name="close-a" size={20}/>
              </View>
              <View style={{display: 'flex', flexDirection: 'row'}}>
                <TextInput
                  style={styles.smInput}
                  placeholder="Largo"
                  onChangeText={(text) =>{
                    let num = text.replace(".", '');
                    if(!isNaN(num)){
                      setMeasurementsData(text, 'largo')
                    }
                  }
                    
                  }
                  value={actividadMonitoreadaData.medidas.largo}
                  placeholderTextColor={AppStyles.color.grey}
                  underlineColorAndroid="transparent"
                />
                {
                  actividadMonitoreadaData.medidas.largo == ""?
                  <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
                  :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
                }
              </View>
          </View>
          <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', marginBottom:10, marginLeft:-20}}>
                <View style={{display: 'flex', flexDirection:'row'}}>
                  <TextInput
                    style={styles.smInput}
                      placeholder="Profundidad"
                      onChangeText={(text) =>{
                        let num = text.replace(".", '');
                        if(!isNaN(num)){
                          setMeasurementsData(text, 'profundidad')
                        }
                      }
                      }
                      value={actividadMonitoreadaData.medidas.profundidad}
                      placeholderTextColor={AppStyles.color.grey}
                      underlineColorAndroid="transparent"
                  />
                  { 
                  actividadMonitoreadaData.medidas.profundidad == ""?
                  <AntIcon name="warning" style={{marginLeft:3, alignSelf:'center'}} size={15} color="red"/>
                  :<AntIcon name="checkcircleo" style={{marginLeft:2, alignSelf:'center'}} size={15} color="green"/>
                }
                </View>
              </View>
        </View>
      </View>
    );
  };

  return (
    <ImageBackground source={background.image} style={AppStyles.image}>
      <Spinner
        visible={actividadMonitoreadaData.spinner}
        textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.formContainer}>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
        <TouchableOpacity
          style={AppIcon.imgContainer}
          onPress={() => navigation.navigate("UploadMonitor")}
        >
          <Image source={header.logo} />
        </TouchableOpacity>
        <View style={styles.mainView}>
          {Platform.OS === "android" ? (
            <ScrollView>{mainView()}</ScrollView>
          ) : (
            <KeyboardAwareScrollView>{mainView()}</KeyboardAwareScrollView>
          )}
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  mainView: {
    maxHeight: "85%",
    backgroundColor: "#f8f8f8",

    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  mainContainerTitle: {
    paddingTop: 30,
    paddingBottom: 30,
    textAlign: "center",
    fontWeight: "bold",
    color: "#5C5C5C",
    fontSize: 20,
  },
  mainFormGroupTitle: {
    paddingTop: 20,
    paddingBottom: 10,
    paddingLeft: 15,
    textAlign: "left",
    color: "#5C5C5C",
    fontSize: 16,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: AppStyles.color.blue,
    marginTop: 10,
    textAlign: "left",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  smInput: {
    paddingLeft: 15,
    height: 50,
    borderColor: "#5C5C5C",
    borderWidth: 1,
    borderStyle: "solid",
    borderTopRightRadius: 27,
    borderTopLeftRadius: 27,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: "#ffffff",
    color: AppStyles.color.text,
    width: vw(30),
  },
  dropdownWrap: {
    display: "flex", flexDirection: "row"
  }
});

export default ActividadMonitoreadaScreen;
