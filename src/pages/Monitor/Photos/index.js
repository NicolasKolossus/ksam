import React, { useState, useEffect } from "react";
import { FlatGrid } from "react-native-super-grid";
import { vw, vh } from "react-native-css-vh-vw";
import ImagePicker from "react-native-image-crop-picker";
import AsyncStorage from "@react-native-community/async-storage";
import Button from "react-native-button";
import Icon from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import { AppStyles, AppIcon } from "../../../AppStyles";
import { background, header } from '../../../assets';

import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  Modal
} from "react-native";

const PhotosOnMonitor = ({ navigation }) => {
  const [viewFlag, setViewFlagData] = useState(false);
  const [imageUrl, setImageUrl] = useState([]);
  const [previewImage, setPreviewImageData] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");

  useEffect(() => {
    (async () => {
      const temp = await AsyncStorage.getItem("@monitorPhotos:state");
      if (temp) {
        const parsingTemp = JSON.parse(temp);
        setImageUrl(parsingTemp);
        setPreviewImageData(parsingTemp[0].image);
      } else {
        setImageUrl([]);
        AsyncStorage.removeItem("@monitorPhotos:state");
      }
    })();
  }, []);

  const changeState = () => {
    //setPreviewImageData(imageUrl[0].image);
    const temp = JSON.stringify(imageUrl);
    AsyncStorage.setItem("@monitorPhotos:state", temp);
  };


  const openCamera = (_callback) => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
    }).then((image) => {
      let temp = [];
      temp = imageUrl;
      console.log(temp);
      temp.push({ id: parseInt(image.modificationDate) + 1, image: image.path });
      setImageUrl(temp);
      setViewFlagData(true);
      changeState();
      setPreviewImageData(image.path);
      _callback();
    });
  };

  const openLibrary = (_callback) => {
    ImagePicker.openPicker({
      multiple: true,
    }).then((images) => {
      let temp = [];
      temp = imageUrl;
      console.log(temp);
      images.map((item) => {
        temp.push({ id: parseInt(item.modificationDate) + 1, image: item.path });
      });
      setPreviewImageData(images[0].path);
      setImageUrl(temp);
      setViewFlagData(true);
      changeState();
      _callback();
    });
  };

  const confirmDel = (id) => {
    setModalVisible1(true);
    setSelectedImage(id);
  };

  function closeModal() {
    setModalVisible(!modalVisible);
  }

  const delImage = () => {
    var index = imageUrl.findIndex(function(o){
      return o.id === selectedImage;
    })
    if (index !== -1) imageUrl.splice(index, 1);

    AsyncStorage.removeItem("@monitorPhotos:state");
    const temp1 = JSON.stringify(imageUrl);
    AsyncStorage.setItem("@monitorPhotos:state", temp1);
    setSelectedImage("");
    setModalVisible1(!modalVisible1);
  };
  return (
    <ImageBackground
      source={background.image}
      style={AppStyles.image}
    >
      <SafeAreaView style={styles.formContainer}>
        <TouchableOpacity
          style={AppIcon.iconContainer}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="chevron-back-sharp" size={30} color="#D68303" />
        </TouchableOpacity>
        <TouchableOpacity
          style={AppIcon.imgContainer}
          onPress={() => navigation.navigate("UploadMonitor")}
        >
          <Image source={header.logo} />
        </TouchableOpacity>
          <View style={styles.imgPickerFormContainer}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                  <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => openCamera(function() {
                    closeModal();
                  })}
                >
                  Tomar foto
                </Button>
                <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => openLibrary(function() {
                    closeModal();
                  })}
                >
                  Elegir de galeria
                </Button>
                <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => setModalVisible(!modalVisible)}
                >
                  Cancelar
                </Button>
              </View>
            </View>
          </Modal>

          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible1}
            onRequestClose={() => {
              setModalVisible1(!modalVisible1);
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
              <Text style={styles.buttonGroupTitle}>¿Esta seguro que desea eliminar esta imagen?</Text>
                  <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => delImage()}
                >
                  OK
                </Button>
                <Button
                  containerStyle={styles.buttonContainer}
                  style={styles.buttonText}
                  onPress={() => setModalVisible1(!modalVisible1)}
                >
                  Cancelar
                </Button>
              </View>
            </View>
          </Modal>

            <Text style={styles.buttonGroupTitle}>Imagenes</Text>
            {imageUrl.length > 0 ? (
              <Image style={styles.bgImage} source={{ uri: previewImage }} />
            ) : (
              <View
                style={{
                  height: vh(30),
                  justifyContent: "flex-end",
                  alignItems: "center",
                }}
              >
                <MaterialIcons
                  style={[{ color: "#000000" }]}
                  size={100}
                  name={"insert-photo"}
                />
                <Text style={[styles.buttonGroupTitle, { fontSize: 15 }]}>
                  Sin imágen
                </Text>
              </View>
            )}
            {imageUrl.length > 0 ? (
            <FlatGrid
              itemDimension={90}
              data={imageUrl}
              style={styles.gridView}
              spacing={10}
              //extraData={imageUrl}
              renderItem={({ item }) => (
                <View style={styles.itemContainer}>
                  <TouchableOpacity
                    onPress={() => setPreviewImageData(item.image)}
                    delayLongPress={1000} onLongPress={()=>{
                      confirmDel(item.id)
                    }}
                  >
                    <Image
                      source={{ uri: item.image }}
                      style={{ width: "100%", height: "100%" }}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
            ) : (<></>)}
            <Button
              containerStyle={styles.selectButtonContainer}
              style={[styles.buttonText, { color: "#5C5C5C" }]}
              onPress={() => setModalVisible(true)}
            >
              Agregar imagen
            </Button>
          </View>
      </SafeAreaView>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  formContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  mainContainer: {
    backgroundColor: "#f8f8f8",
    alignItems: "center",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  imgPickerFormContainer: {
    backgroundColor: "#f8f8f8",
    alignItems: "center",
    paddingLeft: 30,
    paddingRight: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    flex: 0.9,
  },
  buttonGroupTitle: {
    fontSize: 20,
    color: "#5C5C5C",
    fontWeight: "bold",
    marginBottom: 30,
    marginTop: 20,
  },
  buttonContainer: {
    width: vw(80),
    marginTop: 10,
    borderWidth: 1,
    borderColor: "#CECECE",
    backgroundColor: "#ffffff",
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
  },
  smButtonContainer: {
    width: vw(50),
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    width: vw(50),
    marginTop: 50,
    marginBottom: 50,
  },
  selectButtonContainer: {
    width: vw(50),
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    width: vw(50),
    marginBottom: 20,
    borderWidth: 1,
    borderColor: "#CECECE",
    backgroundColor: "#ffffff",
  },
  deleteButtonContainer: {
    width: vw(50),
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    backgroundColor: "#E5E5E5",
    width: vw(50),
    marginTop: 10,
    marginBottom: 30,
  },
  buttonText: {
    fontSize: 16,
    color: AppStyles.color.grey,
  },
  bgImage: {
    width: "100%",
    height: vh(25),
    borderRadius: 10,
  },
  gridView: {
    height: vw(40),
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 10,
    flex: 1,
    width: vw(90)
  },
  itemContainer: {
    justifyContent: "center",
    height: 80,
  },
  title: {
    fontSize: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  }
});

export default PhotosOnMonitor;
