import * as firebase from "firebase";
import "firebase/firestore";
import "firebase/database";
import "firebase/auth";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyD-B3QevycKtbM7pbpgy1UfdoZxKewM1RY",
  authDomain: "maa-consultores.firebaseapp.com",
  databaseURL: "https://maa-consultores-default-rtdb.firebaseio.com",
  projectId: "maa-consultores",
  storageBucket: "maa-consultores.appspot.com",
  messagingSenderId: "341538575040",
  appId: "1:341538575040:android:49d62a2485a70f3a939325",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export const realDb = firebase.database();
export const db = firebase.firestore();
export const auth = firebase.auth();
export const storage = firebase.storage();
export const fb = firebase;
